<?php
class DBController {
	private $host = 'localhost';
	private $user = 'myboosta_wp';
	private $password = 'KqkyqvSWkh7Wsxsp';
	private $database = 'myboosta_wp';
  public $conn = '';
	
	function __construct() {
		$this->conn = $this->connectDB();
		if(!empty($this->conn)):
			$this->selectDB($this->conn);
		endif;
	}
	
	function connectDB() {
		$this->conn = mysqli_connect($this->host,$this->user,$this->password,$this->database);
    if (mysqli_connect_errno()):
     echo "Failed to connect to MySQL: " . mysqli_connect_error();
    endif;
		return $this->conn;
	}
	
	function selectDB($conn) {
		mysqli_select_db($this->conn, $this->database);
	}
  
  function closeDB($conn, $result) {
		mysqli_free_result($result);
    mysqli_close($conn);
	}
	
	function runQuery($query) {
		$result = mysqli_query($this->conn,$query);
    if ($result):
		  while($row=mysqli_fetch_assoc($result)):
			  $resultset[] = $row;
		  endwhile;
    else:
      $response = "Query has no rows";
      $this->closeDB($this->conn, $result);
      return $response;		
    endif;
		if(!empty($resultset)):
      $this->closeDB($this->conn, $result);
			return $resultset;
    endif;
	}
	
	function numRows($query) {
		$result  = mysqli_query($this->conn, $query);
		$rowcount = mysqli_num_rows($result);
		return $rowcount;	
	}
}
?>