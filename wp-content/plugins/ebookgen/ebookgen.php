<?php
/**
 * Plugin Name: eBook generation
 * Plugin URI: 
 * Description: This plugin generates pdf ebook and preview it in flipbook mode.
 * Version: 1.0.0
 * Author: Lucia Arsena
 * Author URI: http://vivaria.it
 * License: GPL2
 */
 

/* Enqueue js e css */ 

function ebookgen_enqueue_script() {   
  wp_enqueue_script( 'dFlip-script', plugin_dir_url( __FILE__ ) .'dflip/js/dflip.min.js', array('jquery'), '1.0' );
  if (is_page('personalizzazione-iniziale-libro')) {
    wp_enqueue_script( 'ebookgen-initial-script', plugin_dir_url( __FILE__ ) .'js/initial-book-customization.js', array('jquery'), '1.0' );  
  }
  if (is_page('personalizza-libro')) {
    wp_enqueue_script( 'ebookgen-initial-script', plugin_dir_url( __FILE__ ) .'js/initial-book-customization-1.js', array('jquery'), '1.0' );  
  }
  if (is_page('personalizzazione-la-favola-del-mago-del-tempo')) {
    wp_enqueue_script( 'ebookgen-lfdmdt-script', plugin_dir_url( __FILE__ ) .'js/la-favola-del-mago-del-tempo-customization.js', array('jquery'), '1.0' ); 
    wp_enqueue_script( 'ebookgen-customization-script', plugin_dir_url( __FILE__ ) .'js/common-book-customization.js', array('jquery'), '1.0' ); 
  }
	if (is_page('a')) {
    wp_enqueue_script( 'ebookgen-lfdmdt-script', plugin_dir_url( __FILE__ ) .'js/la-favola-del-mago-del-tempo-customization.js', array('jquery'), '1.0' ); 
    wp_enqueue_script( 'ebookgen-customization-script', plugin_dir_url( __FILE__ ) .'js/common-book-customization.js', array('jquery'), '1.0' ); 
  }
  if (is_page('la-favola-del-mago-del-tempo')) {
    wp_enqueue_script( 'ebookgen-lfdmdt-script', plugin_dir_url( __FILE__ ) .'js/la-favola-del-mago-del-tempo-customization-1.js', array('jquery'), '1.0' ); 
    wp_enqueue_script( 'ebookgen-customization-script', plugin_dir_url( __FILE__ ) .'js/common-book-customization-1.js', array('jquery'), '1.0' ); 
  }
  /* !!!da eliminare quando i form finali saranno pronti!!!! */
  if (is_page('crea-libro')) {
    wp_enqueue_script( 'ebookgen-script', plugin_dir_url( __FILE__ ) .'js/gravity-forms-mapping.js', array('jquery'), '1.0' ); 
 
  }  
  
  //wp_enqueue_script( 'so_test', plugin_dir_url( __FILE__ ) .'js/test.js' ); 
   // $i18n = array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'checkout_url' => get_permalink( wc_get_page_id( 'checkout' ) ) );
   // wp_localize_script( 'so_test', 'SO_TEST_AJAX', $i18n );
}
add_action('wp_enqueue_scripts', 'ebookgen_enqueue_script');



function ebookgen_enqueue_style() { 
  wp_enqueue_style( 'dFlip-style', plugin_dir_url( __FILE__ ) .'dflip/css/dflip.css',  array(), 1  );    
  wp_enqueue_style( 'dFlip-icons', plugin_dir_url( __FILE__ ) .'dflip/css/themify-icons.css',  array(), 1  );
  wp_enqueue_style( 'ebookgen-style', plugin_dir_url( __FILE__ ) .'css/ebookgen.css',  array(), 1  );  
}
add_action('wp_enqueue_scripts', 'ebookgen_enqueue_style');


/* procedura di creazione pdf, attivata da ajax */
 
$generated_books_rel_path = '/myboo/generated-books'; 
$template_books_rel_path = '/myboo/template-books/';

function createBook1(){  
  $trace = 'init ' . time();
  $time = time();
  $uploads = wp_upload_dir();  
  if ( !defined('ABSPATH') )
	  define('ABSPATH', dirname(__FILE__) . '/');
  $abspath= ABSPATH; 
  
  global $generated_books_rel_path, $template_books_rel_path;
   
  $generated_book_dir = $uploads['basedir'] .$generated_books_rel_path;
  $generated_book_url = $uploads['baseurl'] .$generated_books_rel_path .'/' .date('Y', $time) .'/' .date('m', $time) .'/' .$_REQUEST['title'];
  $generated_book_path = $generated_book_dir .'/' .date('Y', $time) .'/' .date('m', $time) .'/' .$_REQUEST['title'];


  $template_book_dir = $uploads['basedir'] .$template_books_rel_path .$_REQUEST['title'];
  $path = site_url();
  
  require_once('dbcontroller.php');
  //require('fpdf/fpdf.php');
  require('textbox/textbox.php');
  
  $query_book_val = $_REQUEST['query_book_val'];
  $query_select_var = $_REQUEST['query_select_var'];
  $query_select_val = $_REQUEST['query_select_val'];
  $text_select_var = $_REQUEST['text_select_var'];
  $text_select_val = $_REQUEST['text_select_val'];
  $query_var_num = count($query_select_var);
  $text_var_num = count($text_select_var);
  $query_cond = "(booktitle = '" .$query_book_val ."' AND field = '' AND value = '')";
  
  //$trace = $trace .' start query build ' . time();
  
  for($i = 0; $i < $query_var_num; $i++):
      $query_cond = $query_cond ." OR " ."(booktitle = '" .$query_book_val ."' AND field = '" .$query_select_var[$i] ."' AND " ."value = '" .$query_select_val[$i] ."')";
  endfor;
  /*
  for($i = 0; $i < $text_var_num; $i++):
      $query_cond = $query_cond ." OR " ."(booktitle = '" .$query_book_val ."' AND field LIKE '%" .$text_select_var[$i] ."%' AND " ."value = '%" .$text_select_val[$i] ."%')";
  endfor;
   */
  //$trace = $trace .' end query build ' . time();
  
  $filename = $_REQUEST['title'] .'-' .time();
   
  $db_handle = new DBController();    
  $query = "SELECT type, page, layer, file FROM books WHERE " .$query_cond ." ORDER BY page, layer ASC"; 
  
  //$trace = $trace .' start query ' . time();
  
  $result = $db_handle->runQuery($query);
  
  //$trace = $trace .' end query ' . time();
  
  $trace = $trace .' start new pdf ' . time() .'\r\n';
  
  //$pdf = new FPDF('P','cm',array(5,5));
  $pdf=new PDF_TextBox('L','cm',array(5,5));
  $pdf->AddFont('Satisfy','','Satisfy-Regular.php');

  //$trace = $trace .' end new pdf start init' . time();
  
  $pdf->SetMargins(0, 0);
  //$pdf->SetLineWidth(2);
  $pdf->SetAutoPageBreak(true, 0);
  $page = 0;
  $trace = $trace .' start foreach ' . time() .'\r\n';
  $i = 0;
  foreach($result as $row):
    $i++;
    
    $prev_page = $page;
    $page = $row["page"];
    if ( $page > $prev_page):  
      $pdf->AddPage();
      
      $trace = $trace .' addPage ' .$page .' step ' .$i .' ' .time() .'\r\n';
    endif;    
    if ($row["type"] === 'image'):  
      $file = $template_book_dir .'/' .$row["file"];
      $pdf->Image($file,0,0,5,5);
      $trace = $trace .' addImage ' .$file .' step ' .$i .' ' .time() .'\r\n';
    elseif ($row["type"] === 'text'):  
      //$text = $row["file"];
      $text = file_get_contents($template_book_dir .'/' .$row["file"]);
      //$text_var_count = count($text_select_val);
      for($j = 0; $j < $text_var_num; $j++): 
        $text = str_replace($text_select_var[$j], $text_select_val[$j], $text);
      endfor; 
      //$pageContent = (string)$text;
      //$pdf->MultiCell(0, 1, $text, 0, 'C', 0, 1, '', '', true, null, true);
      //$pdf->Write(1,$text);
       // Read text file
    //$txt = file_get_contents($file);
    // Times 12
    //$pdf->SetFont('Times','',6);
    $pdf->SetFont('Satisfy','', 6);
    $pdf->SetXY(0.5, 0.5);
    // Output justified text
    //$pdf->MultiCell(4,0.3,$text,0, C, false);
    $pdf->drawTextBox($text, 4, 4, 'C', 'M', false);

      
      $trace = $trace .' addImage ' .$text .' step ' .$i .' ' .time() .'\r\n';
      //$pdf->Write(1, $text);
    endif;
  endforeach; 
  $trace = $trace .' end foreach ' . time() .'\r\n';
  if (!file_exists($generated_book_path )) {
    wp_mkdir_p($generated_book_path);
  }
  
  $book_url =  $generated_book_url .'/' .$filename .'.pdf';
  $book_path =  $generated_book_path .'/' .$filename .'.pdf';  
  
  session_start();     
  $_SESSION['wdm_file'] = $book_url;

  $pdf->Output('F', $book_path);           

  $trace = $trace .' end pdf ' . time();
  
  echo $book_url;
 //echo $query;   
 //echo $text_select_val[0]; 

  die();
}
add_action('wp_ajax_createBook1', 'createBook1');
add_action('wp_ajax_nopriv_createBook1', 'createBook1');



function myboo_add_to_cart1() {
	
  ob_start();
  $product_id        = 13;
  $quantity          = 1;
  $passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );
  $product_status    = get_post_status( $product_id ); 

  if ( $passed_validation && WC()->cart->add_to_cart( $product_id, $quantity ) && 'publish' === $product_status ) {
    do_action( 'woocommerce_ajax_added_to_cart', $product_id );
    wc_add_to_cart_message( $product_id );
    
  } 
  else {
    /*
    // If there was an error adding to the cart, redirect to the product page to show any errors
    $data = array(
      'error'       => true,
      'product_url' => apply_filters( 'woocommerce_cart_redirect_after_error', get_permalink( $product_id ), $product_id )
    );
    wp_send_json( $data );
    */
  }

}
add_action( 'gform_after_submission', 'myboo_add_to_cart1', 10, 2 );



function iconic_add_engraving_text_to_cart_item( $cart_item_data, $product_id, $variation_id ) {session_start();
    $engraving_text = $_SESSION['wdm_file'];
 
    if ( empty( $engraving_text ) ) {
        return $cart_item_data;
    }
 
    $cart_item_data['iconic-engraving'] = $engraving_text;
 
    return $cart_item_data;
}
 
add_filter( 'woocommerce_add_cart_item_data', 'iconic_add_engraving_text_to_cart_item', 10, 3 );

function iconic_display_engraving_text_cart( $product_name, $cart_item, $cart_item_key ) {
    if ( empty( $cart_item['iconic-engraving'] ) ) {
        return $product_name;
    }
 
    return sprintf( '%s <p><strong>%s</strong>: %s</p>', $product_name, __( 'Engraving', 'iconic' ), $cart_item['iconic-engraving'] );
}
 
add_filter( 'woocommerce_cart_item_name', 'iconic_display_engraving_text_cart', 10, 3 );

function iconic_add_engraving_text_to_order_items( $item, $cart_item_key, $values, $order ) {
    if ( empty( $values['iconic-engraving'] ) ) {
        return;
    }
 
    $item->add_meta_data( __( 'Engraving', 'iconic' ), $values['iconic-engraving'] );
}
 
add_action( 'woocommerce_checkout_create_order_line_item', 'iconic_add_engraving_text_to_order_items', 10, 4 );


?>