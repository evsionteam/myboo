var dFlipLocation = "http://myboo.nexnova.biz/wp-content/plugins/ebookgen/dflip/";

// Gravity Forms mapping - Forms
bookCustomForm = 12;
bookCustomizeForm = '#gform'+'_'+bookCustomForm;

// Gravity Forms mapping - Pages
sex_settings = 1;
name_settings = 2;
hair_settings = 3;
eyesAndSkin_settings = 4;
bookSelection_settings = 5;
laNascita_settings = 6;
unGiornoAllAsilo_settings = 0;
unaGiornataSpeciale_settings = 0;
buonCompleanno_settings = 0;
lAssistenteDelReDelleStelle_settings = 0;
laFavolaDelMagoDelTempo_settings = 0;
laNascitaNananSpecialEdition_settings = 0;
previewPage = 7;
cartPage = 8;

// Gravity Forms mapping - Sections
laNascitaStep_sections = [68,66,62,74,84,83];
laNascitaStopSection = 96;

jQuery(document).bind('gform_page_loaded', function(event, form_id, current_page){
  if (form_id == bookCustomForm) {
  
    if (current_page == sex_settings) {
      jQuery('.gform_next_button').hide(); 
      
      jQuery('#female-child').on("click",function(e){ 
        jQuery('input[name=input_1]').filter('[value=bambina]').prop('checked', true);
        jQuery('input[name=input_1]').filter('[value=bambino]').prop('checked', false);
        jQuery(bookCustomizeForm).trigger("submit",[true]);
      }); 

      jQuery('#male-child').on("click",function(e){ 
        jQuery('input[name=input_1]').filter('[value=bambina]').prop('checked', false);
        jQuery('input[name=input_1]').filter('[value=bambino]').prop('checked', true);
        jQuery(bookCustomizeForm).trigger("submit",[true]);
      });
    }
    else {
      jQuery('.gform_next_button').show();
    }
    
    if (current_page == previewPage) {
    
      jQuery('#img').show();
      
      title = jQuery('input[name=input_65]:checked', bookCustomizeForm).val();
      
      if (title == 'la-nascita') {
        // get la nascita book customization
        query_book_val = 'La nascita';
        sesso_bambino = jQuery('input[name=input_1]:checked', bookCustomizeForm).val();
        occhi_bambino =jQuery('input[name=input_15]:checked', bookCustomizeForm).val();
        colore_capelli_bambino= jQuery('input[name=input_8]:checked', bookCustomizeForm).val();
        pelle_bambino= jQuery('input[name=input_17]:checked', bookCustomizeForm).val();
        occhi_mamma= jQuery('input[name=input_32]:checked', bookCustomizeForm).val();
        colore_capelli_mamma= jQuery('input[name=input_27]:checked', bookCustomizeForm).val();
        acconciatura_mamma= jQuery('input[name=input_29]:checked', bookCustomizeForm).val();
        lunghezza_mamma= jQuery('input[name=input_42]:checked', bookCustomizeForm).val();
        pelle_mamma= jQuery('input[name=input_45]:checked', bookCustomizeForm).val();
        occhi_papa= jQuery('input[name=input_44]:checked', bookCustomizeForm).val();
        colore_capelli_papa= jQuery('input[name=input_40]:checked', bookCustomizeForm).val();
        acconciatura_papa = jQuery('input[name=input_41]:checked', bookCustomizeForm).val();
        pelle_papa= jQuery('input[name=input_33]:checked', bookCustomizeForm).val();
        capelli_mamma = colore_capelli_mamma+'&'+acconciatura_mamma+'&'+lunghezza_mamma;
        capelli_papa = colore_capelli_papa+'&'+acconciatura_papa;
        bambino = sesso_bambino+'&'+pelle_bambino;
   
        query_select_var = ['sesso-bambino&pelle-bambino','occhi-bambino','colore-capelli-bambino','occhi-mamma','colore-capelli-mamma&acconciatura-mamma&lunghezza-mamma','pelle-mamma','occhi-papa','colore-capelli-papa&acconciatura-papa','pelle-papa'];
        query_select_val = [bambino,occhi_bambino,colore_capelli_bambino,occhi_mamma,capelli_mamma,pelle_mamma,occhi_papa,capelli_papa,pelle_papa];
      } // if (title == 'la-nascita') 
      
      if (title == 'un-giorno-all-asilo') {   
      } // if (title == 'un-giorno-all-asilo')
      
      if (title == 'una-giornata-speciale') {   
      } // if (title == 'una-giornata-speciale')
      
      if (title == 'buon-compleanno') {   
      } // if (title == 'buon-compleanno') 
      
      if (title == 'l-assistente-del-re-delle-stelle') {   
      } // if (title == 'l-assistente-del-re-delle-stelle')
      
      if (title == 'la-favola-del-mago-del-tempo') {   
      } // if (title == 'la-favola-del-mago-del-tempo')
      
      if (title == 'la-nascita-nanan-special-edition') {   
      } // if (title == 'la-nascita-nanan-special-edition')
      
      jQuery.ajax({
        type:"POST",
        url: "/wp-admin/admin-ajax.php",
        data : {
			    action : 'createBook1',
          title: title,
          query_book_val: query_book_val,
          query_select_var: query_select_var,
          query_select_val: query_select_val     
		    },
        success:function(response ){
          var options = {
            height: 500, enableDownload: false,
            allControls: "altPrev,pageNumber,altNext,outline,thumbnail,zoomIn,zoomOut,fullScreen,share,more,download,pageMode,startPage,endPage,sound",
		        mainControls: "altPrev,pageNumber,altNext,outline,thumbnail,zoomIn,zoomOut,fullScreen,share,more",
		        hideControls: "outline, thumbnail, share, more",
            transparent: true,
          };
          jQuery('#img').hide();
          flipBook = jQuery("#flipbookContainer").flipBook(response, options);
          //jQuery("#feedback").html("<p>Ecco l'anteprima del tuo libro </p>" + response);
        }
      });
    } // if (current_page == previewPage)
    
    
    if (current_page == cartPage) {

          jQuery.ajax({  
            type:"POST",
            url: "/wp-admin/admin-ajax.php",
            data : {
			        action : 'myajaxcart', 
		        },
            success:function(response ){

                jQuery("#cart-container").html(response);
                jQuery('.wc-proceed-to-checkout').hide();

            }            

        });
      } // if (current_page == cartPage) 
    
    jQuery(document).ready(function() {
    
      jQuery('.child-name').each(function() {
        jQuery(this).html( jQuery('input[name=input_3]').val());
      });
      
      jQuery('input[name=input_101]').attr('value', 1);
      
      jQuery('#actual-step').html(jQuery('input[name=input_101]').attr('value'));
      jQuery('#final-step').html(laNascitaStep_sections.length); 
      
      if (current_page == hair_settings) {
        jQuery(".no-scelta").on("click",function(e){
          jQuery('input[name=input_8]').filter('[value=biondi]').prop('checked', true);
          jQuery('input[name=input_55]').filter('[value=lisci]').prop('checked', true);
          jQuery('input[name=input_56]').filter('[value=corti]').prop('checked', true);
        });
      } // if (current_page == hair_settings)
    
      if (current_page == eyesAndSkin_settings) {  
        jQuery(".no-scelta").on("click",function(e){
          jQuery('input[name=input_15]').filter('[value=neri]').prop('checked', true);
          jQuery('input[name=input_17]').filter('[value=bianca]').prop('checked', true);
        });
      } // if (current_page == eyesAndSkin_settings)
    
      if (current_page == laNascita_settings) {
    
        jQuery('.gform_next_button').hide();
        
        i = jQuery('input[name=input_101]').attr('value');
        
        if (i ==1) { 
          nextSection = '#field_'+bookCustomForm+'_'+laNascitaStep_sections[i];
          stopSection = '#field_'+bookCustomForm+'_'+laNascitaStopSection;
          jQuery(nextSection).hide();
          jQuery(nextSection).nextUntil(jQuery(stopSection)).hide();
        }
      
        jQuery(".back-button").on("click",function(e){   
          i = jQuery('input[name=input_101]').attr('value');
          if (i > 1) {  
            actualSection = '#field_'+bookCustomForm+'_'+laNascitaStep_sections[i-1];
            prevSection = '#field_'+bookCustomForm+'_'+laNascitaStep_sections[i-2];
            if (i < 6) {  
              nextSection = '#field_'+bookCustomForm+'_'+laNascitaStep_sections[i];
            }
            else {  
              nextSection = '#field_'+bookCustomForm+'_'+laNascitaStopSection;
            }
            jQuery(actualSection).nextUntil(jQuery(nextSection)).hide();
            jQuery(actualSection).prevUntil(jQuery(prevSection)).show();
            i--;
            if(i == 4) {
              jQuery('.master').find('input[name=input_75]:checked').trigger("click",[true]);
            }
            jQuery('input[name=input_101]').attr('value', i);
            jQuery('#actual-step').html(i);
          }
        }); // jQuery(".back-button").on("click",function(e)
         
        jQuery(".next-button").on("click",function(e){   
          i = jQuery('input[name=input_101]').attr('value'); 
          if (((i == 1) && (jQuery('input[name=input_69]').val().length == 0 || jQuery('input[name=input_71]').val().length == 0 )) ||
              (i == 2 && jQuery('input[name=input_24]').val().length == 0)||
              (i == 3 && jQuery('input[name=input_37]').val().length == 0)
            ){
            if (i == 1 && jQuery('input[name=input_69]').val().length == 0) {
              if ((jQuery('input[name=input_69]').parent().parent().find('.custom-validation')).length == 0) {            
                jQuery("<div class='gfield_description custom-validation validation_message'>Questo campo &#232; obbligatorio.</div>").insertAfter(jQuery('input[name=input_69]').parent());
              }
              jQuery('input[name=input_69]').parent().parent().addClass('gfield_error'); 
            }
            if (i == 1 && jQuery('input[name=input_71]').val().length == 0) {
              if ((jQuery('input[name=input_71]').parent().parent().find('.custom-validation')).length == 0) {   
                jQuery("<div class='gfield_description custom-validation validation_message'>Questo campo &#232; obbligatorio.</div>").insertAfter(jQuery('input[name=input_71]').parent());
              }
              jQuery('input[name=input_71]').parent().parent().addClass('gfield_error'); 
            }
            if (i == 2 && jQuery('input[name=input_24]').val().length == 0) {
              if ((jQuery('input[name=input_24]').parent().parent().find('.custom-validation')).length == 0) {              
                jQuery("<div class='gfield_description custom-validation validation_message'>Questo campo &#232; obbligatorio.</div>").insertAfter(jQuery('input[name=input_24]').parent());
              }
              jQuery('input[name=input_24]').parent().parent().addClass('gfield_error'); 
            }
            if (i == 3 && jQuery('input[name=input_37]').val().length == 0) {
              if ((jQuery('input[name=input_37]').parent().parent().find('.custom-validation')).length == 0) {              
                jQuery("<div class='gfield_description custom-validation validation_message'>Questo campo &#232; obbligatorio.</div>").insertAfter(jQuery('input[name=input_37]').parent());
              }
              jQuery('input[name=input_37]').parent().parent().addClass('gfield_error'); 
            }
          }         
          else {
            
            if (i == 1) {
              jQuery('input[name=input_69]').parent().parent().find('.custom-validation').remove();
              jQuery('input[name=input_69]').parent().parent().removeClass('gfield_error'); 
              jQuery('input[name=input_71]').parent().parent().find('.custom-validation').remove();
              jQuery('input[name=input_71]').parent().parent().removeClass('gfield_error'); 
            }
            
            if (i == 2) {
              jQuery('input[name=input_24]').parent().parent().find('.custom-validation').remove();
              jQuery('input[name=input_24]').parent().parent().removeClass('gfield_error');  
            }
          
            if (i == 3) {
              jQuery('input[name=input_37]').parent().parent().find('.custom-validation').remove();
              jQuery('input[name=input_37]').parent().parent().removeClass('gfield_error'); 
            }
          
            if (i < laNascitaStep_sections.length) {
              prev = i; 
              next = i;
              prev--;
              next++;
              actualSection = '#field_'+bookCustomForm+'_'+laNascitaStep_sections[prev];  
              nextSection = '#field_'+bookCustomForm+'_'+laNascitaStep_sections[i];
              stopSection = '#field_'+bookCustomForm+'_'+laNascitaStep_sections[next];
              jQuery(actualSection).hide();
              jQuery(actualSection).nextUntil(jQuery(nextSection)).hide();
              jQuery(nextSection).nextUntil(jQuery(stopSection)).show(); 
              i++;
              if (i == 4) {
                jQuery('.master').find('input[name=input_75]:checked').trigger("click",[true]);
              }
              jQuery('input[name=input_101]').attr('value', i);
              jQuery('#actual-step').html(i);
            }
            if (i == laNascitaStep_sections.length) {
              jQuery('.gform_next_button').show();
            }
          }
        }); // jQuery(".next-button").on("click",function(e)
      } // if (current_page == laNascita_settings)
    
      if (current_page == unGiornoAllAsilo_settings) {
      } // if (current_page == unGiornoAllAsilo_settings)
    
      if (current_page == unaGiornataSpeciale_settings) {
      } // if (current_page == unaGiornataSpeciale_settings)
    
      if (current_page == buonCompleanno_settings) {
      }  // if (current_page == buonCompleanno_settings)
       
      if (current_page == lAssistenteDelReDelleStelle_settings) {
      } // if (current_page == lAssistenteDelReDelleStelle_settings)
    
      if (current_page == laFavolaDelMagoDelTempo_settings) {
      } // if (current_page == laFavolaDelMagoDelTempo_settings)
    
      if (current_page == laNascitaNananSpecialEdition_settings) {
      } // if (current_page == laNascitaNananSpecialEdition_settings)
      
      if (current_page == previewPage) {
        jQuery('.gform_next_button').on('click', function(e){
          //e.preventDefault();  // Prevent the click from going to the link
        
          jQuery.ajax({
            type:"POST",
            url: wc_add_to_cart_params.ajax_url,
            data : {
			        action : 'myajax', 
		        },
            done:function(response ){
              if( response.error != 'undefined' && response.error ){
                //some kind of error processing or just redirect to link
                // might be a good idea to link to the single product page in case JS is disabled
              return true;
              } else {
                //window.location.href = SO_TEST_AJAX.checkout_url;
              }
            }
          });
        });
      } // if (current_page == previewPage) 
      
    }) // jQuery(document).ready(function() 
  } // if (form_id == bookCustomForm)
}) 

jQuery(document).ready(function() {
  
  //if (current_page == sex_settings)   
  jQuery('.gform_next_button').hide();

  //if (current_page == sex_settings) 
  jQuery('#female-child').on("click",function(e){
    jQuery('input[name=input_1]').filter('[value=bambina]').prop('checked', true);
    jQuery('input[name=input_1]').filter('[value=bambino]').prop('checked', false);
    jQuery(bookCustomizeForm).trigger("submit",[true]);
  }); 
  
  //if (current_page == sex_settings) 
  jQuery('#male-child').on("click",function(e){
    jQuery('input[name=input_1]').filter('[value=bambina]').prop('checked', false);
    jQuery('input[name=input_1]').filter('[value=bambino]').prop('checked', true);
    jQuery(bookCustomizeForm).trigger("submit",[true]);
  }); 
}); 


jQuery('.expand').on('click', function(e){e.preventDefault();e.stopPropagation();item=jQuery(this).parent().parent(); item.next();
while(!(item.next().has('.first-level-menu'))){item.toggle();item=next();}});


    