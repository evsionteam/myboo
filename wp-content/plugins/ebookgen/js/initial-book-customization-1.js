bookFormId = 16;
bookFormName =  '#gform';
bookCustomizeForm = bookFormName+'_'+bookFormId;

// Gravity Forms mapping - Pages
sex_settings = 1;
name_settings = 2;
hair_settings = 3;
eyesAndSkin_settings = 4;
bookSelection_settings = 5;

jQuery(document).bind('gform_page_loaded', function(event, form_id, current_page){ 

  if (current_page == bookSelection_settings) {
    current_page_name = bookFormName+'_page_'+bookFormId+'_'+current_page;  
  current_page_footer = jQuery(current_page_name).find(".gform_page_footer"); 
  current_footer_content = current_page_footer.html();
  pos = current_footer_content.indexOf('<input type="submit" id="gform_submit_button'); 
  current_prev_btn = current_footer_content.substr(0, pos); 
  current_next_btn = current_footer_content.substr(pos);  
  
  jQuery(current_page_footer).html('<div class="gform_page_footer">'+
    '<div style="position: fixed; bottom: 0; left: 50%; -webkit-transform: translateX(-50%); -moz-transform: translateX(-50%); -ms-transform: translateX(-50%); -o-transform: translateX(-50%);transform: translateX(-50%);">'+
      current_prev_btn+
      '<div class="customization-steps" style="padding: 10px; float: left">'+
        '<span style="padding: 10px; border: 1px solid #000"> Libro </span>'+
        '<span  style="padding: 10px; border: 1px solid #000"> Personalizza </span>'+
        '<span style="padding: 10px; border: 1px solid #000;font-weight: bold; background: #ddd"> Anteprima </span>'+
        '<span style="padding: 10px; border: 1px solid #000"> Carrello </span>'+
      '</div>'+
    '</div>'+
    current_next_btn+
    '</div>');
  }
  
  jQuery(document).ready(function() {
  
    if (current_page == sex_settings) {
      
      jQuery('.gform_next_button').hide(); 
      
      jQuery('#female-child').on("click",function(e){ 
        jQuery('input[name=input_1]').filter('[value=bambina]').prop('checked', true);
        jQuery('input[name=input_1]').filter('[value=bambino]').prop('checked', false);
        jQuery(bookCustomizeForm).trigger("submit",[true]);
      }); 

      jQuery('#male-child').on("click",function(e){ 
        jQuery('input[name=input_1]').filter('[value=bambina]').prop('checked', false);
        jQuery('input[name=input_1]').filter('[value=bambino]').prop('checked', true);
        jQuery(bookCustomizeForm).trigger("submit",[true]);
      });
    }
    else { 
      jQuery('.gform_next_button').show();
    }
      
    if (current_page == hair_settings) {
      jQuery(".no-scelta").on("click",function(e){
        jQuery('input[name=input_8]').filter('[value=biondi]').prop('checked', true);
        jQuery('input[name=input_55]').filter('[value=lisci]').prop('checked', true);
        jQuery('input[name=input_56]').filter('[value=corti]').prop('checked', true);
      });
    } // if (current_page == hair_settings)
    
    if (current_page == eyesAndSkin_settings) {
      jQuery(".no-scelta").on("click",function(e){
        jQuery('input[name=input_15]').filter('[value=neri]').prop('checked', true);
        jQuery('input[name=input_17]').filter('[value=bianca]').prop('checked', true);
      });
    } // if (current_page == eyesAndSkin_settings)     
  }) 
}) // if (form_id == bookCustomForm)
 

jQuery(document).ready(function() {     
  
  jQuery('.gform_next_button').hide();

  jQuery('#female-child').on("click",function(e){  
    jQuery('input[name=input_1]').filter('[value=bambina]').prop('checked', true);
    jQuery('input[name=input_1]').filter('[value=bambino]').prop('checked', false);
    jQuery(bookCustomizeForm).trigger("submit",[true]);
  }); 
  
  jQuery('#male-child').on("click",function(e){  
    jQuery('input[name=input_1]').filter('[value=bambina]').prop('checked', false);
    jQuery('input[name=input_1]').filter('[value=bambino]').prop('checked', true);
    jQuery(bookCustomizeForm).trigger("submit",[true]);
  }); 
}); 


    