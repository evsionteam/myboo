var bookFormId = 17;
var bookFormName = '#gform';

var previewPage = 5;  //impostare numero pagina 'Anteprima'
var cartPage = 6;  //impostare numero pagina 'Carrello'

var lastStep = 4;

jQuery(document).bind('gform_page_loaded', function(event, form_id, current_page){ 
   
  if (current_page == previewPage) {  
  
   /* prepara gli argomenti della query per selezionare i livelli di libro corrispondenti alla personalizzazione effettuata */
    title = jQuery('input[name=input_126]').val(); //alert('title '+title);
    query_book_val = 'La Favola del Mago del Tempo';  //alert('query_book_val '+query_book_val);
    
    sesso_bambino = jQuery('input[name=input_113]').val(); //alert('sesso_bambino '+sesso_bambino);
    nome_bambino = jQuery('input[name=input_119]').val(); //alert('nome_bambino '+nome_bambino);
    cognome_bambino = jQuery('input[name=input_120]').val(); //alert('cognome_bambino '+cognome_bambino);
    occhi_bambino = jQuery('input[name=input_117]').val(); //alert('occhi_bambino '+occhi_bambino);
    colore_capelli_bambino = jQuery('input[name=input_114]').val(); //alert('colore_capelli_bambino '+colore_capelli_bambino);
    acconciatura_bambino = jQuery('input[name=input_115]').val(); //alert('acconciatura_bambino '+acconciatura_bambino);
    lunghezza_bambino = jQuery('input[name=input_116]').val(); //alert('lunghezza_bambino '+lunghezza_bambino);
    pelle_bambino = jQuery('input[name=input_118]').val(); //alert('pelle_bambino '+pelle_bambino);
    pupazzo = jQuery('input[name=input_72]:checked').val(); //alert('pupazzo '+pupazzo);
    nome_donna = jQuery('input[name=input_24]').val(); //alert('nome_donna '+nome_donna);
    occhi_donna = jQuery('input[name=input_32]:checked').val(); //alert('occhi_donna '+occhi_donna);
    colore_capelli_donna = jQuery('input[name=input_27]:checked').val(); //alert('colore_capelli_donna '+colore_capelli_donna);
    acconciatura_donna = jQuery('input[name=input_29]:checked').val(); //alert('acconciatura_donna '+acconciatura_donna);
    lunghezza_donna = jQuery('input[name=input_42]:checked').val(); //alert('lunghezza_donna '+lunghezza_donna);
    pelle_donna = jQuery('input[name=input_45]:checked').val(); //alert('pelle_donna '+pelle_donna);
    capelli_bambino = colore_capelli_bambino+'&'+acconciatura_bambino+'&'+lunghezza_bambino; //alert('capelli_bambino '+capelli_bambino);
    capelli_donna = colore_capelli_donna+'&'+acconciatura_donna+'&'+lunghezza_donna; //alert('capelli_donna '+capelli_donna);
    bambino = sesso_bambino+'&'+pelle_bambino; //alert('bambino '+bambino);
   
    query_select_var = ['sesso-bambino&pelle-bambino','occhi-bambino','colore-capelli-bambino&acconciatura-bambino&lunghezza-bambino','pupazzo','occhi-donna','colore-capelli-donna&acconciatura-donna&lunghezza-donna','pelle-donna'];
    query_select_val = [bambino,occhi_bambino,capelli_bambino,pupazzo,occhi_donna,capelli_donna,pelle_donna];

 }});
    