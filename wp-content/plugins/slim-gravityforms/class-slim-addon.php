<?php

GFForms::include_addon_framework();

class GFSlimAddOn extends GFAddOn {

    protected $_version = GF_SLIM_ADDON_VERSION;
    protected $_min_gravityforms_version = '1.9';
    protected $_slug = 'slim-gravityforms';
    protected $_path = 'slim-gravityforms/slim-gravityforms.php';
    protected $_full_path = __FILE__;
    protected $_title = 'Slim Gravity Forms Add-On';
    protected $_short_title = 'Slim Add-On';

    private static $_instance = null;

    public static function get_instance() {

        if ( self::$_instance == null ) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function pre_init() {

        parent::pre_init();

        // load
        if ( $this->is_gravityforms_supported() && class_exists( 'GF_Field' ) ) {

            // register field filters
            add_filter( 'gform_field_types_delete_files', array($this, 'filter_add_field_types_delete_files') );

            // load fields
            require_once( 'includes/class-slim-field-settings-base.php' );
            require_once( 'includes/class-slim-field-utils.php' );
            require_once( 'includes/class-slim.php' );
            require_once( 'includes/class-slim-field.php' );
            require_once( 'includes/class-slim-post-field.php' );
            require_once( 'includes/gv-slim-shortcode.php' );
        }
    }

    public function filter_add_field_types_delete_files($field_types) {
        return array_merge($field_types, array('slim', 'slim_post'));
    }

    public function init() {
        parent::init();
    }

    public function scripts() {
        $scripts = array(
            array(
                'handle'  => 'gf_slim_js',
                'src'     => $this->get_base_url() . '/js/slim.global.min.js',
                'version' => $this->_version,
                'callback' => array( 'GFSlimFieldService', 'setup' ),
                'enqueue' => array(
                    array( 'field_types' => array( 'slim', 'slim_post' ) ),
                ),
            ),
            array(
                'handle'  => 'gf_slim_scripts_js',
                'src'     => $this->get_base_url() . '/js/slim.scripts.js',
                'version' => $this->_version,
                'strings'  => array(
                    'currently_uploading' => __( 'Please wait for images to finish uploading.', 'slim-gravityforms' )
                ),
                'enqueue' => array(
                    array( 'field_types' => array( 'slim', 'slim_post' ) ),
                ),
            ),
        );
        return array_merge( parent::scripts(), $scripts );
    }

    public function styles() {
        $styles = array(
            array(
                'handle'  => 'gf_slim_css',
                'src'     => $this->get_base_url() . '/css/slim.min.css',
                'version' => $this->_version,
                'enqueue' => array(
                    array( 'field_types' => array( 'slim', 'slim_post' ) )
                )
            ),
            array(
                'handle'  => 'gf_slim_tweaks_css',
                'src'     => $this->get_base_url() . '/css/styles.css',
                'version' => $this->_version,
                'enqueue' => array(
                    array( 'field_types' => array( 'slim', 'slim_post' ) )
                )
            )
        );
        return array_merge( parent::styles(), $styles );
    }

}