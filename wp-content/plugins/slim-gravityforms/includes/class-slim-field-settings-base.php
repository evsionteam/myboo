<?php
/**
 * Class GFSlimFieldSettingsBase
 */
class GFSlimFieldSettingsBase {

    function __construct() {
        add_filter( 'gform_tooltips', array($this, 'tooltips') );
        add_action( 'gform_field_standard_settings', array( $this, 'standard_settings' ), 10, 2 );
        add_action( 'gform_field_advanced_settings', array( $this, 'advanced_settings' ), 10, 2 );
        add_action( 'gform_editor_js', array( $this, 'settings_script') );
    }

    public function get_standard_settings_position() {
        return null;
    }

    public function get_field_standard_settings() {
        return array();
    }

    public function get_advanced_settings_position() {
        return null;
    }

    public function get_field_advanced_settings() {
        return array();
    }

    private function map_editor_field_to_setting($field) {
        return $field['name'] . '_setting';
    }

    public function get_editor_field_settings() {
        return array_map(
            array($this, 'map_editor_field_to_setting'),
            $this->settings()
        );
    }

    public function standard_settings( $position, $form_id ) {
        if ($position != $this->get_standard_settings_position()) {
            return;
        }
        $this->echo_fields( $this->get_field_standard_settings() );
    }

    public function advanced_settings( $position, $form_id ) {

        if ($position != $this->get_advanced_settings_position()) {
            return;
        }
        $this->echo_fields( $this->get_field_advanced_settings() );
    }

    public function settings_script() {

        $fieldSettings = array();
        $fieldValues = array();
        $fields = $this->settings();

        foreach ($fields as $field) {
            $name = $field['name'];
            array_push($fieldSettings, '.' . $name . '_setting');
            array_push($fieldValues, 'jQuery(\'#field_' . $name . '_value\').val(field.image' . GFSlimFieldUtilities::setting_to_id($name) . ')');
        }
        ?>
        <script type='text/javascript'>

        // Adding setting to fields of type "slim"
        fieldSettings.slim += '<?php echo join(', ', $fieldSettings); ?>';

        // Binding to the load field settings event
        jQuery(document).bind('gform_load_field_settings', function(event, field, form) {
            <?php echo join('; ', $fieldValues); ?>
        });
        </script>
        <?php
    }

    public function tooltips( $tooltips ) {
        $fields = $this->settings();
        foreach ($fields as $field) {
            $tooltips = $this->add_tooltip($tooltips, $field);
        }
        return $tooltips;
    }

    private function settings() {
        return array_merge(
            $this->get_field_standard_settings(),
            $this->get_field_advanced_settings()
        );
    }

    private function add_tooltip( $tooltips, $field ) {
        $tooltips[ 'slim_tooltip_' . $field['name'] ] = sprintf(
            '<h6>%s</h6>%s',
            $field['label'],
            $field['tooltip']
        );
        return $tooltips;
    }

    private function echo_fields( $fields ) {
        foreach ($fields as $field) {
            $this->echo_field($field);
        }
    }

    private function echo_field( $field ) {

        $name = $field['name'];
        $label = $field['label'];
        $placeholder = isset($field['placeholder']) ? $field['placeholder'] : NULL;
        $description = isset($field['description']) ? is_callable($field['description']) ? $field['description']($field) : $field['description'] : NULL;
        $type = isset($field['type']) ? $field['type'] : NULL;
        $size = isset($field['size']) ? $field['size'] : NULL;
        $options = isset($field['options']) ? $field['options'] : NULL;

        $fieldSettingId = $name . '_setting';
        $fieldId = 'field_' . $name . '_value';
        $tooltipId = 'slim_tooltip_' . $name;
        $fieldPropertyId = 'image' . GFSlimFieldUtilities::setting_to_id($name);

        $attrOnChange = 'onchange="SetFieldProperty(\'' . $fieldPropertyId . '\', jQuery(this).val());"';
        $attrId = 'id="' . $fieldId . '"';
        $attrPlaceholder = 'placeholder="' . $placeholder . '"';
        $attrSize = isset($size) ? 'size="' . $size . '"' : 'class="fieldwidth-3"';
        $fieldAttributes = array($attrId, $attrOnChange);

        if (!isset($type)) {
            array_push($fieldAttributes, $attrPlaceholder);
            array_push($fieldAttributes, $attrSize);
            $field = '<input type="text" ' . join(' ', $fieldAttributes) .'/>';
        }
        else if ($type == 'textarea') {
            array_push($fieldAttributes, 'class="fieldwidth-3 fieldheight-2"');
            $field = '<textarea ' . join(' ', $fieldAttributes) .'></textarea>';
        }
        else if ($type == 'select') {
            $field = '<select ' . join(' ', $fieldAttributes) .'>';
            foreach ($options as $key => $value) {
                $field .= '<option value="' . $value['value']. '">' . $value['label'] .'</option>';
            }
            $field .= '</select>';
        }

        // get tooltip if any
        $tip = gform_tooltip($tooltipId, '', true);

        echo '
            <li class="' . $fieldSettingId . ' field_setting">
                <label for="' . $fieldId . '" class="section_label">
                    ' . $label . '
                    ' . $tip . '
                </label>
                ' . $field . '
                <div>' . $description . '</div>
            </li>
        ';
    }

}