<?php

class GFSlimFieldUtilities {

    public static function get_value_for_setting($input, $setting) {
        $value = GFSlimFieldUtilities::get_value_for_setting_name($input, $setting['name']);
        if ($value === null && isset($setting['placeholder'])) {
            return $setting['placeholder'];
        }
        return $value;
    }

    public static function get_value_for_setting_name($input, $setting) {
        $value = $input['image' . GFSlimFieldUtilities::setting_to_id($setting)];
        if (!isset($value) || strlen($value) === 0) {
            return null;
        }
        return $value;
    }

    public static function setting_to_id($setting) {
        $parts = explode('_', $setting);
        $output = '';
        foreach($parts as $part) {
            $output .= ucfirst($part);
        }
        return $output;
    }

    public static function setting_to_attribute($setting, $value) {
        return 'data-' . str_replace('slim-', '', str_replace('_', '-', $setting['name'])) . '="' . $value . '"';
    }

    public static function getImageByInputId($id) {
        $images = Slim::getImages("input_{$id}");
        if (count($images) == 0) {
            return false;
        }
        $image = array_shift($images);
        return $image['output'];
    }

    public static function getImage($input) {
        return GFSlimFieldUtilities::getImageByInputId($input->id);
    }

    public static function writeTempFile( $form_id , $file_name, $data ) {

        // path
        $target_root = GFSlimFieldUtilities::get_temp_upload_path( $form_id );

        // get temp file name
        $file_info = pathinfo( $file_name );
        $extension = rgar( $file_info, 'extension' );
        if ( ! empty( $extension ) ) {
            $extension = '.' . $extension;
        }
        $file_name = basename( $file_info['basename'], $extension );
        $file_name = sanitize_file_name( $file_name );

        $counter = 1;
        $target_path = $target_root . $file_name . $extension;
        while ( file_exists( $target_path ) ) {
            $target_path = $target_root . $file_name . "$counter" . $extension;
            $counter ++;
        }

        // Remove '.' from the end if file does not have a file extension
        $target_path = trim( $target_path, '.' );

        // Test if directory already exists
        if(!is_dir($target_root)){
            mkdir($target_root, 0755, true);
        }

        // save the temp file to disk, we'll make it final in the get_value_save_entry method
        file_put_contents($target_path, $data);

        // set correct permissions
        GFFormsModel::set_permissions($target_path);

        return array(
            'root' => $target_root,
            'path' => $target_path,
            'name' => basename($target_path)
        );
    }

    public static function get_temp_upload_path($form_id ) {
        return GFFormsModel::get_upload_path( $form_id ) . '/tmp/';
    }

    public static function getTempImage($field_id, $form_id, $image_name) {

        $info = pathinfo($image_name);
        $form_uid = GFFormsModel::get_form_unique_id( $form_id );

        return array(
            'url' => GFFormsModel::get_upload_url( $form_id ),
            'path' => GFSlimFieldUtilities::get_temp_upload_path( $form_id ),
            'name' => "{$form_uid}_{$field_id}.{$info['extension']}",
        );
    }

    public static function isJSON($str) {
        $start = substr( $str, 0, 2 );
        return $start == '{"' || $start == '{&';
    }

    public static function cleanExtension($extension) {
        return preg_replace('/[\.\s]+/', '', $extension);
    }

    public static function extensionToMimeType($extension) {
        $cleaned = strtolower(GFSlimFieldUtilities::cleanExtension($extension));
        if ($cleaned === 'jpg') {
            $cleaned = 'jpeg';
        }
        return 'image/' . $cleaned;
    }

    public static function extensionsToMimeTypes($extensions) {
        return array_unique(array_map(
            array('GFSlimFieldUtilities', 'extensionToMimeType'),
            explode(',', $extensions)
        ));
    }

    public static function get_mime_type($filename) {

        if (function_exists('mime_content_type')) {
            return mime_content_type($filename);
        }

        if (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            if (strpos($mimetype, ';') !== FALSE) {
                $mimetype = explode(';', $mimetype)[0];
            }
            return strtolower($mimetype);
        }

        $mime_types = array(
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml'
        );

        $parts = explode('.', $filename);
        $lower = array_pop($parts);
        $ext = strtolower($lower);

        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        }

        return 'application/octet-stream';
    }
}
