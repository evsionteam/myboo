<?php

if ( ! class_exists( 'GFForms' ) ) {
    die();
}





class GFSlimFieldService {

    public static function configure_woocommerce_gforms_strip_meta_html( $strip_html, $display_value, $field, $lead, $form_meta ) {
        $field_type = $field->get_input_type();
        if ( $field_type == 'slim' || $field_type == 'slim_post' ) {
            $strip_html = false;
        }
        return $strip_html;
    }

    public static function setup() {
        $protocol = isset( $_SERVER['HTTPS'] ) ? 'https://' : 'http://';
        wp_localize_script( 'gf_slim_js', 'gf_slim_ajax', array(
            'ajax_url'   => admin_url( 'admin-ajax.php', $protocol ),
        ) );
    }

    public static function save() {

        // get id of input field that posted the image
        $field_id = $_POST['field'];
        $form_id = $_POST['form'];
        $input_name = 'input_' . $field_id;

        // init
        $images = null;

        // Get posted data, if something is wrong, exit
        try {
            $images = Slim::getImages($input_name);
        }
        catch (Exception $e) {}

        // No image found under the supplied input name
        if (!is_array($images)) {

            Slim::outputJSON(array(
                'status' => SlimStatus::FAILURE,
                'message' => 'Unknown'
            ));

            return;
        }

        // Should always be one image (when posting async), so we'll use the first on in the array (if available)
        $image = array_shift($images);

        // get the name of the file
        $image_data = $image['output']['data'];
        $image_name = $image['output']['name'];

        // write a temp file
        $result = GFSlimFieldUtilities::writeTempFile( $form_id, $image_name, $image_data );

        // we return filename
        echo $result['name'];

        die();
    }

}

// Setup Ajax action hook
add_action( 'wp_ajax_nopriv_gf_slim_save', array( 'GFSlimFieldService', 'save' ) );
add_action( 'wp_ajax_gf_slim_save', array( 'GFSlimFieldService', 'save' ) );
add_filter( 'woocommerce_gforms_strip_meta_html', array( 'GFSlimFieldService', 'configure_woocommerce_gforms_strip_meta_html' ), 5, 10 );



class GFSlimFieldSettings extends GFSlimFieldSettingsBase {

    private static $_instance = null;

    public static function get_instance() {

        if ( self::$_instance == null ) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function get_standard_settings_position() { return 25; }

    public function get_advanced_settings_position() { return 50; }

    public function get_field_standard_settings() {
        return array(
            array(
                'name' => 'slim_ratio',
                'size' => 10,
                'label' => esc_html__('Output image ratio', 'slim-gravityforms'),
                'placeholder' => 'free',
                'description' => __(
                    '<p>Default is <b>"free"</b> (no forced ratio).</p>
                     <p>Set to <b>"input"</b> to use input image aspect ratio.</p>
                     <p>Set to number for a fixed ratio, for example <b>"1:1"</b> to force a square crop, or <b>"16:9"</b> for widescreen crop (ratio needs to consist of whole numbers).</p>', 'slim-gravityforms'),
                'tooltip' => esc_html__('The ratio the image can be cropped in', 'slim-gravityforms'),
                'sanitizer' => function($value) {
                    if (preg_match('/^free|input|[0-9]+:[0-9]+$/i', $value)) {
                        return strtolower($value);
                    }
                    return '';
                }
            ),
            array(
                'name' => 'slim_size',
                'size' => 15,
                'label' => esc_html__('Output image size', 'slim-gravityforms'),
                'description' => __('<p>By default Slim does not resize the output. Set to a <b>"width,height"</b> value. For example, <b>"400,400"</b> will resize all images to fit in a 400 by 400 pixel rectangle.</p>', 'slim-gravityforms'),
                'tooltip' => esc_html__('The bounding box the image will be scaled down to before upload', 'slim-gravityforms'),
                'sanitizer' => function($value) {
                    if (preg_match('/^[0-9]+,[0-9]+$/', $value)) {
                        return $value;
                    }
                    return '';
                }
            ),
            array(
                'name' => 'slim_min_size',
                'size' => 15,
                'label' => esc_html__('Minimum image size', 'slim-gravityforms'),
                'description' => __('<p>Minimum size of uploaded images. Set to a <b>"width,height"</b> value. Default minimum image size is <b>"100,100"</b>.</p>', 'slim-gravityforms'),
                'tooltip' => esc_html__('The minimum required dimensions of an image', 'slim-gravityforms'),
                'placeholder' => '100,100',
                'sanitizer' => function($value) {
                    if (preg_match('/^[0-9]+,[0-9]+$/', $value)) {
                        return $value;
                    }
                    return '';
                }
            ),
            array(
                'bind' => 'internal',
                'name' => 'slim_quality_warning',
                'size' => 15,
                'label' => esc_html__('Image quality warning', 'slim-gravityforms'),
                'description' => __('<p>Adds a warning below the drop area when an image is smaller than the given the defined size. Set to a <b>"width,height"</b> value. Default setting is empty, no warning.</p>', 'slim-gravityforms'),
                'tooltip' => esc_html__('The minimum resolution to be determined a quality image.', 'slim-gravityforms'),
                'sanitizer' => function($value) {
                    if (preg_match('/^[0-9]+,[0-9]+$/', $value)) {
                        return $value;
                    }
                    return '';
                }
            ),
            array(
                'label' => esc_html__('Instant edit mode', 'slim-gravityforms'),
                'name' => 'slim_instant_edit',
                'description' => __('<p>Open editor immediately when an image is dropped.</p>', 'slim-gravityforms'),
                'tooltip' => esc_html__('Opens the editor immediately when an image is dropped', 'slim-gravityforms'),
                'type' => 'select',
                'options' => array(
                    array('label' => 'No', 'value' => ''),
                    array('label' => 'Yes', 'value' => 'true')
                )
            ),
            array(
                'name' => 'slim_max_field_size',
                'size' => 15,
                'bind' => 'style',
                'label' => esc_html__('Maximum drop area size', 'slim-gravityforms'),
                'description' => __('<p>By default the Slim drop area will use up as much space as possible. Define a maximum width by supplying one value (e.g <b>"300"</b>), set max width and height by supplying two (e.g. <b>"300,300"</b>)</p>', 'slim-gravityforms'),
                'tooltip' => esc_html__('The maximum size of the drop area', 'slim-gravityforms'),
                'sanitizer' => function($value) {
                    if (preg_match('/^[0-9]+(,[0-9]+)*$/', $value)) {
                        return $value;
                    }
                    return '';
                }
            ),
            array(
                'name' =>  'slim_max_file_size',
                'size' => 3,
                'label' => esc_html__('Maximum file size', 'slim-gravityforms'),
                'description' => __('<p>The maximum file size in megabytes. A value of <b>"2.5"</b> means images larger than 2.5 megabytes are not allowed.</p><p>Maximum allowed on this server: <b>' . (wp_max_upload_size() / 1048576) . 'MB</b>.</p>', 'slim-gravityforms'),
                'tooltip' => esc_html__('The maximum file size of a dropped file', 'slim-gravityforms'),
                'placeholder' => wp_max_upload_size() / 1048576,
                'sanitizer' => function($value) {
                    if (preg_match('/^[0-9]+(\.[0-9]+)*$/', $value)) {
                        return $value;
                    }
                    return '';
                }
            ),
            array(
                'name' => 'slim_file_types',
                'bind' => 'input',
                'size' => 30,
                'label' => esc_html__('Allowed image formats', 'slim-gravityforms'),
                'description' => __('<p>A comma separated list of file extensions to allow.</p><p>For example <b>"png, jpg, jpeg, gif"</b>.</p>', 'slim-gravityforms'),
                'tooltip' => esc_html__('The image format a loaded image has to conform to', 'slim-gravityforms'),
                'sanitizer' => function($value) {
                    if (preg_match('/^(\.*[a-z]{3,4})(,\s*\.*[a-z]{3,4})*$/i', $value)) {
                        return strtolower($value);
                    }
                    return '';
                }
            ),
            array(
                'name' => 'slim_jpeg_compression',
                'size' => 3,
                'label' => esc_html__('JPEG compression', 'slim-gravityforms'),
                'description' => __('<p>If a JPEG is uploaded this controls the compression amount. Set to <b>"0"</b> for lowest image quality and to <b>"100"</b> for best image quality.</p><p>Default is controlled by browser and averages around 94 percent.</p>', 'slim-gravityforms'),
                'tooltip' => esc_html__('Controls the compression of a loaded or converted JPEG image', 'slim-gravityforms'),
                'placeholder' => '94',
                'sanitizer' => function($value) {
                    if (preg_match('/^[0-9]{1,3}$/', $value) && intval($value) <= 100) {
                        return $value;
                    }
                    return '';
                }
            ),
            array(
                'name' => 'slim_force_type',
                'size' => 4,
                'label' => esc_html__('Force output format', 'slim-gravityforms'),
                'description' => __('</p>Set to either <b>"jpg"</b> to convert all images to JPEGs or to <b>"png"</b> to save all images as PNGs.</p>', 'slim-gravityforms'),
                'tooltip' => __('All loaded images will be converted to the set output format', 'slim-gravityforms'),
                'type' => 'select',
                'options' => array(
                    array('label' => 'none', 'value' => ''),
                    array('label' => 'png', 'value' => 'png'),
                    array('label' => 'jpeg', 'value' => 'jpg')
                )
            )
        );
    }

    public function get_field_advanced_settings() {
        return array(

            // switch to sync upload
            array(
                'bind' => 'internal',
                'label' => esc_html__('Send on submit', 'slim-gravityforms'),
                'name' => 'slim_sync_upload',
                'description' => __('
                    <p>By default Slim uploads files when the user taps the submit button. When uploading large images this can however cause problems on iOS devices.</p>
                    <p>You can then either set the "output image size" to a lower value or set "Send on submit" field to false.</p>
                    <p>When set to false Slim will immediately upload files when they\'re dropped on the drop area. During upload the submit button will be disabled.</p>', 'slim-gravityforms'),
                'tooltip' => esc_html__('Enables sync uploading when set to true', 'slim-gravityforms'),
                'type' => 'select',
                'options' => array(
                    array('label' => 'True', 'value' => ''),
                    array('label' => 'False', 'value' => 'false')
                )
            ),
            // max image size
            array(
                'name' => 'slim_internal_canvas_size',
                'size' => 15,
                'label' => esc_html__('Maximum internal canvas size', 'slim-gravityforms'),
                'description' => __('<p>Some devices have limited memory and therefor cannot load super high resolution images. To cope with big amounts of data you can set the maximum internal canvas size Slim uses to load data, images that are bigger are scaled down to fit this size.</p><p>Set to a <b>"width,height"</b> value. Default maximum image size is <b>"4096,4096"</b>.</p>', 'slim-gravityforms'),
                'tooltip' => esc_html__('The maximum size of images handled by Slim', 'slim-gravityforms'),
                'placeholder' => '4096,4096',
                'sanitizer' => function($value) {
                    if (preg_match('/^[0-9]+,[0-9]+$/', $value)) {
                        return $value;
                    }
                    return '';
                }
            ),

            // labels
            array(
                'name' => 'slim_label',
                'label' => esc_html__('Drop area label', 'slim-gravityforms'),
                'placeholder' => esc_attr__('Drop your image here', 'slim-gravityforms'),
                'tooltip' => esc_html__('Label of the drop area.', 'slim-gravityforms'),
            ),
            array(
                'name' => 'slim_button_confirm_label',
                'label' => esc_html__('Button confirm label', 'slim-gravityforms'),
                'placeholder' => esc_attr__('Confirm', 'slim-gravityforms'),
                'tooltip' => esc_html__('Label of confirm button.', 'slim-gravityforms'),
                'size' => 15
            ),
            array(
                'name' => 'slim_button_cancel_label',
                'label' => esc_html__('Button cancel label', 'slim-gravityforms'),
                'placeholder' => esc_attr__('Cancel', 'slim-gravityforms'),
                'tooltip' => esc_html__('Label of cancel button.', 'slim-gravityforms'),
                'size' => 15
            ),
            array(
                'name' => 'slim_button_rotate_title',
                'label' => esc_html__('Button rotate tooltip', 'slim-gravityforms'),
                'placeholder' => esc_attr__('Rotate', 'slim-gravityforms'),
                'tooltip' => esc_html__('Tooltip of rotate button.', 'slim-gravityforms'),
                'size' => 15
            ),
            array(
                'name' => 'slim_button_edit_title',
                'label' => esc_html__('Button edit tooltip', 'slim-gravityforms'),
                'placeholder' => esc_attr__('Edit', 'slim-gravityforms'),
                'tooltip' => esc_html__('Tooltip of edit button.', 'slim-gravityforms'),
                'size' => 15
            ),
            array(
                'name' => 'slim_button_remove_title',
                'label' => esc_html__('Button remove tooltip', 'slim-gravityforms'),
                'placeholder' => esc_attr__('Remove', 'slim-gravityforms'),
                'tooltip' => esc_html__('Tooltip of remove button.', 'slim-gravityforms'),
                'size' => 15
            ),

            // client status
            array(
                'name' => 'slim_status_no_support',
                'label' => esc_html__('Slim browser support', 'slim-gravityforms'),
                'placeholder' => esc_attr__('Unfortunately your browser does not support image cropping.', 'slim-gravityforms'),
                'tooltip' => esc_html__('Error shown by Slim when the users browser is too old and does not support file cropping.', 'slim-gravityforms')
            ),
            array(
                'name' => 'slim_status_file_size',
                'label' => esc_html__('Slim file too big', 'slim-gravityforms'),
                'placeholder' => esc_attr__('File is too big, maximum file size: $0 MB.', 'slim-gravityforms'),
                'tooltip' => esc_html__('Error shown by Slim when file size exceeds max file size', 'slim-gravityforms'),
                'description' => __('<p>$0 is automatically replaced with maximum file size.</p>', 'slim-gravityforms')
            ),
            array(
                'name' => 'slim_status_file_type',
                'label' => esc_html__('Slim file wrong type', 'slim-gravityforms'),
                'placeholder' => esc_attr__('Invalid file type, expects: $0.', 'slim-gravityforms'),
                'tooltip' => esc_html__('Error shown by Slim when file is of wrong type', 'slim-gravityforms'),
                'description' => __('<p>$0 is automatically replaced with expected file types.</p>', 'slim-gravityforms')
            ),
            array(
                'name' => 'slim_status_image_too_small',
                'label' => esc_html__('Slim image too small', 'slim-gravityforms'),
                'placeholder' => esc_attr__('Image is too small, minimum size is: $0 pixels.', 'slim-gravityforms'),
                'tooltip' => esc_html__('Error shown by Slim when image is too small', 'slim-gravityforms'),
                'description' => __('<p>$0 is automatically replaced with minimum image size.</p>', 'slim-gravityforms')
            ),
            array(
                'name' => 'slim_status_unknown_response',
                'label' => esc_html__('Server error message', 'slim-gravityforms'),
                'placeholder' => esc_attr__('An unknown error occurred', 'slim-gravityforms'),
                'tooltip' => esc_html__('Message shown when an error occurs during data transfer.', 'slim-gravityforms')
            ),
            array(
                'name' => 'slim_status_upload_success',
                'label' => esc_html__('Image saved message', 'slim-gravityforms'),
                'placeholder' => esc_attr__('Image saved successfully', 'slim-gravityforms'),
                'tooltip' => esc_html__('Message shown when file saved to server.', 'slim-gravityforms')
            ),
            array(
                'bind' => 'internal',
                'name' => 'slim_status_image_quality_low',
                'label' => esc_html__('Slim image quality low', 'slim-gravityforms'),
                'placeholder' => __('The quality of this image is quite low, a resolution of at least $0 pixels is advised.', 'slim-gravityforms'),
                'tooltip' => esc_html__('Error shown by Slim when file is deemed low quality', 'slim-gravityforms'),
                'description' => __('<p>$0 is automatically replaced with minimum quality image size.</p>', 'slim-gravityforms')
            )
        );
    }

};

GFSlimFieldSettings::get_instance();



/**
 * Class Slim_GF_Field
 */
class Slim_GF_Field extends GF_Field {


    /**
     * @var string $type The field type.
     */
    public $type = 'slim';



    // # FORM EDITOR & FIELD MARKUP -------------------------------------------------------------------------------------

    /**
     * Return the field title, for use in the form editor.
     *
     * @return string
     */
    public function get_form_editor_field_title() {
        return esc_attr__( 'Slim', GF_SLIM_ADDON_SLUG );
    }

    /**
     * Assign the field button to the Advanced Fields group.
     *
     * @return array
     */
    public function get_form_editor_button() {
        return array(
            'group' => 'advanced_fields',
            'text'  => $this->get_form_editor_field_title(),
        );
    }

    /**
     * Returns settings for this field
     * // custom method
     * @return array
     */
    public function get_slim_field_settings() {
        return array_merge(
            GFSlimFieldSettings::get_instance()->get_field_standard_settings(),
            GFSlimFieldSettings::get_instance()->get_field_advanced_settings()
        );
    }

    /**
     * The settings which should be available on the field in the form editor.
     *
     * @return array
     */
    function get_form_editor_field_settings() {
        return array_merge(
            array(
                'label_setting',
                'description_setting',
                'rules_setting',
                'input_class_setting',
                'css_class_setting',
                'admin_label_setting',
                'visibility_setting',
                'conditional_logic_field_setting',
                'label_placement_setting'
            ),
            GFSlimFieldSettings::get_instance()->get_editor_field_settings()
        );
    }

    /**
     * Enable this field for use with conditional logic.
     *
     * @return bool
     */
    public function is_conditional_logic_supported() {
        return true;
    }


    /**
     * Define the fields inner markup.
     *
     * @param array $form The Form Object currently being processed.
     * @param string|array $value The field value. From default/dynamic population, $_POST, or a resumed incomplete submission.
     * @param null|array $entry Null or the Entry Object currently being edited.
     *
     * @return string
     */
    public function get_field_input( $form, $value = '', $entry = null ) {

        $lead_id         = absint( rgar( $entry, 'id' ) );
        $id              = absint( $this->id );
        $form_id         = absint( $form['id'] );
        $is_entry_detail = $this->is_entry_detail();
        $is_form_editor  = $this->is_form_editor();

        // Prepare the value of the input ID attribute.
        $input_name      = 'input_' . $id;
        $field_id        = $is_entry_detail || $is_form_editor || $form_id == 0 ? "input_$id" : 'input_' . $form_id . "_$id";
        $value           = esc_attr( $value );

        // Get the value of the inputClass property for the current field.
        $inputClass = $this->inputClass;

        // Prepare the input classes.
        $size         = $this->size;
        $class_suffix = $is_entry_detail ? '_admin' : '';
        $class        = $size . $class_suffix . ' ' . $inputClass;

        // Prepare the other input attributes.
        $tabindex              = $this->get_tabindex();
        $logic_event           = ! $is_form_editor && ! $is_entry_detail ? $this->get_conditional_logic_event( 'keyup' ) : '';
        $required_attribute    = $this->isRequired ? 'aria-required="true"' : '';
        $invalid_attribute     = $this->failed_validation ? 'aria-invalid="true"' : 'aria-invalid="false"';
        $disabled_text         = $is_form_editor ? 'disabled="disabled"' : '';

        // Prepare the input tag for this field.
        $slim_attributes = array();
        $slim_styles = array();
        $slim_scripts = array();
        $input_attributes = array();


        // Get Slim attribute values
        $fieldSettings = $this->get_slim_field_settings();
        foreach ($fieldSettings as $setting) {

            if (isset($setting['bind']) && $setting['bind'] != 'slim') {
                continue;
            }

            $settingValue = GFSlimFieldUtilities::get_value_for_setting($this, $setting);
            if ($settingValue == null) {
                continue;
            }
            array_push(
                $slim_attributes,
                GFSlimFieldUtilities::setting_to_attribute($setting, $settingValue)
            );
        }


        // Get Slim style values
        if (isset($this->imageSlimMaxFieldSize) && strlen($this->imageSlimMaxFieldSize) > 0) {
            $parts = explode(',', $this->imageSlimMaxFieldSize);
            $maxSize = 'max-width:' . $parts[0] . 'px;';
            if (isset($parts[1])) {
                $maxSize .= 'max-height:' . $parts[1] . 'px';
            }
            array_push($slim_styles, $maxSize);
        }
        if (count($slim_styles) > 0) {
            array_push(
                $slim_attributes,
                'style="' . join($slim_styles, ';') . '"'
            );
        }


        // Get accepted mime types
        if (isset($this->imageSlimFileTypes) && strlen($this->imageSlimFileTypes) > 0) {
            $accept = 'accept="' . join(',', GFSlimFieldUtilities::extensionsToMimeTypes($this->imageSlimFileTypes)) . '"';
            array_push($input_attributes, $accept);
        }


        $slim_initial_image = '';
        if ($this->imageSlimSyncUpload != true && !empty($value) && !GFSlimFieldUtilities::isJSON($value)) {

            $merge_parts = explode( '|:|', $value );
            $value = array_shift( $merge_parts );

            // get safe url
            $file_url = $this->get_download_url($value);
            $file_name = pathinfo($value)['basename'];

            // we want to load the image
            $slim_initial_image = "<img src='$file_url' data-filename='$file_name' class='slim-initial-image' alt=''/>";

            // we don't want to load a url value to the hidden input
            $value = '';

            // we do want to save the initial image data
            array_push($slim_attributes, 'data-save-initial-image="true"');

        }

        // get url?
        if ($this->imageSlimSyncUpload == true && !empty($value)) {

            $value = array_shift( explode( '|:|', $value ) );

            $file_name = pathinfo($value)['basename'];
            $slim_initial_image = "<img src='$value' data-filename='$file_name' alt=''/>";

        }

        // if we're in the admin add handle delete file method
        if ($is_entry_detail) {
            array_push($slim_attributes, "data-did-remove='handle_delete_file_{$id}'");
            array_push($slim_scripts, "function handle_delete_file_{$id}() { DeleteFile({$lead_id}, {$id}, this); }");
        }

        // add init method
        array_push($slim_scripts, "
            function slim_create_{$id}() { if (typeof window.Slim !== 'undefined') { Slim.create(document.getElementById('slim_{$field_id}')) } }
            if (document.readyState === 'complete') { slim_create_{$id}() } else { document.addEventListener('DOMContentLoaded', slim_create_${id}) }
        ");

        // add async upload
        if ($this->imageSlimSyncUpload == true) {
            array_push($slim_attributes,'data-service="slim_send_'. $id . '"');
            array_push($slim_attributes,'data-push="true"');
            array_push($slim_scripts, "
                function slim_send_{$id}(data, progress, success, failure) {
                
                    // field id
                    data.append('field', '{$id}');
                    data.append('form', '{$form_id}');
                    
                    // action handler on server
                    data.append('action', 'gf_slim_save');
                
                    // do request
                    var xhr = new XMLHttpRequest();
                    xhr.open('POST', gf_slim_ajax.ajax_url, true);
                    xhr.upload.onprogress = function(e) {
                        progress(e.loaded, e.total);
                    }
                    xhr.onload = function() {
                        success(xhr.response);
                    }
                    xhr.onerror = function() {
                        failure(xhr.statusText);
                    }
                    xhr.send(data);
                    
                }
            ");
        }


        // add quality warning logic
        if ($this->imageSlimQualityWarning) {

            $parts = explode(',',$this->imageSlimQualityWarning);
            $minWidth = $parts[0];
            $minHeight = $parts[1];


            $warning = $this->imageSlimStatusImageQualityLow;
            if (isset($warning) && strlen($warning) === 0) {
                $warning = __('The quality of this image is quite low, a resolution of at least $0 pixels is advised.', 'slim-gravityforms');
            }
            $warning = str_replace('$0', $minWidth . ', ' . $minHeight, $warning);

            array_push($slim_attributes,'data-did-load="slim_quality_warn_'. $id . '"');
            array_push($slim_attributes,'data-did-remove="slim_quality_warn_clean_'. $id . '"');

            array_push($slim_scripts,"
                function slim_quality_warn_{$id}(file, image, meta) {
                    
                    // test image dimensions
                    var width = parseInt(image.width, 10);
                    var height = parseInt(image.height, 10);
                        
                    if (image.width < {$minWidth} || image.height < {$minHeight}) {
                        
                        // create message
                        var message = document.createElement('p');
                        message.className = 'slim-quality-warning';
                        message.id = 'slim_quality_warning_{$id}';
                        message.textContent = '{$warning}';
                        
                        // append message
                        this.element.parentNode.appendChild(message);
                        
                    }
                    else {
                        slim_quality_warn_clean_{$id}();
                    }
                    
                    // always fine, we only warn and don't prevent the load
                    return true;
                }
            ");

            array_push($slim_scripts,"
                function slim_quality_warn_clean_{$id}() {
                    var node = document.getElementById('slim_quality_warning_{$id}');
                    if (!node) {
                        return;
                    }
                    node.parentNode.removeChild(node);
                }
            ");

        }

        // render Slim HTML
        $slim_settings = join(' ', $slim_attributes);
        $input_settings = join(' ', $input_attributes);
        $slim_script_body = join(';', $slim_scripts);

        // slim input element
        $input = "
            <div id='slim_{$field_id}' 
                 class='slim {$class}' {$slim_settings}>

                 {$slim_initial_image}

                 <input id='{$field_id}' 
                        type='file' 
                        {$input_settings} 
                        {$disabled_text} 
                        {$tabindex} 
                        {$logic_event} />

                 <input type='hidden' 
                        value='{$value}' 
                        name='{$input_name}' 
                        {$required_attribute} 
                        {$invalid_attribute}/>

            </div>
        ";

        // slim initialisation script
        $script = "<script>{$slim_script_body}</script>";

        return sprintf("<div class='ginput_container ginput_container_%s'>%s %s</div>",
            $this->type,
            $input,
            $script
        );
    }






    // # SUBMISSION -----------------------------------------------------------------------------------------------------
    public function is_value_submission_empty( $form_id )
    {
        $input_name = 'input_' . $this->id;
        return empty( $_POST[$input_name] );
    }


    /**
     * Perform custom validation logic.
     *
     * Return the result (bool) by setting $this->failed_validation.
     * Return the validation message (string) by setting $this->validation_message.
     *
     * @param string|array $value The field value from get_value_submission().
     * @param array        $form  The Form Object currently being processed.
     */
    public function validate( $value, $form ) {

        // continue
        $id              = absint( $this->id );
        $form_id         = absint( $form['id'] );
        $input_name      = 'input_' . $id;

        // test if has received a value
        if (empty($value)) {
            return;
        }

        // if the upload path is contained in the value, the value was not changed, no need to validate
        if (strpos($value, GFFormsModel::get_upload_path( $form['id'] ))) {
            return;
        }


        // default errors
        $fileupload_validation_message = esc_html__('An unknown error occurred while processing the image.', 'slim-gravityforms');


        // get allowed mime types
        $allowed_mimetypes = empty($this->imageSlimFileTypes) ? array() : GFSlimFieldUtilities::extensionsToMimeTypes(strtolower($this->imageSlimFileTypes));


        // get maximum defined file size
        $max_file_size = $this->imageSlimMaxFileSize;
        $max_upload_size_in_bytes = !empty($max_file_size) && $max_file_size > 0 ? $max_file_size * 1048576 : wp_max_upload_size();
        $max_upload_size_in_mb = $max_upload_size_in_bytes / 1048576;


        // get images from post
        if (GFSlimFieldUtilities::isJSON($value)) {

            $image = null;
            try {
                $image = GFSlimFieldUtilities::getImage( $this );
            } catch (Exception $e) {
                $this->validation_message = empty($this->errorMessage) ? $fileupload_validation_message : $this->errorMessage;
                return;
            }

            // no images received (but should have been received, so is basically the same as unknown error)
            if ($image === FALSE) {
                $this->validation_message = empty($this->errorMessage) ? $fileupload_validation_message : $this->errorMessage;
                return;
            }

            // get the image data
            $image_data = $image['data'];
            $image_name = $image['name'];

            // write to temp file
            $result = GFSlimFieldUtilities::writeTempFile( $form_id, $image_name, $image_data );
            $target = $result['path'];
            session_start();     
  					$_SESSION['uploaded_file'] = $target;

        }
        else {
            $name = $value;
            $path = GFSlimFieldUtilities::get_temp_upload_path( $form_id );
            $target = $path . $name;
            session_start();     
  					$_SESSION['uploaded_file'] = $target;
        }


        // source file no longer exists (happens when form submit is refreshed, temp file has then already been unlinked)
        if (!file_exists($target)) {
            return;
        }


        // validate temp file
        if (isset($max_file_size) && filesize($target) > $max_upload_size_in_bytes) {
            $this->failed_validation = true;
            $this->validation_message = sprintf(esc_html__('Image exceeds size limit. Maximum file size: %dMB', 'slim-gravityforms'), $max_upload_size_in_mb);
            return;
        }

        // validate mime type against extensions
        $mime_type = GFSlimFieldUtilities::get_mime_type($target);
        if (count($allowed_mimetypes) > 0 && !in_array($mime_type, $allowed_mimetypes)) {
            $this->failed_validation = true;
            $this->validation_message = sprintf(esc_html__('Image does not match required type. Must be one of these types: %s', 'slim-gravityforms'), $this->imageSlimFileTypes);
        }

    }


    /**
     * Sanitize and format the value before it is saved to the Entry Object.
     *
     * @param string $value      The value to be saved.
     * @param array  $form       The Form Object currently being processed.
     * @param string $input_name The input name used when accessing the $_POST.
     * @param int    $lead_id    The ID of the Entry currently being processed.
     * @param array  $lead       The Entry Object currently being processed.
     *
     * @return array|string The safe value.
     */
    public function get_value_save_entry( $value, $form, $input_name, $lead_id, $lead ) {

        if (empty($value)) {
            // remove files previously associated with this field
            return '';
        }

        // if the upload path is contained in the value this is the original file, the value was not changed
        $form_uid = wp_hash( $form['id'] );
        if (strpos($value, $form_uid) !== false ) {
            return $value;
        }

        // if is posted JSON string
        if (GFSlimFieldUtilities::isJSON($value)) {

            // get images from post
            $image = GFSlimFieldUtilities::getImage($this);

            // no images received (which is fine)
            if ($image === FALSE) {
                return '';
            }

            // handle image data
            $imageData = $image['data'];
            $imageName = Slim::sanitizeFileName($image['name']);

            // Create upload path
            $target = GFFormsModel::get_file_upload_path($this->formId, $imageName);

            // save the file to disk
            Slim::save($imageData, $target['path']);

            // set correct permissions
            GFFormsModel::set_permissions($target['path']);

        }
        else {

            $imageName = $value;

            // get source
            $source = GFSlimFieldUtilities::get_temp_upload_path( $this->formId ) . $imageName;

            // Create upload path
            $target = GFFormsModel::get_file_upload_path($this->formId, $imageName);

            // source file no longer exists (happens when form submit is refreshed, temp file has then already been unlinked)
            if (!file_exists($source)) {
                return '';
            }

            // move file from temp location to upload path
            copy($source, $target['path']);

            // remove temp file
            unlink($source);

        }

        //
        // !! start > fixes problem with gravity view where the return value of this method does not update the field
        //
        $_POST[$input_name] = $target['url'];
        //
        // !! end
        //

        // return url of new file
        return $target['url'];

    }




    // # ADMIN -----------------------------------------------------------------------------------------------------

    public function get_value_entry_list($value, $entry, $field_id, $columns, $form)
    {
        if (empty($value)) {
            return $value;
        }

        $file_path = $value;

        // we render a thumbnail icon for the image
        $icon_url = GFEntryList::get_icon_url($file_path);

        // we render the file name for the image
        $file_url = esc_attr( $this->get_download_url($file_path) );

        return "
        <a href='{$file_url}' 
           target='_blank' 
           title='" . esc_attr__('Click to view', 'slim-gravityforms') . "'>
           <img src='$icon_url' alt=''/>
        </a>";
    }


    public function get_value_entry_detail($value, $currency = '', $use_text = false, $format = 'html', $media = 'screen')
    {
        if (empty($value)) {
            return $value;
        }

        $file_path = $value;
        $file_path_info = pathinfo($file_path);
        $file_name = $file_path_info['basename'];

        // no fun on 1.x
        $force_download = method_exists($this, 'get_modifiers') ? in_array('download', $this->get_modifiers()) : false;
        $file_url = $this->get_download_url($value, $force_download);

        $field_ssl = apply_filters('gform_secure_file_download_is_https', true, $file_url, $this);

        if (GFCommon::is_ssl() && strpos($file_url, 'http:') !== false && $field_ssl === true) {
            $file_url = str_replace('http:', 'https:', $file_url);
        }

        $file_url = esc_attr(str_replace(' ', '%20', apply_filters('gform_fileupload_entry_value_file_path', $file_url, $this)));
        $click_to_view_text = esc_attr__('Click to view image', 'slim-gravityforms');


        return $format == 'text' ? $file_path . PHP_EOL : '
            <a href="' . $file_url . '" target="_blank" title="' . $click_to_view_text . '">
                <span>' . $file_name . '</span>
                <img src="' . $file_url . '" alt=""/>
            </a>';
    }





    // # HELPERS -----------------------------------------------------------------------------------------------------

    /**
     * Forces settings into expected values while saving the form object.
     *
     * No escaping should be done at this stage to prevent double escaping on output.
     *
     * Currently called only for forms created after version 1.9.6.10.
     *
     */
    public function sanitize_settings()
    {
        parent::sanitize_settings();

        // loop over custom settings and sanitize input
        $fieldSettings = $this->get_slim_field_settings();
        foreach ($fieldSettings as $setting) {
            $setting_name = $setting['name'];
            $value = $this['image' . GFSlimFieldUtilities::setting_to_id($setting_name)];
            if (isset($setting['sanitizer'])) {
                $this['image' . GFSlimFieldUtilities::setting_to_id($setting_name)] = $setting['sanitizer']($value);
            }
        }

    }

    /**
     * Returns the download URL for a file. The URL is not escaped for output.
     *
     * @access public
     *
     * @param string $file The complete file URL.
     *
     * @return string
     */
    public function get_download_url($file, $force_download = false)
    {
        // is older version of gravity forms
        if (!method_exists($this, 'get_modifiers')) {
            return $file;
        }

        $download_url = $file;
        $secure_download_location = true;

        /**
         * By default the real location of the uploaded file will be hidden and the download URL will be generated with
         * a security token to prevent guessing or enumeration attacks to discover the location of other files.
         *
         * Return FALSE to display the real location.
         *
         * @param bool $secure_download_location If the secure location should be used.  Defaults to true.
         * @param string $file The URL of the file.
         * @param GF_Field_FileUpload $this The Field
         */
        $secure_download_location = apply_filters('gform_secure_file_download_location', $secure_download_location, $file, $this);
        $secure_download_location = apply_filters('gform_secure_file_download_location_' . $this->formId, $secure_download_location, $file, $this);

        if (!$secure_download_location) {

            /**
             * Allow filtering of the download URL.
             *
             * Allows for manual filtering of the download URL to handle conditions such as
             * unusual domain mapping and others.
             *
             * @since 2.1.1.1
             *
             * @param string $download_url The URL from which to download the file.
             * @param GF_Field_FileUpload $field The field object for further context.
             */
            return apply_filters('gform_secure_file_download_url', $download_url, $this);

        }

        $upload_root = GFFormsModel::get_upload_url($this->formId);
        $upload_root = trailingslashit($upload_root);

        // Only hide the real URL if the location of the file is in the upload root for the form.
        // The upload root is calculated using the WP Salts so if the WP Salts have changed then file can't be located during the download request.

        if (strpos($file, $upload_root) !== false) {
            $file = str_replace($upload_root, '', $file);
            $download_url = site_url('index.php');
            $args = array(
                'gf-download' => urlencode($file),
                'form-id' => $this->formId,
                'field-id' => $this->id,
                'hash' => GFCommon::generate_download_hash($this->formId, $this->id, $file),
            );
            if ($force_download) {
                $args['dl'] = 1;
            }
            $download_url = add_query_arg($args, $download_url);
        }

        /**
         * Allow filtering of the download URL.
         *
         * Allows for manual filtering of the download URL to handle conditions such as
         * unusual domain mapping and others.
         *
         * @param string $download_url The URL from which to download the file.
         * @param GF_Field_FileUpload $field The field object for further context.
         */
        return apply_filters('gform_secure_file_download_url', $download_url, $this);
    }
}











GF_Fields::register( new Slim_GF_Field( array() ) );