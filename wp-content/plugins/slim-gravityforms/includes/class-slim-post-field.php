<?php

if ( ! class_exists( 'GFForms' ) ) {
    die();
}

/**
 * Class Slim_Post_GF_Field Post Helper
 */
class Slim_Post_Helper {

    function __construct() {
        add_filter( 'gform_post_data', array($this, 'handle_post_data'), 10, 3);
        add_filter( 'gform_after_submission', array( $this , 'handle_submission', ), 10, 2  );
    }

    // make sure Slim is added to images post data
    function handle_post_data($post_data, $form, $entry) {

        foreach ( $form['fields'] as $field ) {

            // skip fields that are not slim_post
            if ( $field->type !== 'slim_post' ) {
                continue;
            }

            $value = $entry[$field->id];

            $ary         = ! empty( $value ) ? explode( '|:|', $value ) : array();
            $url         = count( $ary ) > 0 ? $ary[0] : '';
            $title       = count( $ary ) > 1 ? $ary[1] : '';
            $caption     = count( $ary ) > 2 ? $ary[2] : '';
            $description = count( $ary ) > 3 ? $ary[3] : '';

            array_push($post_data['images'], array(
                    'field_id' => $field->id,
                    'url' => $url,
                    'title' => $title,
                    'description' => $description,
                    'caption' => $caption
                )
            );

        }

        return $post_data;
    }

    function handle_submission($entry, $form) {

        foreach ( $form['fields'] as $field ) {

            // skip fields that are not slim_post
            if ( $field->type !== 'slim_post' ) {
                continue;
            }

            // get attachment id
            $value = $entry[$field->id];
            $ary         = ! empty( $value ) ? explode( '|:|', $value ) : array();
            $attachment_id = count( $ary ) > 4 ? $ary[4] : '';

            // get custom field name
            $custom_field_name = $field->postCustomFieldName;
            if (!isset($custom_field_name) || strlen($custom_field_name) === 0) {
                continue;
            }

            // add to custom field
            add_post_meta( $entry['post_id'], $custom_field_name, $attachment_id );

        }

    }
}

new Slim_Post_Helper();


/**
 * Class Slim_Post_GF_Field
 */
class Slim_Post_GF_Field extends Slim_GF_Field {


    /**
     * @var string $type The field type.
     */
    public $type = 'slim_post';



    // # FORM EDITOR & FIELD MARKUP -------------------------------------------------------------------------------------

    /**
     * Return the field title, for use in the form editor.
     *
     * @return string
     */
    public function get_form_editor_field_title() {
        return esc_attr__( 'Slim', GF_SLIM_ADDON_SLUG );
    }

    /**
     * Assign the field button to the Advanced Fields group.
     *
     * @return array
     */
    public function get_form_editor_button() {
        return array(
            'group' => 'post_fields',
            'text'  => $this->get_form_editor_field_title(),
        );
    }


    /**
     * The settings which should be available on the field in the form editor.
     *
     * @return array
     */
    function get_form_editor_field_settings() {

        $settings = parent::get_form_editor_field_settings();

        array_push($settings, 'post_custom_field_setting');
        array_push($settings, 'post_image_setting');
        array_push($settings, 'post_image_featured_image');

        return $settings;

    }


    /**
     * Enable this field for use with conditional logic.
     *
     * @return bool
     */
    public function is_conditional_logic_supported() {
        return true;
    }


    /**
     * Define the fields inner markup.
     *
     * @param array $form The Form Object currently being processed.
     * @param string|array $value The field value. From default/dynamic population, $_POST, or a resumed incomplete submission.
     * @param null|array $entry Null or the Entry Object currently being edited.
     *
     * @return string
     */
    function get_field_input($form, $value = '', $entry = null) {

        // if $value is in array format, only use the image for the slim field
        $inputValue = $value;
        if (is_array($value)) {
            $inputValue = $value[$this->id];
        }

        // create the Slim snippet
        $snippet = parent::get_field_input($form, $inputValue, $entry);

        $id              = (int) $this->id;
        $form_id         = $form['id'];
        $is_entry_detail = $this->is_entry_detail();
        $is_form_editor  = $this->is_form_editor();
        $is_admin        = $is_entry_detail || $is_form_editor;
        $class_suffix    = $is_entry_detail ? '_admin' : '';
        $field_id        = $is_entry_detail || $is_form_editor || $form_id == 0 ? "input_$id" : 'input_' . $form_id . "_$id";
        $disabled_text   = $is_form_editor ? 'disabled="disabled"' : '';

        if ($is_entry_detail && isset($entry)) {
            return "<div>You can <a href='post.php?action=edit&post={$entry['post_id']}'>edit this post</a> from the post page.</div>";
        }

        $title       = esc_attr( rgget( $this->id . '.1', $value ) );
        $caption     = esc_attr( rgget( $this->id . '.4', $value ) );
        $description = esc_attr( rgget( $this->id . '.7', $value ) );

        $hidden_style      = "style='display:none;'";

        $title_style       = ! $this->displayTitle && $is_admin ? $hidden_style : '';
        $caption_style     = ! $this->displayCaption && $is_admin ? $hidden_style : '';
        $description_style = ! $this->displayDescription && $is_admin ? $hidden_style : '';

        $tabindex = $this->get_tabindex();
        $title_field = $this->displayTitle || $is_admin ? sprintf( "<span class='ginput_full$class_suffix ginput_post_image_title' $title_style><input type='text' name='input_%d.1' id='%s_1' value='%s' $tabindex %s/><label for='%s_1'>" . gf_apply_filters( array( 'gform_postimage_title', $form_id ), __( 'Title', 'slim-gravityforms' ), $form_id ) . '</label></span>', $id, $field_id, $title, $disabled_text, $field_id ) : '';

        $tabindex = $this->get_tabindex();
        $caption_field = $this->displayCaption || $is_admin ? sprintf( "<span class='ginput_full$class_suffix ginput_post_image_caption' $caption_style><input type='text' name='input_%d.4' id='%s_4' value='%s' $tabindex %s/><label for='%s_4'>" . gf_apply_filters( array( 'gform_postimage_caption', $form_id ), __( 'Caption', 'slim-gravityforms' ), $form_id ) . '</label></span>', $id, $field_id, $caption, $disabled_text, $field_id ) : '';

        $tabindex = $this->get_tabindex();
        $description_field = $this->displayDescription || $is_admin ? sprintf( "<span class='ginput_full$class_suffix ginput_post_image_description' $description_style><input type='text' name='input_%d.7' id='%s_7' value='%s' $tabindex %s/><label for='%s_7'>" . gf_apply_filters( array( 'gform_postimage_description', $form_id ), __( 'Description', 'slim-gravityforms' ), $form_id ) . '</label></span>', $id, $field_id, $description, $disabled_text, $field_id ) : '';

        return "<div class='ginput_complex$class_suffix ginput_container ginput_container_slim_post'>" . $snippet . $title_field . $caption_field . $description_field . '</div>';

    }

    public function validate($value, $form)
    {
        $image = $value[$this->id];
        parent::validate($image, $form);
    }

    public function get_value_save_entry( $value, $form, $input_name, $lead_id, $lead ) {

        $url = parent::get_value_save_entry( $value, $form, $input_name, $lead_id, $lead );

        if (empty($url)) {
            return '';
        }

        $image_title       = isset( $_POST["{$input_name}_1"] ) ? wp_strip_all_tags( $_POST["{$input_name}_1"] ) : '';
        $image_caption     = isset( $_POST["{$input_name}_4"] ) ? wp_strip_all_tags( $_POST["{$input_name}_4"] ) : '';
        $image_description = isset( $_POST["{$input_name}_7"] ) ? wp_strip_all_tags( $_POST["{$input_name}_7"] ) : '';

        return $url . '|:|' . $image_title . '|:|' . $image_caption . '|:|' . $image_description;
    }







    public function get_value_submission( $field_values, $get_from_post_global_var = true ) {

        $value[ $this->id ] = $this->get_input_value_submission( 'input_' . $this->id , $get_from_post_global_var );
        $value[ $this->id . '.1' ] = $this->get_input_value_submission( 'input_' . $this->id . '_1', $get_from_post_global_var );
        $value[ $this->id . '.4' ] = $this->get_input_value_submission( 'input_' . $this->id . '_4', $get_from_post_global_var );
        $value[ $this->id . '.7' ] = $this->get_input_value_submission( 'input_' . $this->id . '_7', $get_from_post_global_var );

        return $value;
    }

    public function get_value_merge_tag( $value, $input_id, $entry, $form, $modifier, $raw_value, $url_encode, $esc_html, $format, $nl2br ) {
        list( $url, $title, $caption, $description ) = array_pad( explode( '|:|', $value ), 4, false );
        switch ( $modifier ) {
            case 'title' :
                return $title;

            case 'caption' :
                return $caption;

            case 'description' :
                return $description;

            default :
                return str_replace( ' ', '%20', $url );
        }
    }





    public function get_value_entry_list( $value, $entry, $field_id, $columns, $form ) {
        list( $url, $title, $caption, $description ) = rgexplode( '|:|', $value, 4 );
        if ( ! empty( $url ) ) {
            $thumb = GFEntryList::get_icon_url( $url );
            $value = "<a href='" . esc_attr( $url ) . "' target='_blank' title='" . __( 'Click to view', 'slim-gravityforms' ) . "'><img src='$thumb'/></a>";
        }
        return $value;
    }

    public function get_value_entry_detail( $value, $currency = '', $use_text = false, $format = 'html', $media = 'screen' ) {
        $ary         = explode( '|:|', $value );
        $url         = count( $ary ) > 0 ? $ary[0] : '';
        $title       = count( $ary ) > 1 ? $ary[1] : '';
        $caption     = count( $ary ) > 2 ? $ary[2] : '';
        $description = count( $ary ) > 3 ? $ary[3] : '';

        if ( ! empty( $url ) ) {
            $url = str_replace( ' ', '%20', $url );

            switch ( $format ) {
                case 'text' :
                    $value = $url;
                    $value .= ! empty( $title ) ? "\n\n" . $this->label . ' (' . __( 'Title', 'slim-gravityforms' ) . '): ' . $title : '';
                    $value .= ! empty( $caption ) ? "\n\n" . $this->label . ' (' . __( 'Caption', 'slim-gravityforms' ) . '): ' . $caption : '';
                    $value .= ! empty( $description ) ? "\n\n" . $this->label . ' (' . __( 'Description', 'slim-gravityforms' ) . '): ' . $description : '';
                    break;

                default :
                    $value = "<a href='$url' target='_blank' title='" . __( 'Click to view', 'slim-gravityforms' ) . "'><img src='$url' width='100' /></a>";
                    $value .= ! empty( $title ) ? "<div>Title: $title</div>" : '';
                    $value .= ! empty( $caption ) ? "<div>Caption: $caption</div>" : '';
                    $value .= ! empty( $description ) ? "<div>Description: $description</div>" : '';

                    break;
            }
        }

        return $value;
    }

}

GF_Fields::register( new Slim_Post_GF_Field( array() ) );