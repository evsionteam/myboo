<?php
/**
 * @param $atts
 * @return string
 */
/*

// set source url for image
[gv_slim_field source="{FieldName:FieldId}"]

// turn image into a view entry link
[gv_slim_field view_entry="{entry_id}"]

// hide/show image name
[gv_slim_field show_name="true"]

// hide/show image itself
[gv_slim_field show_image="false"]

// turn image into a lightbox link
[gv_slim_field lightbox="true"]

*/
function gv_slim_field_register_shortcode( $atts ) {

    $atts = shortcode_atts( array(
        'view_entry' => NULL,
        'source' => NULL,
        'lightbox' => 'false',
        'show_name' => 'false',
        'show_image' => 'true'
    ), $atts, 'message' );

    // get parameters
    $entry_id = $atts['view_entry'];
    $file_url = $atts['source'];
    $file_name = basename($atts['source']);
    $lightbox = $atts['lightbox'] === 'true';
    $link = null;
    $link_wrapper_open = '';
    $link_wrapper_close = '';

    // append view entry to url
    if (isset($entry_id)) {
        $link = get_permalink() . 'entry/' . $entry_id . '/';
        $link_wrapper_open = '<a href="' . $link . '">';
        $link_wrapper_close = '</a>';
    }

    // echo error if no source supplied
    if (!isset($file_url)) {
        return 'Slim is missing the shortcode source attribute, [gv_slim_field source="{FieldName:FieldId}"]';
    }

    // render lightbox link
    if ($lightbox === true) {
        $link_wrapper_open = '<a href="' . $file_url . '" class="thickbox">';
        $link_wrapper_close = '</a>';
    }

    // name is hidden by default
    $file_name = $atts['show_name'] == 'true' ? '<span class="gv-slim-name">' . $file_name . '</span>' : '';

    // image is visible by default
    $file_url = $atts['show_image'] == 'false' ? '' : '<img class="gv-slim-image" src="' . $file_url . '" alt=""/>';

    return "{$link_wrapper_open}{$file_name}{$file_url}{$link_wrapper_close}";
}

add_shortcode( 'gv_slim_field', 'gv_slim_field_register_shortcode' );