(function($) {

	// get translations
	var strings = gf_slim_scripts_js_strings || {};

	$(document).bind('gform_post_render', function(e, formID){

		var $form = $("form#gform_" + formID);

		// no form found, exit!
		if ($form.length === 0) { return; }

		// handle submit
		$form.submit(function() {

			var pendingUploads = false;

			// test if all slim fields are either idle or done uploading
			var nodes = document.querySelectorAll('.slim');
			var i = 0;
			var l = nodes.length;
			for (;i<l;i++) {
				var instance = Slim.find(nodes[i]);
				if (!instance) {
					continue;
				}
				var state = instance.element.getAttribute('data-state') || '';
				if (state.indexOf('upload') !== -1) {
					pendingUploads = true;
				}
			}

			// if currently busy uploading, block submit and show message that we are waiting for the upload to finish
			if (pendingUploads) {

				alert(strings.currently_uploading);

				window["gf_submitting_" + formID] = false;

				$('#gform_ajax_spinner_' + formID).remove();

				return false;
			}

		});

	});

}(jQuery));