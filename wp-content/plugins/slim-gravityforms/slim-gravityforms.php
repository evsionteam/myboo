<?php
/**
 * Plugin Name: Slim Image Cropper for Gravity Forms
 * Plugin URI: http://wordpress.slimimagecropper.com
 * Description: Slim Image Cropper for Gravity Forms is a cross platform Image Cropping and Uploading plugin. It’s very easy to setup and features beautiful graphics and animations.
 * Version: 1.4.3
 * Author: PQINA
 * Author URI: http://pqina.nl
 * License: GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: slim-gravityforms
 */

// sets global gf slim version for use in script embeds
define( 'GF_SLIM_ADDON_SLUG', 'slim-gravityforms' );
define( 'GF_SLIM_ADDON_VERSION', '1.4.3' );

// wait for gravity forms to load
add_action( 'gform_loaded', array( 'GF_Slim_AddOn_Bootstrap', 'load' ), 5 );

// sets up the slim addon
class GF_Slim_AddOn_Bootstrap {

    public static function load() {

        if ( ! method_exists( 'GFForms', 'include_addon_framework' ) ) {
            return;
        }

        require_once( 'class-slim-addon.php' );

        GFAddOn::register( 'GFSlimAddOn' );

    }

}