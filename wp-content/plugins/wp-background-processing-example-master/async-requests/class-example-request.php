<?php

class WP_Example_Request extends WP_Async_Request {

	use WP_Example_Logger;

	/**
	 * @var string
	 */
	protected $action = 'example_request';

	/**
	 * Handle
	 *
	 * Override this method to perform any actions required
	 * during the async request.
	 */
	protected function handle() {
		$order_id =  $_POST['order'];     error_log( '$order_id ' .$order_id, 1, "lucia.arsena@gmail.com" );
     $message = $this->createBook();
		//$this->really_long_running_task();
		$this->log( $message );

     $order = wc_get_order( $order_id );
    $mailer = WC()->mailer();
$mails = $mailer->get_emails();
if ( ! empty( $mails ) ) {
    foreach ( $mails as $mail ) {
        if ( $mail->id == 'supplier_order_email' ) {
           $mail->trigger( $order_id );
        }
     }
}


	}
  

protected function createBook(){  
$generated_books_rel_path = '/myboo/generated-books-high-res'; 
$template_books_rel_path = '/myboo/template-books-high-res/';

	$time = time();
  	$uploads = wp_upload_dir();  
  	if ( !defined('ABSPATH') )
	  	define('ABSPATH', dirname(__FILE__) . '/');
  	$abspath= ABSPATH; 
  	
    $title = 'la-favola-del-mago-del-tempo';
    
$customization = 'bambino = Mario {Cognome}, occhi = neri, capelli = biondi lisci corti, pelle = bianca, peluche = bambola, nome donna = Irene, capelli donna = biondi lisci corti, occhi donna = neri, pelle donna = bianca';
    
   	$generated_book_dir = $uploads['basedir'] .$generated_books_rel_path;
  	$generated_book_url = $uploads['baseurl'] .$generated_books_rel_path .'/' .date('Y', $time) .'/' .date('m', $time) .'/' .$title;
  	$generated_book_path = $generated_book_dir .'/' .date('Y', $time) .'/' .date('m', $time) .'/' .$title;

	$template_book_dir = $uploads['basedir'] .$template_books_rel_path .$title;
  	$path = site_url();
  
  	require_once(get_template_directory() . '/theme-extension/db-controller/db-controller.php');
	require(get_template_directory() . '/theme-extension/ebook-generator/textbox.php');
	
	/*
  $query_book_val = $_REQUEST['query_book_title'];
  	$query_select_var = $_REQUEST['query_select_var'];
  	$query_select_val = $_REQUEST['query_select_val'];
  	$text_select_var = $_REQUEST['text_select_var'];
  	$text_select_val = $_REQUEST['text_select_val'];
	$customization = $_REQUEST['customization'];
  	$query_var_num = count($query_select_var);
  	$text_var_num = count($text_select_var);
  	$query_cond = "(booktitle = '" .$query_book_val ."' AND field = '' AND value = '')";
  
  	for($i = 0; $i < $query_var_num; $i++):
		$query_cond = $query_cond ." OR " ."(booktitle = '" .$query_book_val ."' AND field = '" .$query_select_var[$i] ."' AND " ."value = '" .$query_select_val[$i] ."')";
  	endfor;
      
  	$filename = $_REQUEST[$title] .'-' .time();   */
   
   $filename = 'pippo';
   
   
   
  	$db_handle = new DBController();    
  	$query = "SELECT type, page, layer, file FROM books WHERE (booktitle = 'La Favola del Mago del Tempo' AND field = '' AND value = '') OR (booktitle = 'La Favola del Mago del Tempo' AND field = 'sesso-bambino&pelle-bambino' AND value = 'bambino&bianca') OR (booktitle = 'La Favola del Mago del Tempo' AND field = 'occhi-bambino' AND value = 'neri') OR (booktitle = 'La Favola del Mago del Tempo' AND field = 'colore-capelli-bambino&acconciatura-bambino&lunghezza-bambino' AND value = 'biondi&lisci&corti') OR (booktitle = 'La Favola del Mago del Tempo' AND field = 'pupazzo' AND value = 'bambola') OR (booktitle = 'La Favola del Mago del Tempo' AND field = 'occhi-donna' AND value = 'neri') OR (booktitle = 'La Favola del Mago del Tempo' AND field = 'colore-capelli-donna&acconciatura-donna&lunghezza-donna' AND value = 'biondi&lisci&corti') OR (booktitle = 'La Favola del Mago del Tempo' AND field = 'pelle-donna' AND value = 'bianca') ORDER BY page, layer ASC"; 

  	$result = $db_handle->runQuery($query);
  
  	$pdf = new PDF_TextBox('L','cm',array(20,20));
    $pdf->AddFont('Satisfy','','Satisfy-Regular.php');
	$pdf->SetMargins(0, 0);
  	$pdf->SetAutoPageBreak(true, 0);
  	$page = 0;

  	$i = 0;
  	foreach($result as $row):
    	$i++;
    	$prev_page = $page;
    	$page = $row["page"];
    	if ( $page > $prev_page):  
      		$pdf->AddPage();
    	endif;    
    	if ($row["type"] === 'image'):  
      		$file = $template_book_dir .'/' .$row["file"];
      		$pdf->Image($file,0,0,5,5);
    	elseif ($row["type"] === 'text'):  
      		$text = file_get_contents($template_book_dir .'/' .$row["file"]);
      		//for($j = 0; $j < $text_var_num; $j++): 
        //		$text = str_replace($text_select_var[$j], $text_select_val[$j], $text);
      	//	endfor; 
			$pdf->SetFont('Satisfy','', 6);
    		$pdf->SetXY(1, 1);
			$pdf->drawTextBox($text, 18, 18, 'C', 'M', false);
		endif;
  	endforeach; 
  	if (!file_exists($generated_book_path )) {
    	wp_mkdir_p($generated_book_path);
  	}
	$book_url =  $generated_book_url .'/' .$filename .'.pdf';
    $book_path =  $generated_book_path .'/' .$filename .'.pdf';  
  	
	session_start();     
  	$_SESSION['book_link'] = $book_url;
  	$_SESSION['query'] = $query;
	$_SESSION['customization'] = $customization;

	$pdf->Output('F', $book_path);
	return $book_url;

}

}