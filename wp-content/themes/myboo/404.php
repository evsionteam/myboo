<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package My_Boo
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			
			<div class="container">
				<div class="col-md-12">
				
					<section class="error-404 not-found text-center">
						
						<h1>404</h1>
						<h2>Page Not Found</h2>
						<a href="<?php echo site_url(); ?>" class="c-btn c-btn-primary">Go to Home</a>
						
					</section><!-- .error-404 -->

				</div><!-- /.col-md-12 -->
			</div><!-- /.container -->

			<!-- FOR SVG WAVE ANIMATION -->
			<?php get_template_part('template-parts/content', 'svg-wave') ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
