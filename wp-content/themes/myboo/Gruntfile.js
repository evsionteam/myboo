module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // COMPASS
        compass: {
            aboveFold: {
                options: { // Target options
                    sassDir: './assets/src/sass/above-fold',
                    cssDir: './assets/build/css/',
                    environment: 'development',
                    outputStyle: 'compact',
                    sourcemap: true
                }
            },
            style: {
                options: { // Target options
                    sassDir: './assets/src/sass/below-fold',
                    cssDir: './assets/build/css/',
                    environment: 'development',
                    outputStyle: 'compact',
                    sourcemap: true
                }
            },
            bootstrap: {
                options: { // Target options
                    sassDir: './assets/src/sass/vender',
                    cssDir: './assets/src/css/',
                    environment: 'development',
                    outputStyle: 'compact',
                    sourcemap: false
                }
            }
        },

        // CSS AND JS CONCAT
        concat: {
            // options: {
            //     separator: "\n \n /*** New File ***/ "
            // },
            vendor: {
                src: [
                    './assets/src/css/bootstrap.css',
                    './node_modules/font-awesome/css/font-awesome.css'
                ],
                dest: './assets/build/css/vendor.css'
            },
            css: {
                src: [
                    './assets/build/css/vendor.css',
                    './node_modules/owl-carousel-2/assets/owl.carousel.css',
                    './node_modules/jquery.mmenu/dist/jquery.mmenu.all.css',
                    './assets/build/css/main.css'
                ],
                dest: './assets/build/css/main.css'
            },
            js: {
                src: [
                    './node_modules/jquery.nicescroll/dist/jquery.nicescroll.js',
                    './node_modules/jquery.mmenu/dist/jquery.mmenu.all.js',
                    './node_modules/owl-carousel-2/owl.carousel.js',
                    './assets/src/js/scroll-down.js',
                    './assets/src/js/skip-link-focus-fix.js',
                    './assets/src/js/mrMobileMenu.js',
                    './assets/src/js/bootstrap.js',
                    './assets/src/js/morph.js',
                    './assets/src/js/custom.js',
                    './assets/src/js/main.js',
                ],
                dest: './assets/build/js/main.js'
            }

        },

        // MINIFIE CSS
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1,
                keepSpecialComments: 0,
                sourceMap: false
            },
            target: {
                files: [{
                    expand: true,
                    cwd: './assets/build/css/',
                    src: ['*.css', '!*.min.css'],
                    dest: './assets/build/css/',
                    ext: '.min.css'
                }]
            }
        },

        // MIINIFY JS
        uglify: {
            options: {
                report: 'gzip'
            },
            main: {
                src: ['./assets/build/js/main.js'],
                dest: './assets/build/js/main.min.js'
            }
        },

        // COPY CONTENT
        copy: {
            img: {
                files: [{
                    expand: true,
                    cwd: './assets/src/img',
                    src: '**',
                    dest: './assets/build/img/',
                    filter: 'isFile'
                }]
            },
            fonts: {
                files: [{
                    expand: true,
                    cwd: './node_modules/font-awesome/fonts',
                    src: '**',
                    dest: './assets/build/fonts/',
                    filter: 'isFile'
                }]
            }
        },

        // GRUNT WATCH
        watch: {
            sass: {
                files: [
                    './assets/src/sass/*.scss',
                    './assets/src/sass/**/*.scss',
                    '../assets/src/sass/**/**/*.scss'
                ],
                tasks: ['css']
            },

            img: {
                files: [
                    './assets/src/img/*'
                ],
                tasks: ['img']
            },

            js: {
                files: [
                    './assets/src/js/*.js'
                ],
                tasks: ['js']
            }

        }

    });

    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');

    //register grunt default task
    grunt.registerTask('css', ['compass:style', 'compass:aboveFold']);
    grunt.registerTask('js', ['concat']);
    grunt.registerTask('img', ['copy']);

    grunt.registerTask('default', ['compass', 'concat', 'cssmin', 'uglify', 'copy']);
}