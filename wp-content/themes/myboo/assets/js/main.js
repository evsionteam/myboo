+(function ($) {
  var ui = function () {

    this.init = function () {
      this.waveAnimation();
      var _this = this;
      this.niceScroll();
      this.mobileMenu();

      /* mobile slider for stories */
      if (jQuery('.stories').length > 0) {
        this.owlCarousel();
      }
    };  


    /* ---------------------------------------------------------
     *  Home page and inner page Wave animation
     * --------------------------------------------------------- */
    this.waveAnimation = function () {
      var tl = new TimelineMax({ repeat: -1, yoyo: true });
      var tl2 = new TimelineMax({ repeat: -1, yoyo: true });
      tl.to("#line-one", 4.2234343, { morphSVG: "#line-two", ease: Sine.easeInOut });
      tl2.to("#line-four", 5.632324, { morphSVG: "#line-three", ease: Sine.easeInOut });
    };


    /* ---------------------------------------------------------
     *  Adding niceScroll for changing site scroll bar design
     * --------------------------------------------------------- */
    this.niceScroll = function () {
      var windowWidth = jQuery(window).width();
      if (windowWidth > 767) {
        jQuery("html").niceScroll({
          cursorcolor: "#1eb281",
          cursorwidth: "10px",
          cursorborder: "none",
          cursorborderradius: "10px",
          cursoropacitymin: 1,
          mousescrollstep: 80,
          zindex: 99,
          background: '#ddd',
        });
      }
    }

    /* ---------------------------------------------------------
     *  Stories slider on mobile device only
     * --------------------------------------------------------- */
    this.owlCarousel = function () {
      var _this = this,
        isInit = null;
      var windowWidth = jQuery(window).width();
      if (windowWidth < 767) {
        _this.initOwlCarousel();
        isInit = true;
      } else {
        _this.destroyOwlCarousel();
        isInit = false;
      }

      jQuery(window).resize(function () {
        windowWidth = jQuery(window).width();

        if (windowWidth < 767 && !isInit) {
          isInit = true;
          window.location.reload(); /* Reloading browser once for reinitialize slider*/
        }
        if (windowWidth > 769 && isInit) {
          _this.destroyOwlCarousel();
          isInit = false;
        }
      });
    }

    this.initOwlCarousel = function () {
      jQuery('#stories').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        items: 1,
        loop: true,
        margin: 0,
        dots: false,
        navText: ['<i class="fa fa-arrow-left></i>', '<i class="fa fa-arrow-left></i>']
      });
    };

    this.destroyOwlCarousel = function () {
      jQuery('#stories').trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
    };

    /* ---------------------------------------------------------
     *  Mobile menu
     * --------------------------------------------------------- */
    this.mobileMenu = function () {
      jQuery('#mobile-menu').mmenu({
        extensions: ["fullscreen", "listview-large", "fx-panels-slide-up", "fx-listitems-drop", "border-offset"],
        offCanvas: {
          position: "bottom",
          zposition: "top",
        },
        navbar: {
          title: ""
        },
        navbars: [{
          height: 2,
          content: [
            '<a href="' + main.home_url + '"><img src="' + main.logo + '" /></a>'
          ]
        }]
      });

      var API = $("#mobile-menu").data("mmenu");
      $(document).on('click', '.fixed-menu-close', function () {
        API.close();
      });

      $(".fixed-menu-close").click(function () {
        API.open();
      });

      $(window).load(function () {
        alert('added');   
        jQuery('body').append('<div class="fixed-menu-close"><i class="fa fa-close"></i></div>');
      });
    }

  };

  $(document).ready(function () {
    new ui().init();
  });

})(jQuery)