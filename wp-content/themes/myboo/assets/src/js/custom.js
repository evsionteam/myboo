/* ----------------------------------------------------------
 * HOME PAGE SLIDER
 * ---------------------------------------------------------- */
+(function($) {
  var Slider = (function() {
      function Slider(config) {
          this.activeClassName = 'active';
          this.sliderName = config.selector || '#our-slider';
          this.prevBtn = config.prevBtn || '#btn-prev';
          this.nextBtn = config.nextBtn || '#btn-next';
          this.setTotalSlider();
      }
      Slider.prototype.setTotalSlider = function() {
          this.totalSlider = $(this.sliderName).children().length;
          this.totalSlider && this.enableNav();
      };
      Slider.prototype.enableNav = function() {
          var self = this;
          self.setDetailInfo();
          // For Next button
          $(document).on('click', this.nextBtn, function() {
              var firstChild = $(self.sliderName).children().first().clone();
              $(self.sliderName).children().first().remove();
              $(self.sliderName).append(firstChild);
              self.setDetailInfo();
          });
          // For Prev button
          $(document).on('click', this.prevBtn, function() {
              var lastChild = $(self.sliderName).children().last().clone();
              $(self.sliderName).children().last().remove();
              $(self.sliderName).prepend(lastChild);
              self.setDetailInfo();
          });
      };
      Slider.prototype.setDetailInfo = function() {
          var dataVal = $($(this.sliderName).children()[2]).data(),
              $moreInfo = $(this.sliderName).parent().next();
          $moreInfo.children().removeClass('slide-in');
          var time = setTimeout(function() {
              $moreInfo.find('h1').text(dataVal.title);
              $moreInfo.find('p').text(dataVal.description);
              $moreInfo.find('a').text(dataVal.link);
              $moreInfo.children().addClass('slide-in');
          }, 300);
      };
      return Slider;
  }()); /* End slider class*/
  $(document).ready(function() {
      var sliderConfig = {
          selector: '#our-slider',
          prevBtn: '#btn-prev',
          nextBtn: '#btn-next'
      };
      var S = new Slider(sliderConfig);
  });
})(jQuery);


/* ----------------------------------------------------------
* SCROLL DOWN SLIDER
* ---------------------------------------------------------- */
+(function($) {
  $.fn.scrollDown = function() {
      this.click(function(e) {
          e.preventDefault();
          var $this = $(this),
              $element = $('body,html'), //don't select html to scroll it's won't work
              target = $this.attr('data-target'),
              reduceHeightAttr = $this.attr('data-reduce-offset'),
              animationDuration = $this.attr('data-animation-duration'),
              //elementOffset
              targetOffset = $(target).offset().top,
              //settingDefaultValue
              animationDurationValue = (typeof animationDuration != 'undefined') ? parseInt(animationDuration) : 5000,
              //calculatingActualOffset
              actualOffset = targetOffset - reduceHeight(reduceHeightAttr);

          function reduceHeight(param) {
              var reduceHeight;
              if ($(param).length > 0) {
                  reduceHeight = $(param).outerHeight();
                  return reduceHeight;
              } else if ($.isNumeric(param)) {
                  return parseInt(param);
              }
              return 0;
          }

          //scrollAnimation
          $element.stop().animate({
              scrollTop: actualOffset
          }, animationDurationValue);

      });
  };

  $(document).ready(function() {
      $('.scroll-btn').scrollDown();
  });

})(jQuery)


/* ----------------------------------------------------------
* Custome upload file
* ---------------------------------------------------------- */
+(function($) {
  $(document).ready(function() {
      var $file = $('.c-file-upload');
      $(document).on('click', '.next-button', function() {
          if ($file.length > 0 && !$(document).find('.c-file-upload').hasClass('added')) {
              $(document)
                  .find('.c-file-upload')
                  .addClass('added')
                  .find('input[type="file"]')
                  .hide()
                  .parent()
                  .prev()
                  .append('<span class="file-name"></span><span class="c-btn-file">Choose File</span>');
          }
      });

      function previewPhoto(input) {

          if (input.files && input.files[0]) {
              var reader = new FileReader();
              $(reader).on("load", function(eLoad) {
                  $(document)
                      .find('.c-file-upload')
                      .prev()
                      .find('img')
                      .attr("src", eLoad.originalEvent.target.result);
              });
              reader.readAsDataURL(input.files[0]);
          }
      }

      $(document).on('change', '.c-file-upload input[type="file"]', function(e) {
          var fileName = $(this).get(0).files[0].name;
          $(this).parent().prev().find('.file-name').text(fileName);
          previewPhoto(this);
      });

  });
})(jQuery);