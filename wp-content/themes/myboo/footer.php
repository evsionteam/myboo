<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package My_Boo
 */
?>

	</div><!-- #content -->

		<footer id="colophon" class="site-footer">
		   <section class="footer-top-section">
		      <div class="container">
		         <div class="col-md-6 col-xs-12 col-sm-6">
		            <div class="newsletter">
		               <h1>Rimani in contatto </h1>
		               <p>Lascia la tua mail per ricere la nostra newsletter.</p>
		            </div>
		         </div>
		         <div class="col-md-6 col-xs-12 col-sm-6 menu-section">
		            <div class="col-md-4 col-sm-4 col-xs-4">
		               <div class="footer-right-content">
		                  <h3>MYBOO</h3>
		                  <ul>
		                     <li><a href="#">Chi siamo</a></li>
		                     <li><a href="#">Le nostre storie</a></li>
		                  </ul>
		               </div>
		            </div>
		            <div class="col-md-4 col-sm-4 col-xs-4">
		               <div class="footer-right-content">
		                  <h3>Social</h3>
		                  <ul>
		                     <li><a href="#">Facebook</a></li>
		                     <li><a href="#">Instagram</a></li>
		                     <li><a href="#">pinterest</a></li>
		                  </ul>
		               </div>
		            </div>
		            <div class="col-md-4 col-sm-4 col-xs-4">
		               <div class="footer-right-content">
		                  <h3>ASSISTENZA</h3>
		                  <ul>
		                     <li><a href="#">Contattaci</a></li>
		                     <li><a href="#">FAQ</a></li>
		                  </ul>
		               </div>
		            </div>
		          </div><!-- right nav -->
		         </div><!-- container -->
		         <div class="line"></div>
		         <div class="container">
			         <div class="col-md-12 footer-logo-social clearfix">
			            <div class="footer-logo clearfix">
			               <div class="col-md-6 col-xs-12 col-sm-6">
			                  <img src="<?php bloginfo('template_directory'); ?>/assets/build/img/footer-logo.png">
			               </div>
			               <div class="col-md-6">
			                  <div class="social-nav clearfix">
			                     <ul>
			                        <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/assets/build/img/fb.png"></a></li>
			                        <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/assets/build/img/insta.png"></a></li>
			                        <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/assets/build/img/pin.png"></a></li>
			                     </ul>
			                  </div>
			               </div>
			            </div>
			         </div><!-- social icon -->
			         <!-- col-12 -->
		         </div><!-- container-->
		   </section>
		   <div class="site-info">
		      <div class="container">
		         <div class="footer-bottom clearfix">
		                <ul>
		                   <li><a href="#">TERMINI & CONDIZIONI</a></li>
		                   <li><a href="#">PRIVACY POLICY</a></li>
		                </ul>
		            </div><!-- footer buttom -->
		        </div>
		      <!-- container -->
		   </div>
	   <!-- .site-info -->
	</footer><!-- footer -->
<!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
