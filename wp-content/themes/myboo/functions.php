<?php
/**
 * Sets up theme defaults and registers support for various WordPress features.
 */
add_action( 'after_setup_theme', 'myboo_setup' );
function myboo_setup() {
	/* Make theme available for translation. */
	load_theme_textdomain( 'myboo', get_template_directory() . '/languages' );

	/* Add default posts and comments RSS feed links to head. */
	add_theme_support( 'automatic-feed-links' );

	/* Let WordPress manage the document title. */
	add_theme_support( 'title-tag' );

	/* Enable support for Post Thumbnails on posts and pages. */
	add_theme_support( 'post-thumbnails' );

	/* This theme uses wp_nav_menu() in one location. */
	register_nav_menus( array(
		'primary-menu' => esc_html__( 'Primary', 'myboo' ),
	) );

	/* Switch default core markup for search form, comment form, and comments to output valid HTML5. */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/* Add theme support fochanger selective refresh for widgets. */
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add theme support for woocommerce
	 */
	add_theme_support( 'woocommerce' );
}


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
add_action( 'after_setup_theme', 'myboo_content_width', 0 );
function myboo_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'myboo_content_width', 640 );
}

/**
 * Register widget area.
 */
add_action( 'widgets_init', 'myboo_widgets_init' );
function myboo_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'myboo' ),
		'id'            => 'primary-sidebar',
		'description'   => esc_html__( 'Add widgets here.', 'myboo' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}

/* Enqueue scripts and styles. */
add_action( 'wp_enqueue_scripts', 'myboo_scripts' );
function myboo_scripts() {
	wp_enqueue_style( 'myboo', get_stylesheet_uri() );
	wp_enqueue_style( 'myboo-fonts', '//fonts.googleapis.com/css?family=Lato:100,300,300italic,400,400italic,700,900|Quicksand');

	wp_enqueue_style('above-css', get_stylesheet_directory_uri() .'/assets/build/css/above-fold.css', array(),'20151215', false );
	wp_enqueue_style('vendor-css', get_stylesheet_directory_uri() .'/assets/build/css/vendor.css', array(),'20151215', false );
	wp_enqueue_style('main-css', get_stylesheet_directory_uri() .'/assets/build/css/main.css', array(),'20151215', false );

	wp_enqueue_script( 'myboo-skip-link-focus-fix', get_stylesheet_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script('tweenMax','//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js', array('jquery'),false );	

	wp_enqueue_script('main-js', get_stylesheet_directory_uri() .'/assets/build/js/main.js', array('tweenMax'),false );	

	wp_localize_script( 
						'main-js',
						'main',
						 array('home_url'=> home_url(),'logo' => home_url().'/wp-content/themes/myboo/assets/build/img/logo.png' )
						);
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}



/* Custom template tags for this theme. */
require get_template_directory() . '/inc/template-tags.php';

/* Functions which enhance the theme by hooking into WordPress. */
require get_template_directory() . '/inc/template-functions.php';

require get_template_directory() . '/inc/woo-changes.php';

include get_template_directory() . '/theme-extension/theme-extend.php';
