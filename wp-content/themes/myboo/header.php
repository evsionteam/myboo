<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package My_Boo
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'myboo' ); ?></a>
    
	<section class="myboo-header" id="myboo-header">

		<header id="masthead" class="site-header">
			 <div class="container"> 

				<div class="col-xs-6 col-md-3 col-sm-3">
					<!-- <?php
					the_custom_logo();
					if ( is_front_page() && is_home() ) : ?>
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php else : ?>
						<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<?php
					endif;

					$description = get_bloginfo( 'description', 'display' );
					if ( $description || is_customize_preview() ) : ?>
						<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
					<?php
					endif; ?> -->
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"> <img src="<?php bloginfo('template_directory'); ?>/assets/build/img/logo.png"  alt=""></a>
				</div><!-- /.col-xs-6 -->

	            <div class="col-xs-6 col-md-9 col-sm-9">

					<div class="col-md-9 myboo-nav-wrapper">
						<nav id="site-navigation" class="main-navigation">
							<?php
								wp_nav_menu( array(
									'theme_location' => 'primary-menu',
									'menu_id'        => 'primary-menu'
 								) );
							?>
						</nav><!-- #site-navigation -->
					</div><!-- /.myboo-nav-wrapper -->

					<div class="visible-xs" id="mobile-menu" style="display:none;">

						<?php
							wp_nav_menu( array(
								'theme_location' => 'primary-menu',
								'menu_id'        => 'primary-menu'
							) );
						?>
					</div>

					<div class="col-md-3 cart-language">
						<div class="yellow-circle"></div>

						<!-- Mobile menu toggler -->
						<a href="#mobile-menu" class="fixed-menu-open visible-xs" ><div class="fa fa-bars"></div></a>
						
						<ul>
							<?php 
								global $woocommerce;
								$cart_url = $woocommerce->cart->get_cart_url();
							?>
							<li class="cart"><a href="<?php echo $cart_url;?>"><img src="<?php bloginfo('template_directory'); ?>/assets/build/img/sadcart.png"></a></li>
							<li class="bar"></li>
							<li class="dropdown-toggle" data-toggle="dropdown" ><a href="#"><img src="<?php bloginfo('template_directory'); ?>/assets/build/img/glob.png"></a></li>
							<ul class="dropdown-menu language">
								<li><a href="#">Italino</a></li>
								<li><a href="#">English</a></li>
								<li><a href="#">FRANÇAIS</a></li>
							</ul>
						</ul>
					</div><!-- /.cart-language -->

				</div><!-- /.col-md-9 -->

			  </div><!-- #masthead -->
		</header><!-- #masthead -->

	</section><!--banner section-->

	<div class="fixed-menu-close"><i class="fa fa-close"></i></div>

	<div id="content" class="site-content">
