<?php 
/* Template Name: About us */
    get_header();
?>

<div class="inner-page about-us">
    <div class="container">
        <?php while ( have_posts() ) : the_post(); ?>
            <div class="col-md-12 text-center">
                <div class="mb-title-n-sub-title">
                    <?php the_title("<h1>","</h1>");?>
                    <?php
                    /* todo need meta field for sub title here */
                     ?>
                    <h2><?php _e('Scopri come è nata l’avventura di MyBoo','myboo');?></h2>
                </div>
                <?php if ( has_post_thumbnail() ) { ?>
                        <?php 
                        /* todo insert feature image display size here */
                        the_post_thumbnail();?>
                <?php } else { ?>
                    <img src="<?php echo get_template_directory_uri()?>/assets/build/img/abt-baby.png" alt="baby" />
                <?php } ?>
                <div class="mb-content">               
                    <?php the_content();?>
                </div>                    
                <a href="#" class="c-btn c-btn-primary"><?php _e('CREA IL TUO LIBRO','myboo');?></a>                       
            </div><!-- /.col-md-12 -->
        <?php endwhile; // End of the loop. ?>               
    </div><!-- /.container -->
</div><!-- /.about-us -->

<!-- FOR SVG WAVE ANIMATION -->
<?php get_template_part('template-parts/content', 'svg-wave') ?>

<?php 
    get_footer();
?>