<?php
/*
Template Name: Gravity form
*/

get_header();
?>
<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="boo-inner-banner">
	            <div class="container">
		            <div class="row myboo-banner-content">
						<div class="col-sm-10 col-sm-offset-1">
							<div class="myboo-gvform">
							  <?php
							  while ( have_posts() ) : the_post();
							 	the_content();
							  endwhile; // End of the loop.
							  ?>

							  <!-- <img src="<?php bloginfo('template_directory'); ?> /assets/img/green-circle.svg" class="svg-img circle">
							  <img src="<?php bloginfo('template_directory'); ?> /assets/img/rect.svg" class="svg-img rect"> -->
							</div>
						</div>

							<div class="small-circles">
								<span class="green square"></span>
								<span class="purple circles"></span>
								<span class="purple circles light-pur"></span>
								<span class="green circles"></span>
								<span class="orange square"></span>
								<span class="orange circles"></span>
								<span class="pink bar"></span>
								<span class="triangle"></span>
								<span class="blue circles"></span>
								<span class="yelow circles"></span>
							</div>

					</div><!-- myboo-banner-content -->

				</div><!-- container -->
			</div>
			
			<!-- FOR SVG WAVE ANIMATION -->
			<?php get_template_part('template-parts/content', 'svg-wave') ?>
			
		</main><!-- #main -->
	</div><!-- #primary -->
