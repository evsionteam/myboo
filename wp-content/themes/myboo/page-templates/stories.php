<?php
    /* Template Name: Stories */
    get_header();
?>  

    <!-- Modal box -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content c-modal-content">
                
                <div class="modal-body">
                    <div class="row"> 
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div class="col-md-6">
                            <img src="<?php echo get_template_directory_uri()?>/assets/build/img/modal-pic.png" alt="modal-img" />
                            
                            <div class="title">
                                <h2>Un giorno all’asilo</h2>
                                <img src="<?php echo get_template_directory_uri();?>/assets/build/img/rate-icon.png" alt="rate-icon" />
                            </div>
                            <div class="detail">
                                Dedicato a tutti i bimbi che stanno muovendo
                                i primi passi in un posto tutto nuovo 
                                per aiutarli a vivere serenamente il cambiamento. 
                                E a tutti i bimbi che concludono un capitolo 
                                della loro storia e vogliono ricordare 
                                i momenti belli vissuti e le persone meravigliose incontrate.
                            </div>
                        </div>
                        <div class="col-md-6 customize">
                            
                            <h2>POTRAI PERSONALIZZARE</h2>

                            <ul>
                                <li>NOME BAMBINO/A</li>
                                <li>CARATTERISTICHE FISCHE DEL BAMBINO/A</li>
                                <li>NOME DELLA MAESTRA</li>
                                <li>CARETTERISTICHE FISICHE DELLA MAESTRA</li>
                                <li>NOME DELL’ASILO</li>
                                <li>DEDICA</li>
                                <li>FILASTROCCA</li>
                            </ul>

                            <p>
                                Se non hai qualche informazione non ti preoccupare, 
                                compileremo noi i campi e potrai vedere l’anteprima.
                            </p>

                        </div>
                    </div>
                </div>
            
            </div>
        </div>
    </div><!-- /.modal -->

    <div class="inner-page stories">
        <div class="container">
            <div class="col-md-12">

                <div class="text-center top-sec">
                    <div class="mb-title-n-sub-title">
                        <h1>Le nostre storie</h2>
                        <h2>Personalizza ogni dettaglio per renderle veramente magiche.</h2>
                    </div>

                    <img src="<?php echo get_template_directory_uri()?>/assets/build/img/stories-baby.png" alt="stories-img" />
                    
                    <div class="clearfix"></div>
                    
                    <div class="ratings">
                        <img src="<?php echo get_template_directory_uri();?>/assets/build/img/rate-icon.png" alt="rating-icon" />
                        <img src="<?php echo get_template_directory_uri();?>/assets/build/img/rate-icon.png" alt="rating-icon" />
                        <img src="<?php echo get_template_directory_uri();?>/assets/build/img/rate-icon.png" alt="rating-icon" />
                        <img src="<?php echo get_template_directory_uri();?>/assets/build/img/rate-icon.png" alt="rating-icon" />
                    </div>

                    <div class="detail">
                        Le bacchette magiche ti indicano quanto puoi
                        personalizzare ciascuna storia.
                        Più è personalizzata, più è garantito l’effetto magia.
                    </div>
                    
                    <div class="clearfix"></div>

                    <a href="#" class="c-btn c-btn-primary">CREA IL TUO LIBRO</a>
                </div><!-- /.text-center -->



                <div class="stories-lists">
                    <div class="row owl-carousel" id="stories">

                        <?php for( $i=0; $i<=8; $i++ ) {?>
                            <div class="col-md-6">
                                <section class="story">
                                
                                    <div class="title-n-info">
                                        <div class="info" data-toggle="modal" data-target=".bs-example-modal-lg">
                                            <i class="fa fa-info"></i>
                                        </div>
                                        <div class="title-n-rate">
                                            <h2>Un giorno all’asilo</h2>
                                            <div class="ratings pull-left">
                                                <img src="<?php echo get_template_directory_uri();?>/assets/build/img/rate-icon.png" alt="rating-icon" />
                                                <img src="<?php echo get_template_directory_uri();?>/assets/build/img/rate-icon.png" alt="rating-icon" />
                                                <img src="<?php echo get_template_directory_uri();?>/assets/build/img/rate-icon.png" alt="rating-icon" />
                                                <img src="<?php echo get_template_directory_uri();?>/assets/build/img/rate-icon.png" alt="rating-icon" />
                                            </div>
                                        </div>
                                    </div><!-- /.title-n-info -->

                                    <div class="img-n-detail">
                                        <div class="img">
                                            <img src="<?php echo get_template_directory_uri();?>/assets/build/img/story-pic.jpg" alt="story-img" />
                                        </div>
                                        <div class="detail">
                                            Dedicato a tutti i bimbi che stanno muovendo
                                            i primi passi in un posto tutto nuovo per aiutarli
                                            a vivere serenamente il cambiamento. 
                                            E a tutti i bimbi che concludono un capitolo 
                                            della loro storia e vogliono ricordare 
                                            i momenti belli vissuti 
                                            e le persone meravigliose incontrate.
                                        </div>
                                    </div>

                                    <div class="price-detail-links visible-xs">
                                        <div>
                                            <div><b>€26,90</b></div>
                                            <span>COPERTINA MORBIDA</span>
                                        </div>
                                        <div>
                                            <div><b>€36,90</b></div>
                                            <span>COPERTINA RIGIDA</span>
                                        </div>
                                    </div>

                                    <div class="price-detail-links">
                                        <a href="" class="btn-cre-book">CREA LIBRO</a>
                                        <a href="" class="btn-example">ESEMPIO</a>

                                        <div class="hidden-xs">
                                            <div><b>€26,90</b></div>
                                            <span>COPERTINA MORBIDA</span>
                                        </div>
                                        <div class="hidden-xs">
                                            <div><b>€36,90</b></div>
                                            <span>COPERTINA RIGIDA</span>
                                        </div>
                                    </div>


                                </section>
                            </div><!-- /.col-md-6 -->
                        <?php } ?>

                    </div><!-- /.row -->
                </div><!-- /.stories-lists -->

                <center>
                    <a href="#" class="c-btn c-btn-primary">CREA IL TUO LIBRO</a>
                </center>

            </div><!-- /.col-md-12 -->
        </div><!-- /.container -->
    </div><!-- /.stories -->
    

<?php
    get_footer();
