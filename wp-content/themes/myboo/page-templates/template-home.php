<?php
/** 
 * Template Name: Home page
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="myboo-banner" id="myboo-banner">
				<div class="container">
					<div class="myboo-banner-content clearfix">

						<div class="col-md-9 text-left">
							<img src="<?php bloginfo('template_directory'); ?>/assets/build/img/small-purple.svg" class="svg-img small-purple">
							<img src="<?php bloginfo('template_directory'); ?>/assets/build/img/small-blue-circle.svg" class="svg-img small-blue-circle">
							<h1>Crea un libro personalizzato per il tuo bambino!</h1>
							<img src="<?php bloginfo('template_directory'); ?>/assets/build/img/stick.svg" class="svg-img stick">
						</div>

						<img src="<?php bloginfo('template_directory'); ?>/assets/build/img/blue-triangle.svg" class="col-md-1 svg-img blue-triangle">

						<div class="col-md-12 banner-description">
							<div class="col-md-7">
								<p class="text-left">CON MYBOO PUOI CREARE UN LIBRO UNICO, PERSONALIZZANDO QUALSIASI ASPETTO.</p>
								<img src="<?php bloginfo('template_directory'); ?>/assets/build/img/green-circle.svg" class="svg-img circle">
								<img src="<?php bloginfo('template_directory'); ?>/assets/build/img/rect.svg" class="svg-img rect">
							</div>
							<div class="col-md-5 btn-wrapper">
								<a href="#" class="c-btn c-btn-primary">OK, INIZIAMO</a>
								<img src="<?php bloginfo('template_directory'); ?>/assets/build/img/circle.svg">
							</div>
						</div><!-- md-12 -->

					</div><!-- myboo-banner-content -->

					<div class="scroll-down">
						<a href="#" class="scroll-btn at-ripple" data-animation-duration="500" data-target="#myboo-feature"><i class="fa fa-angle-down"></i></a>
					</div>

				</div><!-- container -->

				<!-- FOR SVG WAVE ANIMATION -->
				<?php get_template_part('template-parts/content', 'svg-wave') ?>
				
			</section>

			<section class="myboo-feature" id="myboo-feature"><!-- feature section -->
				<div class="container">
					<div class="row">
					    <h1 class="mb-title">PERCHE’ SCEGLIERE MYBOO</h1>
						<div class="col-md-12">
							<div class="col-md-4 col-x-12 col-sm-6">
								<div class="feature-section">
								  <img src="<?php bloginfo('template_directory'); ?>/assets/build/img/blue-rectangle.svg" class="svg-img blue-rectangle"><!-- rectangle -->
									<img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/assets/build/img/feature1.png">
									<h2>Ogni libro è unico</h2>
									<p>Sometimes the simplest things are the hardest to find. So we created a new line for everyday life</p>
								</div>
							</div>
							<div class="col-md-4 col-x-12 col-sm-6">
								<div class="feature-section">
									<img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/assets/build/img/feature2.png">
									<h2>Ogni libro è unico</h2>
									<p>Sometimes the simplest things are the hardest to find. So we created a new line for everyday life</p>
								</div>
							</div>
							<div class="col-md-4 col-x-12 col-sm-6">
								<div class="feature-section">
								 <img src="<?php bloginfo('template_directory'); ?>/assets/build/img/small-red-circle.svg" class="svg-img small-red-circle">
									<img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/assets/build/img/feature3.png">
									<h2>Ogni libro è unico</h2>
									<p>Sometimes the simplest things are the hardest to find. So we created a new line for everyday life</p>
								</div>
							</div>
						</div>
					</div><!-- row -->
				</div><!-- container -->
			</section><!-- section -->

			<section class="slider-wrapper text-center">
				<h1 class="mb-title">I NOSTRI LIBRI</h1>

				<div class="slider-with-nav">
					<ul id="our-slider" class="clearfix">
						<li
							data-title="1IL MAGO DEL TEMPO"
							data-description="1Un viaggio tra maghi distratti, fatine buone e draghi dispettosi in cui i due amici dopo aver affrontato con coraggio e intelligenza mille difficoltà riceveranno un meritato premio."
							data-link="1www.google.com">
							<img width="100%" src="<?php bloginfo('template_directory'); ?>/assets/build/img/slider1.jpg" alt="">
						</li>
						<li
							data-title="2IL MAGO DEL TEMPO"
							data-description="2Un viaggio tra maghi distratti, fatine buone e draghi dispettosi in cui i due amici dopo aver affrontato con coraggio e intelligenza mille difficoltà riceveranno un meritato premio."
							data-link="2www.google.com">
							<img width="100%" src="<?php bloginfo('template_directory'); ?>/assets/build/img/slider1.jpg" alt="">
						</li>
						<li
							data-title="3IL MAGO DEL TEMPO"
							data-description="3Un viaggio tra maghi distratti, fatine buone e draghi dispettosi in cui i due amici dopo aver affrontato con coraggio e intelligenza mille difficoltà riceveranno un meritato premio."
							data-link="3www.google.com">
							<img width="100%" src="<?php bloginfo('template_directory'); ?>/assets/build/img/slider1.jpg" alt="">
						</li>
						<li
							data-title="4IL MAGO DEL TEMPO"
							data-description="4Un viaggio tra maghi distratti, fatine buone e draghi dispettosi in cui i due amici dopo aver affrontato con coraggio e intelligenza mille difficoltà riceveranno un meritato premio."
							data-link="4www.google.com">
							<img width="100%" src="<?php bloginfo('template_directory'); ?>/assets/build/img/slider1.jpg" alt="">
						</li>
						<li
							data-title="IL MAGO DEL TEMPO"
							data-description="Un viaggio tra maghi distratti, fatine buone e draghi dispettosi in cui i due amici dopo aver affrontato con coraggio e intelligenza mille difficoltà riceveranno un meritato premio."
							data-link="www.google.com">
							<img width="100%" src="<?php bloginfo('template_directory'); ?>/assets/build/img/slider1.jpg" alt="">
						</li>
						<li
							data-title="IL MAGO DEL TEMPO"
							data-description="Un viaggio tra maghi distratti, fatine buone e draghi dispettosi in cui i due amici dopo aver affrontato con coraggio e intelligenza mille difficoltà riceveranno un meritato premio."
							data-link="www.google.com">
							<img width="100%" src="<?php bloginfo('template_directory'); ?>/assets/build/img/slider1.jpg" alt="">
						</li>
						<li
							data-title="IL MAGO DEL TEMPO"
							data-description="Un viaggio tra maghi distratti, fatine buone e draghi dispettosi in cui i due amici dopo aver affrontato con coraggio e intelligenza mille difficoltà riceveranno un meritato premio."
							data-link="www.google.com">
							<img width="100%" src="<?php bloginfo('template_directory'); ?>/assets/build/img/slider1.jpg" alt="">
						</li>
						<li
							data-title="IL MAGO DEL TEMPO"
							data-description="Un viaggio tra maghi distratti, fatine buone e draghi dispettosi in cui i due amici dopo aver affrontato con coraggio e intelligenza mille difficoltà riceveranno un meritato premio."
							data-link="www.google.com">
							<img width="100%" src="<?php bloginfo('template_directory'); ?>/assets/build/img/slider1.jpg" alt="">
						</li>
						<li
							data-title="IL MAGO DEL TEMPO"
							data-description="Un viaggio tra maghi distratti, fatine buone e draghi dispettosi in cui i due amici dopo aver affrontato con coraggio e intelligenza mille difficoltà riceveranno un meritato premio."
							data-link="www.google.com">
							<img width="100%" src="<?php bloginfo('template_directory'); ?>/assets/build/img/slider1.jpg" alt="">
						</li>

					</ul>
					<button id="btn-prev"><i class="fa fa-angle-left"></i></button>
					<button id="btn-next"><i class="fa fa-angle-right"></i> </button>
				</div>

				<div class="more-info">
					<h1>loading...</h1>
					<p class="description" style="-webkit-animation-delay: 0.1s; animation-delay: 0.1s;">
						loading...
					</p>
					<a href="#" class="c-btn c-btn-border" style="-webkit-animation-delay: 0.2s; animation-delay: 0.2s;">loading...</a>
				</div>



			</section> <!-- slider-wrapper -->


			<section class="baby-reading clearfix"><!-- baby reading section -->

				<img class="image-right" src="<?php bloginfo('template_directory'); ?>/assets/build/img/baby-right.png">

				<div class="container">
					<div class="detail">
						<h1 class="mb-title">INIZIA A CREARE IL TUO LIBRO</h1>
						<p>
							Potrai personalizzarlo in ogni sua parte
							per renderlo veramente unico
						</p>
						<a href="#" class="c-btn c-btn-primary">CREA IL TUO LIBRO</a>
					</div><!-- text -->
				</div>

				<img class="image-left" src="<?php bloginfo('template_directory'); ?>/assets/build/img/baby-left.png">

			</section><!-- section -->

			<section class="review"><!-- reviewsection -->
				<div class="container">
					<div class="col-md-12">
						<div class="review-head">
						    <img src="<?php bloginfo('template_directory'); ?>/assets/build/img/circle.svg" class="svg-img col-md-2 review-circle">
							<h1 class="mb-title">COSA DICONO I NOSTRI CLIENTI</h1>
							<p>Recensioni</p>
						</div>
						<div class="col-md-6">
							<div class="review-content">
								<span>
								  <img src="<?php bloginfo('template_directory'); ?>/assets/build/img/comma.png">
								</span>
								<h5>E’ stato un regalo apprezzatissimo perchè ho potuto
                                personalizzare qualsiasi aspetto del libro.</h5>
                                <p>John Updike</p>
							</div>
						</div>
						<div class="col-md-6">
							<div class="review-content">
								<span>
								  <img src="<?php bloginfo('template_directory'); ?>/assets/build/img/comma.png">
								</span>
								<h5>Colori vivaci e tantissimi opzioni con cui personalizzare il libro.</h5>
                                <p>Marta B.</p>
							</div>
						</div>
						<div class="col-md-6">
							<div class="review-content">
								<span>
								  <img src="<?php bloginfo('template_directory'); ?>/assets/build/img/comma.png">
								</span>
								<h5>Marta, una bambina di 3 anni, non smette più di leggere il libro che le abbiamo regalato, è subito diventato il suo preferito!</h5>
                                <p>Tommaso G.</p>
							</div>
						</div>
						<div class="col-md-6">
							<div class="review-content">
								<span>
								  <img src="<?php bloginfo('template_directory'); ?>/assets/build/img/comma.png">
								</span>
								<h5>Una sorpresa gradita da tutti.</h5>
                                <p>Abraham Lincon</p>
							</div>
						</div>
					</div>
				</div><!-- container -->
			</section><!-- section -->

			<section class="review full-width-review"><!-- reviewfull width section -->
				<div class="container">
					<div class="review-head">
						<h1>DICONO DI NOI</h1>
						<p>Recensioni</p>
					</div>
					<div class="col-md-12">
						<div class="second-review-content review-content">
							<h5>E’ stato un regalo apprezzatissimo perchè ho potuto personalizzare qualsiasi aspetto del libro.</h5>
                        </div>
					</div>
					<div class="col-md-12">
						<div class="second-review-content review-content">
							<h5>Marta, una bambina di 3 anni, non smette più di leggere il libro che le abbiamo regalato, è subito diventato il suo preferito!</h5>
                        </div>
					</div>
					 <div class="btn-wrapper for-mobile">
					   <a href="#" class="myboo-link">CREA IL TUO LIBRO</a>
					</div>
				</div><!-- container -->
			</section><!-- section -->
        </main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
