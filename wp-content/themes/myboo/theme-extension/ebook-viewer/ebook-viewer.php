<?php
function ebook_viewer_enqueue_script() {   
  wp_enqueue_script( 'dFlip-script', get_template_directory_uri() . '/theme-extension/ebook-viewer/dflip/js/dflip.min.js', array('jquery'), '1.0' );
}
add_action('wp_enqueue_scripts', 'ebook_viewer_enqueue_script');


function ebook_viewer_enqueue_style() {
  wp_enqueue_style( 'dFlip-style', get_template_directory_uri() . '/theme-extension/ebook-viewer/dflip/css/dflip.css',  array(), 1  );    
  wp_enqueue_style( 'dFlip-icons', get_template_directory_uri() . '/theme-extension/ebook-viewer/dflip/css/themify-icons.css',  array(), 1  );
}
add_action('wp_enqueue_scripts', 'ebook_viewer_enqueue_style');
?>