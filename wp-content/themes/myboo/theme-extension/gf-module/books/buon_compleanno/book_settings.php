<?php

class buon_compleanno {

	function __construct() { 	
		$this->init();
	}
	
	function init () { 	
		function buon_compleanno_enqueue_script() {  
			if (is_page('buon-compleanno')) {
				wp_enqueue_script( 'buon_compleanno', get_template_directory_uri() . '/theme-extension/gf-module/books/buon_compleanno/buon_compleanno.js', array('jquery'), time() ); 
				wp_enqueue_script( 'ebookgen-customization-script', get_template_directory_uri() . '/theme-extension/gf-module/js/common-book-customization.js', array('jquery'), time() );
			}		
		}
		add_action('wp_enqueue_scripts', 'buon_compleanno_enqueue_script');	
		
		//add_action( 'gform_pre_submission', 'pre_submission_handler' );
		//function pre_submission_handler( $form ) {
    	//	if (empty(rgpost( 'input_72' )) ) $_POST['input_72'] = 'new value for field 14';
		//}
	}	
}

?>