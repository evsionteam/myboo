var bookFormId = 21;
var bookFormName = '#gform';

var lastStep = 6;
var previewPage = 7;  
var detailPage = 8;

var variable_field = 'input_119';

default_pupazzo = 'bambola';
default_nome_pupazzo = 'Dudù';
default_piatto_preferito = 'risotto';
default_nome_mamma = 'Sonia';
default_occhi_mamma = 'neri';
default_colore_capelli_mamma = 'biondi';
default_acconciatura_mamma = 'lisci';
default_lunghezza_mamma = 'corti';
default_pelle_mamma = 'bianca';
default_nome_amico = 'Andrea';
default_sesso_amico = 'amico';
default_occhi_amico = 'neri';
default_colore_capelli_amico = 'biondi';
default_acconciatura_amico = 'lisci';
default_lunghezza_amico = 'corti';
default_pelle_amico = 'bianca';
default_nome_papa = 'Marco';
default_occhi_papa = 'neri';
default_colore_capelli_papa = 'biondi';
default_acconciatura_papa = 'lisci';
default_lunghezza_papa = 'corti';
default_pelle_papa = 'bianca';
default_barba_papa = 'no';
default_anni = 1;


jQuery(document).bind('gform_page_loaded', function(event, form_id, current_page){ 
	  
	if (current_page == previewPage) {  
		
		// prepara gli argomenti della query per selezionare i livelli di libro corrispondenti alla personalizzazione effettuata
    
    	//bambino
		sesso_bambino = jQuery('input[name=input_113]').val(); 
		nome_bambino = jQuery('input[name=input_119]').val(); 
		cognome_bambino = jQuery('input[name=input_120]').val(); 
		occhi_bambino = jQuery('input[name=input_117]').val();
		occhi_bambino_full = sesso_bambino+'&'+occhi_bambino; 
		colore_capelli_bambino = jQuery('input[name=input_114]').val(); 
		acconciatura_bambino = jQuery('input[name=input_115]').val(); 
		lunghezza_bambino = (sesso_bambino == 'bambino') ? 'corti' : jQuery('input[name=input_116]').val(); 
		pelle_bambino = jQuery('input[name=input_118]').val(); 
		capelli_bambino = sesso_bambino+'&'+colore_capelli_bambino+'&'+acconciatura_bambino+'&'+lunghezza_bambino; 
		bambino = sesso_bambino+'&'+pelle_bambino; 
		
		//pupazzo
		pupazzo = (jQuery('input[name=input_72]').is(":checked")) ? jQuery('input[name=input_72]:checked').val() : default_pupazzo; 
		nome_pupazzo = (jQuery('input[name=input_183]').val().length != 0) ? jQuery('input[name=input_183]').val() : default_nome_pupazzo; 
		
		//mamma
		nome_mamma = (jQuery('input[name=input_24]').val().length != 0) ? jQuery('input[name=input_24]').val() : default_nome_mamma;
		occhi_mamma = (jQuery('input[name=input_32]').is(":checked")) ? jQuery('input[name=input_32]:checked').val() : default_occhi_mamma; 
		occhi_mamma_full = 'Mamma&'+occhi_mamma;
		colore_capelli_mamma = (jQuery('input[name=input_27]').is(":checked"))  ? jQuery('input[name=input_27]:checked').val() : default_colore_capelli_mamma; 
		acconciatura_mamma = (jQuery('input[name=input_29]').is(":checked"))  ? jQuery('input[name=input_29]:checked').val() : default_acconciatura_mamma; 
		lunghezza_mamma = (jQuery('input[name=input_42]').is(":checked"))  ? jQuery('input[name=input_42]:checked').val() : default_lunghezza_mamma; 
		pelle_mamma = (jQuery('input[name=input_45]').is(":checked"))  ? jQuery('input[name=input_45]:checked').val() : default_pelle_mamma; 
		pelle_mamma_full = 'Mamma&'+pelle_mamma; 
		capelli_mamma = 'Mamma&'+colore_capelli_mamma+'&'+acconciatura_mamma+'&'+lunghezza_mamma; 
				
		//amico
		nome_amico = (jQuery('input[name=input_184]').val().length != 0) ? jQuery('input[name=input_184]').val() : default_nome_amico; 
		sesso_amico = (jQuery('input[name=input_185]').is(":checked")) ? jQuery('input[name=input_185]:checked').val() : default_sesso_amico;
		occhi_amico = ((jQuery('input[name=input_176]').is(":checked"))  ? jQuery('input[name=input_176]:checked').val() : default_occhi_amico); 
		occhi_amico_full = sesso_amico+'&'+occhi_amico;
		colore_capelli_amico = (jQuery('input[name=input_173]').is(":checked")) ? jQuery('input[name=input_173]:checked').val() : default_colore_capelli_amico; 
		acconciatura_amico = (jQuery('input[name=input_172]').is(":checked")) ? jQuery('input[name=input_172]:checked').val() : default_acconciatura_amico; 
		lunghezza_amico = (sesso_bambino == 'bambino') ? 'corti' : ((jQuery('input[name=input_174]').is(":checked")) ? jQuery('input[name=input_174]:checked').val() : default_lunghezza_amico); 
		pelle_amico = (jQuery('input[name=input_177]').is(":checked")) ? jQuery('input[name=input_177]:checked').val() : default_pelle_amico; 
		pelle_amico_full = sesso_amico+'&'+pelle_amico;
		capelli_amico = sesso_amico+'&'+colore_capelli_amico+'&'+acconciatura_amico+'&'+lunghezza_amico; 
		
		//papà
		nome_papa = (jQuery('input[name=input_157]').val().length != 0) ? jQuery('input[name=input_157]').val() : default_nome_papa;
		occhi_papa = (jQuery('input[name=input_163]').is(":checked")) ? jQuery('input[name=input_163]:checked').val() : default_occhi_papa; 
		occhi_papa_full = 'Papa&'+occhi_papa;
		colore_capelli_papa = (jQuery('input[name=input_160]').is(":checked"))  ? jQuery('input[name=input_160]:checked').val() : default_colore_capelli_papa; 
		acconciatura_papa = (jQuery('input[name=input_161]').is(":checked"))  ? jQuery('input[name=input_161]:checked').val() : default_acconciatura_papa; 
		lunghezza_papa = 'corti'; 
		pelle_papa = (jQuery('input[name=input_164]').is(":checked"))  ? jQuery('input[name=input_164]:checked').val() : default_pelle_papa;
		pelle_papa_full = 'Papa&'+pelle_papa;
		capelli_papa = 'Papa&'+colore_capelli_papa+'&'+acconciatura_papa+'&'+lunghezza_papa; 
		barba_papa = (jQuery('input[name=input_166]').is(":checked"))  ? jQuery('input[name=input_166]:checked').val() : default_barba_papa;
		barba_papa_full = 'Papa&'+barba_papa;
		
		//anni
		anni = (jQuery('select[name=input_186]').val().length != 0) ? jQuery('select[name=input_186]').val() : default_anni;
		data_nascita = jQuery('input[name=input_153]').val();
		giorno = data_nascita.substr(0, data_nascita.indexOf("/"));
		mese = data_nascita.substr(data_nascita.indexOf("/")+1,data_nascita.substring(data_nascita.indexOf("/")+1,100).indexOf("/"));
		switch(mese) {
			case '1':  
				stagione = 'inverno';
			break;
			case '2':  
				stagione = 'inverno';
			break;
			case '3': 
				if (giorno < 21)
					stagione = 'inverno';
				else stagione = 'primavera';
			break;
			case '4':  
				stagione = 'primavera';
			break;
			case '5':  
				stagione = 'primavera';
			break;
			case '6':  
				if (giorno < 22)
					stagione = 'primavera';
				else stagione = 'estate';
			break;
			case '7':  
				stagione = 'estate';
			break;
			case '8':  
				stagione = 'estate';
			break;
			case '9':  
				if (giorno < 23)
					stagione = 'estate';
				else stagione = 'autunno';
			break;
			case '10':  
				stagione = 'autunno';
			break;
			case '11':  
				stagione = 'autunno';
			break;
			case '12':  
				if (giorno < 23)
					stagione = 'autunno';
				else stagione = 'inverno';
			break;
			default:
		}		
		
		// dedica
		dedica = jQuery('input[name=input_85]').val();
		
		// foto
		foto = jQuery('input[name=input_89]').val();
		
		// parametri query
		
		title = jQuery('input[name=input_126]').val(); 
    	query_book_title = 'Buon Compleanno'; 
		query_select_var = ['sesso-bambino', 'personaggio&pelle','personaggio&occhi','personaggio&colore-capelli&acconciatura&lunghezza','pupazzo','personaggio&occhi','personaggio&colore-capelli&acconciatura&lunghezza','personaggio&pelle','personaggio&occhi','personaggio&colore-capelli&acconciatura&lunghezza','personaggio&pelle','personaggio&occhi','personaggio&colore-capelli&acconciatura&lunghezza','personaggio&pelle','personaggio&barba','anni','stagione'];
		query_select_val = [sesso_bambino, bambino,occhi_bambino_full,capelli_bambino,pupazzo,occhi_mamma_full,capelli_mamma,pelle_mamma_full,occhi_amico_full,capelli_amico,pelle_amico_full,occhi_papa_full,capelli_papa,pelle_papa_full,barba_papa_full,anni,stagione];
		text_select_var = ['$$$nome-bambino$$$','$$$nome-mamma$$$','$$$pupazzo$$$','$$$nome-pupazzo$$$','$$$nome-papa$$$','$$$nome-amico$$$','$$$anni$$$'];
		text_select_val = [nome_bambino,nome_mamma,pupazzo,nome_pupazzo,nome_papa,nome_amico,anni];
		customization = sesso_bambino+' = '+nome_bambino+' '+cognome_bambino+', '+'occhi = '+occhi_bambino+', '+'capelli = '+colore_capelli_bambino+' '+acconciatura_bambino+' '+lunghezza_bambino+', '+'pelle = '+pelle_bambino+', '+'pupazzo = '+pupazzo+', '+'nome mamma = '+nome_mamma+', '+'capelli mamma = '+colore_capelli_mamma+' '+acconciatura_mamma+' '+lunghezza_mamma+', '+'occhi mamma = '+occhi_mamma+', '+'pelle mamma = '+pelle_mamma+', '+'occhi amico = '+occhi_amico+', '+'capelli amico = '+colore_capelli_amico+' '+acconciatura_amico+' '+lunghezza_amico+', '+'pelle amico = '+pelle_amico+', '+'occhi papà = '+occhi_papa+', '+'capelli papà = '+colore_capelli_papa+' '+acconciatura_papa+' '+lunghezza_papa+', '+'pelle papà = '+pelle_papa+', '+'barba papà = '+barba_papa+', '+'anni = '+anni+', '+'stagione = '+stagione;
	}
});