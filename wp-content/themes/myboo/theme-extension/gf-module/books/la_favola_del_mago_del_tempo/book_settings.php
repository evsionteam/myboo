<?php

class la_favola_del_mago_del_tempo {

	function __construct() { 	
		$this->init();
	}
	
	function init () { 	
		function mago_del_tempo_enqueue_script() {  
			if (is_page('la-favola-del-mago-del-tempo')) {
				wp_enqueue_script( 'la-favola-del-mago-del-tempo', get_template_directory_uri() . '/theme-extension/gf-module/books/la_favola_del_mago_del_tempo/la-favola-del-mago-del-tempo.js', array('jquery'), time() ); 
				wp_enqueue_script( 'ebookgen-customization-script', get_template_directory_uri() . '/theme-extension/gf-module/js/common-book-customization.js', array('jquery'), time() );
			}		
		}
		add_action('wp_enqueue_scripts', 'mago_del_tempo_enqueue_script');	
		
		add_action( 'gform_pre_submission', 'pre_submission_handler' );
		function pre_submission_handler( $form ) {
    		if (empty(rgpost( 'input_72' )) ) $_POST['input_72'] = 'new value for field 14';
		}
	}	
}

?>