<?php

class la_nascita {

	function __construct() { 	
		$this->init();
	}
	
	function init () { 	
		
		function la_nascita_enqueue_script() {  
			if (is_page('la-nascita')) {
				wp_enqueue_script( 'la-nascita', get_template_directory_uri() . '/theme-extension/gf-module/books/la_nascita/la-nascita.js', array('jquery'), time() ); 
				wp_enqueue_script( 'ebookgen-customization-script', get_template_directory_uri() . '/theme-extension/gf-module/js/common-book-customization.js', array('jquery'), time() );
			}		
		}
		add_action('wp_enqueue_scripts', 'la_nascita_enqueue_script');	
		
		//add_action( 'gform_pre_submission', 'pre_submission_handler' );
		//function pre_submission_handler( $form ) {
    	//	if (empty(rgpost( 'input_72' )) ) $_POST['input_72'] = 'new value for field 14';
		//}
	}	
}

?>