var bookFormId = 20;
var bookFormName = '#gform';

var lastStep = 6;
var previewPage = 7;  
var detailPage = 8;

var variable_field = 'input_119';

default_peluche = 'bambola';
default_nome_peluche = 'Dudù';
default_piatto_preferito = 'risotto';
default_nome_donna = 'Sonia';
default_occhi_donna = 'neri';
default_colore_capelli_donna = 'biondi';
default_acconciatura_donna = 'lisci';
default_lunghezza_donna = 'corti';
default_pelle_donna = 'bianca';


jQuery(document).bind('gform_page_loaded', function(event, form_id, current_page){ 
   
	if (current_page == previewPage) {  
		
		// prepara gli argomenti della query per selezionare i livelli di libro corrispondenti alla personalizzazione effettuata
    
    	sesso_bambino = jQuery('input[name=input_113]').val(); 
		nome_bambino = jQuery('input[name=input_119]').val(); 
		cognome_bambino = jQuery('input[name=input_120]').val(); 
		occhi_bambino = jQuery('input[name=input_117]').val(); 
		colore_capelli_bambino = jQuery('input[name=input_114]').val(); 
		acconciatura_bambino = jQuery('input[name=input_115]').val(); 
		lunghezza_bambino = jQuery('input[name=input_116]').val(); 
		pelle_bambino = jQuery('input[name=input_118]').val(); 
		peluche = (jQuery('input[name=input_72]').is(":checked")) ? jQuery('input[name=input_72]:checked').val() : default_peluche; 
		nome_donna = (jQuery('input[name=input_24]').val().length != 0) ? jQuery('input[name=input_24]').val() : default_nome_donna;
		occhi_donna = (jQuery('input[name=input_32]').is(":checked")) ? jQuery('input[name=input_32]:checked').val() : default_occhi_donna; 
		colore_capelli_donna = (jQuery('input[name=input_27]').is(":checked"))  ? jQuery('input[name=input_27]:checked').val() : default_colore_capelli_donna; 
		acconciatura_donna = (jQuery('input[name=input_29]').is(":checked"))  ? jQuery('input[name=input_29]:checked').val() : default_acconciatura_donna; 
		lunghezza_donna = (jQuery('input[name=input_42]').is(":checked"))  ? jQuery('input[name=input_42]:checked').val() : default_lunghezza_donna; 
		pelle_donna = (jQuery('input[name=input_45]').is(":checked"))  ? jQuery('input[name=input_45]:checked').val() : default_pelle_donna; 
		capelli_bambino = colore_capelli_bambino+'&'+acconciatura_bambino+'&'+lunghezza_bambino; 
		capelli_donna = colore_capelli_donna+'&'+acconciatura_donna+'&'+lunghezza_donna; 
		bambino = sesso_bambino+'&'+pelle_bambino; 

		
		// parametri query
		
		title = jQuery('input[name=input_126]').val(); 
    	query_book_title = 'La Nascita'; 
		query_select_var = ['sesso-bambino&pelle-bambino','occhi-bambino','colore-capelli-bambino&acconciatura-bambino&lunghezza-bambino','peluche','occhi-donna','colore-capelli-donna&acconciatura-donna&lunghezza-donna','pelle-donna'];
		query_select_val = [bambino,occhi_bambino,capelli_bambino,peluche,occhi_donna,capelli_donna,pelle_donna];
		text_select_var = ['$$$nome-bambino$$$','$$$nome-donna$$$','$$$peluche$$$'];
		text_select_val = [nome_bambino,nome_donna,peluche];
		customization = sesso_bambino+' = '+nome_bambino+' '+cognome_bambino+', '+'occhi = '+occhi_bambino+', '+'capelli = '+colore_capelli_bambino+' '+acconciatura_bambino+' '+lunghezza_bambino+', '+'pelle = '+pelle_bambino+', '+'peluche = '+peluche+', '+'nome donna = '+nome_donna+', '+'capelli donna = '+colore_capelli_donna+' '+acconciatura_donna+' '+lunghezza_donna+', '+'occhi donna = '+occhi_donna+', '+'pelle donna = '+pelle_donna;
	}
});