<?php 
/* Enqueue js e css per la componente Gravity Forms*/ 

$book_forms = array(18,20,21);

class gf_extend {
	
	function __construct($books) {
		$this->init($books);
	}	
	
	function init ($books) { 	
		function gf_settings() {   
			if (is_page('personalizzazione-iniziale-libro')) {
				wp_enqueue_script( 'gf-settings-initial-script', get_template_directory_uri() . '/theme-extension/gf-module/js/initial-book-customization.js', array('jquery'), time() );  
			}
			if (is_page('personalizzazione-libro')) {
				wp_enqueue_script( 'gf-settings-initial-script', get_template_directory_uri() . '/theme-extension/gf-module/js/initial-book-customization.js', array('jquery'), time() );  
			}
		}
		add_action('wp_enqueue_scripts', 'gf_settings');
		
		function gf_style() {
  			wp_enqueue_style( 'gf-style', get_template_directory_uri() . '/theme-extension/gf-module/css/gf-style.css',  array(), 100  );    
		}
		add_action('wp_enqueue_scripts', 'gf_style');
		
		foreach($books as $book) { 
		    include get_template_directory() . '/theme-extension/gf-module/books/' .$book . '/book_settings.php';
     		$init_book = new $book;
			
		}
		
		add_filter( 'gform_upload_path', 'change_upload_path', 10, 2 );
		function change_upload_path( $path_info, $form_id ) {
			$uploaded_photo_rel_path = '/myboo/uploaded-photos/'; 
			$time = time();
			$uploads = wp_upload_dir();  
			if ( !defined('ABSPATH') )
				define('ABSPATH', dirname(__FILE__) . '/');
			$abspath= ABSPATH; 
		   	$path_info['path'] = $uploads['basedir'] .$uploaded_photo_rel_path;
		   	$path_info['url'] = $uploads['baseurl'] .$uploaded_photo_rel_path;
		   	return $path_info;
		}
		
		add_action( 'gform_post_process_21', 'uploads_log_pre_submission' );
function uploads_log_pre_submission( $form ) {
    foreach ( $form["fields"] as &$field ) {
        if ( $field["type"] == "fileupload" ) {

            $id = "input_".$field["id"];

            if ( $field["multipleFiles"] ) {
                if ( empty( $_POST["gform_uploaded_files"] )  )
                    continue;

                $uploaded_files = json_decode( stripslashes( $_POST["gform_uploaded_files"] ), true );
                GFCommon::log_debug( "Field: {$field["id"]} (multipleFiles) \$_POST - " . print_r( $uploaded_files, true ) );

                foreach ($uploaded_files[$id] as $uploaded_file){
                    $file_name = $uploaded_file["uploaded_filename"];
                    $file = GFFormsModel::get_file_upload_path( $form["id"], $file_name );
                    GFCommon::log_debug( "Field: {$field["id"]} (multipleFiles) - {$file["url"]}" );
                }
            } else {
                if ( empty( $_FILES[$id]["name"] )  )
                    continue;

                GFCommon::log_debug( "Field: {$field["id"]} (singleFile) \$_FILES - " . print_r( $_FILES[$id], true ) );
                $file_name = $_FILES[$id]["name"];
                $file = GFFormsModel::get_file_upload_path( $form["id"], $file_name );
                GFCommon::log_debug( "Field: {$field["id"]} (singleFile) - {$file["url"]}" );
				
				$form_upload_path = GFFormsModel::get_upload_path($form["id"]);
	$file_info = GFFormsModel::get_temp_filename( $form["id"], $id  );
	$patth = $form_upload_path .'/tmp/' .$file_info['temp_filename'];
				
				session_start();     
  	$_SESSION['uploaded_file'] = $patth;
				
				error_log($patth .' e ' .print_r( $file_info, true ), 1, "lucia.arsena@gmail.com");
            }
        }
		
		
		
    }
}
	}
}


?>