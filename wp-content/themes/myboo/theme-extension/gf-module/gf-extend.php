<?php 
/* Enqueue js e css per la componente Gravity Forms*/ 

$book_forms = array(18,20,21);

class gf_extend {
	
	function __construct($books) {
		$this->init($books);
	}	
	
	function init ($books) { 	
		function gf_settings() {   
			if (is_page('personalizzazione-iniziale-libro')) {
				wp_enqueue_script( 'gf-settings-initial-script', get_template_directory_uri() . '/theme-extension/gf-module/js/initial-book-customization.js', array('jquery'), time() );  
			}
			if (is_page('personalizzazione-libro')) {
				wp_enqueue_script( 'gf-settings-initial-script', get_template_directory_uri() . '/theme-extension/gf-module/js/initial-book-customization.js', array('jquery'), time() );  
			}
		}
		add_action('wp_enqueue_scripts', 'gf_settings');
		
		function gf_style() {
  			wp_enqueue_style( 'gf-style', get_template_directory_uri() . '/theme-extension/gf-module/css/gf-style.css',  array(), 1  );    
		}
		add_action('wp_enqueue_scripts', 'gf_style', 100);
		
		foreach($books as $book) { 
		    include get_template_directory() . '/theme-extension/gf-module/books/' .$book . '/book_settings.php';
     		$init_book = new $book;
			
		}
		
		
		function change_upload_path( $path_info, $form_id ) {
			$uploaded_photo_rel_path = '/myboo/uploaded-photos/'; 
			$time = time();
			$uploads = wp_upload_dir();  
			if ( !defined('ABSPATH') )
				define('ABSPATH', dirname(__FILE__) . '/');
			$abspath= ABSPATH; 
		   	$path_info['path'] = $uploads['basedir'] .$uploaded_photo_rel_path;
		   	$path_info['url'] = $uploads['baseurl'] .$uploaded_photo_rel_path;
		   	return $path_info;
		}
		add_filter( 'gform_upload_path', 'change_upload_path', 10, 2 );
		
		
		//add_action( 'gform_post_process_21', 'uploads_log_pre_submission' );
		function uploads_log_pre_submission( $form ) {
    		foreach ( $form["fields"] as &$field ) {
        		if ( $field["type"] == "slim" ) {
					$id = "input_".$field["id"];
					if ( $field["multipleFiles"] ) {
                		if ( empty( $_POST["gform_uploaded_files"] )  )
                    		continue;
						$uploaded_files = json_decode( stripslashes( $_POST["gform_uploaded_files"] ), true );
                		GFCommon::log_debug( "Field: {$field["id"]} (multipleFiles) \$_POST - " . print_r( $uploaded_files, true ) );

                		foreach ($uploaded_files[$id] as $uploaded_file){
                    		$file_name = $uploaded_file["uploaded_filename"];
                    		$file = GFFormsModel::get_file_upload_path( $form["id"], $file_name );
                    		GFCommon::log_debug( "Field: {$field["id"]} (multipleFiles) - {$file["url"]}" );
                		}
            		} 
					else {
					
                		if (isset($_FILES[$inputName]))
                    		continue;
						error_log(' else non sono uscito ' .print_r($res, true), 1, "lucia.arsena@gmail.com");
						GFCommon::log_debug( "Field: {$field["id"]} (singleFile) \$_FILES - " . print_r( $_FILES[$id], true ) );
                		$file_name = $_FILES[$id]["name"];
                		$file = GFFormsModel::get_file_upload_path( $form["id"], $file_name );
                		GFCommon::log_debug( "Field: {$field["id"]} (singleFile) - {$file["url"]}" );
				
						$form_upload_path = GFFormsModel::get_upload_path($form["id"]);
						$file_info = GFFormsModel::get_temp_filename( $form["id"], $id  );
						$uploaded_file = $form_upload_path .'/tmp/' .$file_info['temp_filename'];
				
						session_start();     
  						$_SESSION['uploaded_file'] = $uploaded_file;

						error_log('foto post-process ' .$uploaded_file, 1, "lucia.arsena@gmail.com");
            		}
        		}		
    		}
		}
	}
}
/*
		class GFSlimFieldService_custom {

			public static function configure_woocommerce_gforms_strip_meta_html( $strip_html, $display_value, $field, $lead, $form_meta ) {
				$field_type = $field->get_input_type();
				if ( $field_type == 'slim' || $field_type == 'slim_post' ) {
					$strip_html = false;
				}
				return $strip_html;
			}

			public static function setup() {
				$protocol = isset( $_SERVER['HTTPS'] ) ? 'https://' : 'http://';
				wp_localize_script( 'gf_slim_js', 'gf_slim_ajax', array(
					'ajax_url'   => admin_url( 'admin-ajax.php', $protocol ),
				) );
			}

			public static function save() {

				// get id of input field that posted the image
				$field_id = $_POST['field'];
				$form_id = $_POST['form'];
				$input_name = 'input_' . $field_id;

				// init
				$images = null;

				// Get posted data, if something is wrong, exit
				try {
					$images = Slim::getImages($input_name);
				}
				catch (Exception $e) {}

				// No image found under the supplied input name
				if (!is_array($images)) {

					Slim::outputJSON(array(
						'status' => SlimStatus::FAILURE,
						'message' => 'Unknown'
					));

					return;
				}

				// Should always be one image (when posting async), so we'll use the first on in the array (if available)
				$image = array_shift($images);

				// get the name of the file
				$image_data = $image['output']['data'];
				$image_name = $image['output']['name'];

				// write a temp file
				$result = GFSlimFieldUtilities::writeTempFile( $form_id, $image_name, $image_data );
				
				error_log('vediamo ' .print_r($result, true) .' e poi ' .$result['name'], 1, "lucia.arsena@gmail.com");

				// we return filename
				echo $result['name'];

				die();
			}

		}

			// Setup Ajax action hook
			remove_action( 'wp_ajax_nopriv_gf_slim_save', array( 'GFSlimFieldService', 'save' ) );
			remove_action( 'wp_ajax_gf_slim_save', array( 'GFSlimFieldService', 'save' ) );
			remove_filter( 'woocommerce_gforms_strip_meta_html', array( 'GFSlimFieldService', 'configure_woocommerce_gforms_strip_meta_html' ), 5, 10 );
			
			// Setup Ajax action hook
			add_action( 'wp_ajax_nopriv_gf_slim_save', array( 'GFSlimFieldService_custom', 'save' ) );
			add_action( 'wp_ajax_gf_slim_save', array( 'GFSlimFieldService_cistom', 'save' ) );
			add_filter( 'woocommerce_gforms_strip_meta_html', array( 'GFSlimFieldService_custom', 'configure_woocommerce_gforms_strip_meta_html' ), 5, 10 );

*/
?>