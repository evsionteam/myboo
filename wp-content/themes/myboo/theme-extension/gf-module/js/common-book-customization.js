var dFlipLocation = "http://myboo.nexnova.biz/wp-content/themes/myboo/theme-extension/ebook-viewer/dflip/";

var back_url = "http://myboo.nexnova.biz/index.php/personalizzazione-libro";

/* Logica del form  */

jQuery(document).bind('gform_page_loaded', function(event, form_id, current_page){ 
  
  	// imposta il footer in funzione dello specifico step
	
	current_page_name = bookFormName+'_page_'+bookFormId+'_'+current_page;
  	current_page_footer = jQuery(current_page_name).find(".gform_page_footer"); 
  	current_footer_content = current_page_footer.html();
  	if(current_page == detailPage) { 
    	pos = current_footer_content.indexOf('<input type="submit" id="gform_submit_button'); 
  	}
  	else { 
    	pos = current_footer_content.indexOf('<input type="button" id="gform_next_button');
  	}
  	current_prev_btn = current_footer_content.substr(0, pos);
  	current_next_btn = current_footer_content.substr(pos);
  
  	switch(parseInt(current_page)) {
    	case lastStep:         
      		jQuery(current_page_footer).html('<div class="gform_page_footer">'+
        		'<div class="gform-navigator">'+
          			'<div class="step-button">'+
            			'<div class="back-button">'+current_prev_btn+'</div>'+
            			'<div class="customization-breadcrumb"><span id="actual-step">'+current_page+'</span><span>/</span><span id="final-step">'+lastStep+'</span></div>'+
            			'<div class="next-button">'+'</div>'+
          			'</div>'+
          			'<div class="customization-steps">'+
						
            			'<span class="book"> Libro </span>'+
            			'<span class="settings active "> Personalizza </span>'+
            			'<span class="eye"> Anteprima </span>'+
            			'<span class="cart"> Carrello </span>'+
					  '</div>'+
					'<div class="next-prev-btn">' +
					'<a class="back-btn" href="' + back_url + '">' +
					'Indietro' +
					'</a>' +
          			'<div class="next-btn c-btn-secondary jump-to-next-step c-btn">'+
            			current_next_btn+
          			'</div>'+
          			'</div>'+
        		'</div>'+
      		'</div>'); 
    	break;
    	case previewPage:      
      		jQuery(current_page_footer).html('<div class="gform_page_footer">'+
				'<div class="gform-navigator">'+
					'<div class="back-btn">'+
            			current_prev_btn+  			
					'</div>'+         			
					'<div class="customization-steps">'+
						'<span class="book"> Libro </span>'+
						'<span class="settings"> Personalizza </span>'+
						'<span class="eye active"> Anteprima </span>'+
						'<span class="cart"> Carrello </span>'+
					'</div>'+
					'<div class="next-btn jump-to-next-step c-btn-secondary c-btn">'+
						current_next_btn+
					'</div>'+
				'</div>'+
			'</div>'); 
      	break;
		case detailPage:      
      		jQuery(current_page_footer).html('<div class="gform_page_footer">'+
				'<div class="gform-navigator">'+
					'<div class="back-btn">'+
            			current_prev_btn+  			
					'</div>'+         			
					'<div class="customization-steps">'+
						'<span class="book"> Libro </span>'+
						'<span class="settings"> Personalizza </span>'+
						'<span class="eye active"> Anteprima </span>'+
						'<span class="cart"> Carrello </span>'+
					'</div>'+
					'<div class="next-btn jump-to-next-step ask c-btn-secondary c-btn">'+
						current_next_btn+
					'</div>'+
				'</div>'+
			'</div>'); 
      	break;
    	default:  
      		jQuery(current_page_footer).html('<div class="gform_page_footer">'+
        		'<div class="gform-navigator">'+
          			'<div class="step-button">'+
            			'<div class="back-button">'+current_prev_btn+'</div>'+
            			'<div class="customization-breadcrumb"><span id="actual-step">'+current_page+'</span><span>/</span><span id="final-step">'+lastStep+'</span></div>'+
            			'<div class="next-button">'+current_next_btn+'</div>'+
          			'</div>'+
          			'<a class="back-btn" href="'+back_url +'">'+
            			'Indietro'+
          			'</a>'+
					'<div class="customization-steps">'+
						'<span class="book"> Libro </span>'+
						'<span class="settings active "> Personalizza </span>'+
						'<span class="eye"> Anteprima </span>'+
						'<span class="cart"> Carrello </span>'+
					'</div>'+
          			'<div class="next-btn disable c-btn">'+
            			'Avanti'+
          			'</div>'+
        		'</div>'+
      		'</div>');
  	} 

  	if (current_page == previewPage) {    
    
    	jQuery('#img').show(); //visualizza icona 'loading'
		jQuery('.gform_next_button').attr('disabled', 'disabled');
    
    	jQuery.ajax({
      		type:"POST",
      		url: "/wp-admin/admin-ajax.php",
				data : {
				action : 'create_low_res_book',
				title: title,
				query_book_title: query_book_title,
				query_select_var: query_select_var,
				query_select_val: query_select_val,
				text_select_var: text_select_var,
				text_select_val: text_select_val,
				customization: customization
	    	},
      		success: function( response ) {    
        
        		/* imposta parametri plugin flipbook */
        		var options = {
          			height: 500, enableDownload: false,
          			allControls: "altPrev,pageNumber,altNext,outline,thumbnail,zoomIn,zoomOut,fullScreen,share,more,download,pageMode,startPage,endPage,sound",
	        		mainControls: "altPrev,pageNumber,altNext,outline,thumbnail,zoomIn,zoomOut,fullScreen,share,more",
	        		hideControls: "outline, thumbnail, share, more",
          			transparent: true,
        		};
        
				jQuery('#img').hide(); //nasconde icona 'loading'
				flipBook = jQuery("#flipbookContainer").flipBook(response, options); //apre flipbook
				jQuery('.gform_next_button').removeAttr('disabled');
      		}
    	});
  	} // if (current_page == previewPage)
    
    
	jQuery(document).ready(function() { 
    
    	/* imposta il nome del bambino nel testo delle pagine */
    
		jQuery('.child-name').each(function() {
      		jQuery(this).html( jQuery('input[name='+variable_field+']').val());
    	});
		
		jQuery('.c-file-upload').each(function() {
			if( jQuery(this).length > 0 && !jQuery(this).hasClass('added')){
                 jQuery(this).addClass('added').find('input[type="file"]').hide().parent().prev().append('<span class="file-name"></span><span class="c-btn-file">Choose File</span>');
             }
    	});
        
    	// statement temporaneo per risolvere il limite di scrolling
		jQuery('html').attr('style', 'cursor: -webkit-grab; touch-action: none; overflow: initial !important;');
        
  	}) // jQuery(document).ready(function() 

});

jQuery(document).ready(function() {  

	jQuery('.child-name').each(function() {
      		jQuery(this).html( jQuery('input[name='+variable_field+']').val());
    	});
	
	// imposta footer nella prima pagina del form
	
	first_page_name = bookFormName+'_page_'+bookFormId+'_'+1; 
  	first_page_footer = jQuery(first_page_name).find(".gform_page_footer"); 
  	first_page_footer_content = first_page_footer.html();
  	first_page_footer.html('<div class="">'+
    	'<div class="gform-navigator">'+
      		'<div class="step-button">'+
        		'<div class="back-button">'+'Indietro'+'</div>'+
        		'<div class="customization-breadcrumb"><span id="actual-step">1</span><span>/</span><span id="final-step">'+lastStep+'</span></div>'+
        		'<div class="next-button">'+first_page_footer_content+'</div>'+
      		'</div>'+
      		'<div class="customization-steps">'+
        		'<span class="book"> Libro </span>'+
        		'<span class="settings active "> Personalizza </span>'+
        		'<span class="eye"> Anteprima </span>'+
        		'<span class="cart"> Carrello </span>'+
      		'</div>'+
          '<div class="next-prev-btn">'+
            '<a class="back-btn" href="'+back_url +'">'+
              'Indietro'+
            '</a>'+
        		'<div class="next-btn disable c-btn">'+
          		'Avanti'+
        		'</div>'+          
          '</div>'+
    	'</div>'+
  	'</div>');
}); 