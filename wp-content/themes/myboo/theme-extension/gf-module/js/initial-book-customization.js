/*
**
**  Funzionalità e controlli form comune
**
*/


bookFormId = 19;
bookFormName = '#gform';
bookCustomizeForm = bookFormName + '_' + bookFormId;

// Gravity Forms mapping - Pages
sex_settings = 1;
name_settings = 2;
hair_settings = 3;
eyesAndSkin_settings = 4;
bookSelection_settings = 5;


jQuery(document).bind('gform_page_loaded', function (event, form_id, current_page) {

	jQuery(document).ready(function () {

		if (current_page == sex_settings) {

			jQuery('.gform_next_button').hide();

			jQuery('#female-child').on("click", function (e) {
				jQuery('input[name=input_1]').filter('[value=bambina]').prop('checked', true);
				jQuery('input[name=input_1]').filter('[value=bambino]').prop('checked', false);
				jQuery(bookCustomizeForm).trigger("submit", [true]);
			});

			jQuery('#male-child').on("click", function (e) {
				jQuery('input[name=input_1]').filter('[value=bambina]').prop('checked', false);
				jQuery('input[name=input_1]').filter('[value=bambino]').prop('checked', true);
				jQuery(bookCustomizeForm).trigger("submit", [true]);
			});
		} else {
			jQuery('.gform_next_button').show();
		}

		if (current_page == hair_settings) {
			jQuery(".no-scelta").on("click", function (e) {
				jQuery('input[name=input_8]').filter('[value=biondi]').prop('checked', true);
				jQuery('input[name=input_55]').filter('[value=lisci]').prop('checked', true);
				jQuery('input[name=input_56]').filter('[value=corti]').prop('checked', true);
				jQuery(bookCustomizeForm).trigger("submit", [true]);
			});
		} // if (current_page == hair_settings)

		if (current_page == eyesAndSkin_settings) {
			jQuery(".no-scelta").on("click", function (e) {
				jQuery('input[name=input_15]').filter('[value=neri]').prop('checked', true);
				jQuery('input[name=input_17]').filter('[value=bianca]').prop('checked', true);		
				jQuery('input[name=input_111]').val(jQuery('input[name=input_15]:checked').val()); 
				jQuery('input[name=input_110]').val(jQuery('input[name=input_17]:checked').val() ); 
				jQuery(bookCustomizeForm).trigger("submit", [true]);			
			});
			jQuery(":submit").on("click", function (e) {
				jQuery('input[name=input_111]').val(jQuery('input[name=input_15]:checked').val()); 
				jQuery('input[name=input_110]').val(jQuery('input[name=input_17]:checked').val() ); 
			});
		} // if (current_page == eyesAndSkin_settings)    
		
		if (current_page == bookSelection_settings) {
			jQuery(".no-scelta").on("click", function (e) {
				jQuery('input[name=input_108]').filter('[value=neri]').prop('checked', true);
				jQuery('input[name=input_109]').filter('[value=bianca]').prop('checked', true);		
				jQuery('input[name=input_111]').val(jQuery('input[name=input_108]:checked').val()); 
				jQuery('input[name=input_110]').val(jQuery('input[name=input_109]:checked').val() ); 
				jQuery(bookCustomizeForm).trigger("submit", [true]);			
			});
			jQuery(":submit").on("click", function (e) {
				eyes_current_val = (jQuery('input[name=input_15]').is(":checked"))  ? jQuery('input[name=input_15]:checked').val() : jQuery('input[name=input_108]:checked').val();
				jQuery('input[name=input_111]').val(eyes_current_val); 
				skin_current_val = (jQuery('input[name=input_17]').is(":checked"))  ? jQuery('input[name=input_17]:checked').val() : jQuery('input[name=input_109]:checked').val();
				jQuery('input[name=input_110]').val(skin_current_val); 
				book_current_val = (jQuery('input[name=input_106]').val().lenght>0)  ? jQuery('input[name=input_106]').val() : jQuery('input[name=input_65]:checked').val();
				jQuery('input[name=input_106]').val(book_current_val);
			});
		} // if (current_page == eyesAndSkin_settings) 

		// statement temporaneo per risolvere il limite di scrolling
		jQuery('html').attr('style', 'cursor: -webkit-grab; touch-action: none; overflow: initial !important;');
	})
}) // if (form_id == bookCustomForm)


jQuery(document).ready(function () {

	jQuery('.gform_next_button').hide();

	jQuery('#female-child').on("click", function (e) {
		jQuery('input[name=input_1]').filter('[value=bambina]').prop('checked', true);
		jQuery('input[name=input_1]').filter('[value=bambino]').prop('checked', false);
		jQuery(bookCustomizeForm).trigger("submit", [true]);
	});

	jQuery('#male-child').on("click", function (e) {
		jQuery('input[name=input_1]').filter('[value=bambina]').prop('checked', false);
		jQuery('input[name=input_1]').filter('[value=bambino]').prop('checked', true);
		jQuery(bookCustomizeForm).trigger("submit", [true]);
	});
});