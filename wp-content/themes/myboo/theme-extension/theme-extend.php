<?php

include get_template_directory() . '/theme-extension/gf-module/gf-extend.php';
include get_template_directory() . '/theme-extension/woo-module/woo-extend.php';
include get_template_directory() . '/theme-extension/ebook-viewer/ebook-viewer.php';

$books = array(
	"la_favola_del_mago_del_tempo",
	"la_nascita",
	"buon_compleanno"
);

$gf_mapping = new gf_extend($books);


/* procedura di creazione pdf, attivata da ajax */
 


class book_generator {  
	
	function __construct($template_book_dir, $book_path, $result, $text_var_num, $text_select_var, $text_select_val, $uploaded_photo, $font_family_file, $font_family_tag, $font_size, $book_dimension) {
		$this->init();
		return $this->create_book($template_book_dir, $book_path, $result, $text_var_num, $text_select_var, $text_select_val, $uploaded_photo, $font_family_file, $font_family_tag, $font_size, $book_dimension);
	}	
	
	private function init() {
		require(get_template_directory() . '/theme-extension/ebook-generator/textbox.php');
	}	

	public function create_book($template_book_dir, $book_path, $result, $text_var_num, $text_select_var, $text_select_val, $uploaded_photo, $font_family_file, $font_family_tag, $font_size, $book_dimension) {  
		
		$pdf = new PDF_TextBox('L','cm',$book_dimension);
		$pdf->AddFont($font_family_tag,'',$font_family_file);
		$pdf->SetMargins(0, 0);
		$pdf->SetAutoPageBreak(true, 0);
		$page = 0;

		$i = 0;
 		foreach($result as $row):
			$i++;
			$prev_page = $page;
			$page = $row["page"];
			if ( $page > $prev_page):  
				$pdf->AddPage();
			endif;    
			if ($row["type"] === 'image'):
				if ($row["layer"] === '100' && $uploaded_photo):
					$pdf->Image($uploaded_photo,1.5,1,2);
				endif;
				$file = $template_book_dir .'/' .$row["file"];
				$pdf->Image($file,0,0,$book_dimension[0],$book_dimension[1]);
			elseif ($row["type"] === 'text'):  
				$text = file_get_contents($template_book_dir .'/' .$row["file"]);
				$variations = explode("&",$row["field"]);
				$variations_values = explode("&",$row["value"]);
		error_log(print_r(explode("&",$row["field"])) .' e ' .print_r(explode("&",$row["value"])),1, "lucia.arsena@gmail.com");
		//error_log(print_r($variations) .' e ' .print_r($variations_values),1, "lucia.arsena@gmail.com");
				$initial_font_family_tag = $font_family_tag;
				$initial_font_size = $font_size;
				$rotate = false;
				//for($i = 0; $i < count($variations); $i++):
				//	$variations[$i] = $variations_values[$i];
  				//endfor;
				
				for($j = 0; $j < $text_var_num; $j++): 
					$text = str_replace($text_select_var[$j], $text_select_val[$j], $text);
				endfor; 
				$pdf->SetFont($font_family_tag,'', $font_size);
				$pdf->SetTextColor($font_color[0], $font_color[1], $font_color[2]);
				$pdf->SetXY(0.5, 0.5);
				$pdf->drawTextBox($text, $book_dimension[0]-1, $book_dimension[1]-1, 'C', 'M', false);
				$font_family_tag = $initial_font_family_tag;
				$font_size = $initial_font_size;
				$font_color = array(51,51,51);
				$rotate = false;
			endif;
		endforeach; 
		
		return $pdf->Output('F', $book_path);
  	}	
}


function create_low_res_book() {  
	
	$time = time();
	
  	$generated_books_rel_path = '/myboo/generated-books'; 
    $template_books_rel_path = '/myboo/template-books/';
	
	$uploads = wp_upload_dir();  
  	if ( !defined('ABSPATH') )
	  	define('ABSPATH', dirname(__FILE__) . '/');
  	$abspath= ABSPATH; 
  	
   	$generated_book_dir = $uploads['basedir'] .$generated_books_rel_path;
  	$generated_book_url = $uploads['baseurl'] .$generated_books_rel_path .'/' .date('Y', $time) .'/' .date('m', $time) .'/' .$_REQUEST['title'];
  	$generated_book_path = $generated_book_dir .'/' .date('Y', $time) .'/' .date('m', $time) .'/' .$_REQUEST['title'];

	$template_book_dir = $uploads['basedir'] .$template_books_rel_path .$_REQUEST['title'];
  	$path = site_url();
	
	$query_book_val = $_REQUEST['query_book_title'];
  	$query_select_var = $_REQUEST['query_select_var'];
  	$query_select_val = $_REQUEST['query_select_val'];
  	$text_select_var = $_REQUEST['text_select_var'];
  	$text_select_val = $_REQUEST['text_select_val'];
	$customization = $_REQUEST['customization'];
  	$query_var_num = count($query_select_var);
  	$text_var_num = count($text_select_var);
  	$query_cond = "(booktitle = '" .$query_book_val ."' AND type = 'text') OR (booktitle = '" .$query_book_val ."' AND field = '' AND value = '')";
  
  	for($i = 0; $i < $query_var_num; $i++):
		$query_cond = $query_cond ." OR " ."(booktitle = '" .$query_book_val ."' AND field = '" .$query_select_var[$i] ."' AND " ."value = '" .$query_select_val[$i] ."')";
  	endfor;
  
  	$filename = $_REQUEST['title'] .'-' .time();
	$query = "SELECT type, page, layer, file, field, value FROM books WHERE " .$query_cond ." ORDER BY page, layer ASC"; 
	
	require_once(get_template_directory() . '/theme-extension/db-controller/db-controller.php');
  	$db_handle = new DBController();    
  	

  	$result = $db_handle->runQuery($query);
	
	  	
	if (!file_exists($generated_book_path )) {
    	wp_mkdir_p($generated_book_path);
  	}
	$book_url =  $generated_book_url .'/' .$filename .'.pdf';
    $book_path =  $generated_book_path .'/' .$filename .'.pdf';  
  	
	session_start();     
  	$_SESSION['book_link'] = $book_url;
  	$_SESSION['query'] = $query;
	$_SESSION['customization'] = $customization;
	$_SESSION['title'] = $query_book_val;	

	if (isset($_SESSION['uploaded_file'])):
		$uploaded_photo = $_SESSION['uploaded_file'];
	else:
		$uploaded_photo = false;
	endif;
	
	$book_dimension = array(5,5);
	
	$font_family_file = 'Cookie-Regular.php';
	$font_family_tag = 'Cookie';
	$font_size = 8; 

    $generated_book = new book_generator($template_book_dir, $book_path, $result, $text_var_num, $text_select_var, $text_select_val, $uploaded_photo, $font_family_file, $font_family_tag, $font_size, $book_dimension);
	
	echo $book_url;
	
	die();
	
}
add_action('wp_ajax_create_low_res_book', 'create_low_res_book');
add_action('wp_ajax_nopriv_create_low_res_book', 'create_low_res_book');
	

/*

function create_high_res_book($query) {  
	
	$time = time();
	
  	$generated_books_rel_path = '/myboo/generated-books-high-res'; 
    $template_books_rel_path = '/myboo/template-books-high-res/';
	
	$uploads = wp_upload_dir();  
  	if ( !defined('ABSPATH') )
	  	define('ABSPATH', dirname(__FILE__) . '/');
  	$abspath= ABSPATH; 
  	
   	$generated_book_dir = $uploads['basedir'] .$generated_books_rel_path;
  	$generated_book_url = $uploads['baseurl'] .$generated_books_rel_path .'/' .date('Y', $time) .'/' .date('m', $time) .'/' .$_REQUEST['title'] .'test';
  	$generated_book_path = $generated_book_dir .'/' .date('Y', $time) .'/' .date('m', $time) .'/' .$_REQUEST['title'] .'test';

	$template_book_dir = $uploads['basedir'] .$template_books_rel_path .$_REQUEST['title'];
  	$path = site_url();
  
  	$filename = $_REQUEST['title'] .'-' .time();
	
	require_once(get_template_directory() . '/theme-extension/db-controller/db-controller.php');
  	$db_handle = new DBController();    
  	

  	$result = $db_handle->runQuery($query);
	
	  	
	if (!file_exists($generated_book_path )) {
    	wp_mkdir_p($generated_book_path);
  	}
	$book_url =  $generated_book_url .'/' .$filename .'.pdf';
    $book_path =  $generated_book_path .'/' .$filename .'.pdf';  
  	

	session_start();     
  	$_SESSION['book_link'] = $book_url;
  	$_SESSION['query'] = $query;
	$_SESSION['customization'] = $customization;
	$_SESSION['title'] = $query_book_val;


    $generated_book = book_generator($template_book_dir, $book_path, $result, $text_var_num, 1, 2, $text_select_var, $text_select_val);
	
	echo $book_url;
	
	die();
	
}
*/

?>