<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class Supplier_Email extends WC_Email {
	
	public function __construct() {

		$this->id = 'supplier_order_email';
		$this->title = __( 'Ordine di stampa', 'woocommerce' );
		$this->description = __( 'Contiene file di stampa per libro MyBoo', 'woocommerce' );

		$this->subject = apply_filters( 'supplier_order_email_default_subject', __( 'Ordine di stampa', 'woocommerce' ) );
		$this->heading = apply_filters( 'supplier_order_email_default_heading', __( 'Contiene file di stampa per libro MyBoo', 'woocommerce' ) );


		// Trigger email when payment is complete
		//add_action( 'woocommerce_payment_complete', array( $this, 'trigger' ) );
		
		// Call parent constructor to load any other defaults not explicity defined here
		parent::__construct();

		$this->template_html  = 'emails/supplier-new-order.php';
		$this->template_plain = 'emails/plain/supplier-new-order.php';		
		$this->recipient = $this->get_option( 'recipient' );
    	if ( ! $this->recipient )
        	$this->recipient = get_option( 'admin_email' );
	}



	public function trigger( $order_id ) {

		if ( ! $order_id )
			return;

			
	//if ( ! get_post_meta( $order_id, '_supplier_order_email_sent', true ) ) {
			
			// setup order object
		$order = wc_get_order( $order_id );
		
		error_log( '$this->object  ' .$order, 1, "lucia.arsena@gmail.com" );
		
		$order_data = $order->get_data(); // The Order data
		
		//$this->query = $query;
		$this->order_ident = $order_data['id'];
		$this->order_parent_id = $order_data['parent_id'];
		$this->order_status = $order_data['status'];
		$this->order_currency = $order_data['currency'];
		$this->order_version = $order_data['version'];
		$this->order_payment_method = $order_data['payment_method'];
		$this->order_payment_method_title = $order_data['payment_method_title'];
		$this->order_payment_method = $order_data['payment_method'];
		$this->order_payment_method = $order_data['payment_method'];
		
		$this->order_date_created = date_i18n( wc_date_format(), strtotime($order_data['date_created']));
		$this->order_date_modified = $order_data['date_modified'];
			
			// get order items as array
			//$order_items = $this->object->get_items();
//foreach ($order->get_items() as $item_key => $item_values):

    ## Using WC_Order_Item methods ##

    // Item ID is directly accessible from the $item_key in the foreach loop or
   // $item_id = $item_values->get_id();

    ## Using WC_Order_Item_Product methods ##

    //$item_name = $item_values->get_name(); // Name of the product
    //$item_type = $item_values->get_type(); // Type of the order item ("line_item")

    //$product_id = $item_values->get_product_id(); // the Product id
    //$wc_product = $item_values->get_product(); // the WC_Product object
    ## Access Order Items data properties (in an array of values) ##
    //$item_data = $item_values->get_data();
		
		 $items = $order->get_items();



    foreach ( $items as $item_id => $item_data ):

        //echo '<h4>RAW OUTPUT OF THE ORDER ITEM NUMBER: '. $item_id .'): </h4>';
        //print_r($item_data);

    $product_query = $item_data['Query'];
	$this->filename = $item_data['File'];
    $product_id = $item_data['product_id'];
    $variation_id = $item_data['variation_id'];
    $quantity = $item_data['quantity'];
    $tax_class = $item_data['tax_class'];
    $line_subtotal = $item_data['subtotal'];
    $line_subtotal_tax = $item_data['subtotal_tax'];
    $line_total = $item_data['total'];
    $line_total_tax = $item_data['total_tax'];

endforeach;

			// replace variables in the subject/headings
			$this->find[] = '{order_date}';
			$this->replace[] = $this->order_date_created;

			$this->find[] = '{order_number}';
			$this->replace[] = $this->order_ident;

			if ( ! $this->is_enabled() || ! $this->get_recipient() ) {
				return;
			}

			// All well, send the email
			$this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
			

			// Set order meta to indicate that the welcome email was sent
			//update_post_meta( $this->object->id, '_supplier_order_email_sent', 1 );
		//update_post_meta( $this->order_ident, '_supplier_order_email_sent', 1 );
		
		
		//$order = wc_get_order($order_id);

// Iterating through each WC_Order_Item_Product objects

			
		//}
		
	}
	

	public function get_content_html() {
		return wc_get_template_html( $this->template_html, array(
			'order'					=> $this->object,
			'email_heading' 		=> $this->get_heading(),
			'plain_text'			=> false,
			'email'					=> $this,
			'filename'     			=> $this->filename,
			'date'     				=> $this->order_date_created, 
			'order'     			=> $this->order_ident
		) );
	}
	

	public function get_content_plain() {
		return wc_get_template_html( $this->template_plain, array(
			'order'					=> $this->object,
			'email_heading' 		=> $this->get_heading(),
			'plain_text'			=> true,
			'email'					=> $this
		) );
	}



	public function init_form_fields() {

		$this->form_fields = array(
			'enabled'    => array(
				'title'   => __( 'Enable/Disable', 'woocommerce' ),
				'type'    => 'checkbox',
				'label'   => 'Enable this email notification',
				'default' => 'yes'
			),
			'recipient'  => array(
				'title'       => 'Recipient(s)',
				'type'        => 'text',
				'description' => sprintf( 'Enter recipients (comma separated) for this email. Defaults to <code>%s</code>.', esc_attr( get_option( 'admin_email' ) ) ),
				'placeholder' => '',
				'default'     => ''
			),
			'subject'    => array(
				'title'       => __( 'Subject', 'woocommerce' ),
				'type'        => 'text',
				'description' => sprintf( 'This controls the email subject line. Leave blank to use the default subject: <code>%s</code>.', $this->subject ),
				'placeholder' => '',
				'default'     => ''
			),
			'heading'    => array(
				'title'       => __( 'Email Heading', 'woocommerce' ),
				'type'        => 'text',
				'description' => sprintf( __( 'This controls the main heading contained within the email notification. Leave blank to use the default heading: <code>%s</code>.' ), $this->heading ),
				'placeholder' => '',
				'default'     => ''
			),
			'email_type' => array(
				'title'       => __( 'Email type', 'woocommerce' ),
				'type'        => 'select',
				'description' => __( 'Choose which format of email to send.', 'woocommerce' ),
				'default'       => 'html',
				'class'         => 'email_type wc-enhanced-select',
				'options'     => array(
					'plain'	    => __( 'Plain text', 'woocommerce' ),
					'html' 	    => __( 'HTML', 'woocommerce' ),
					'multipart' => __( 'Multipart', 'woocommerce' ),
				)
			)
		);
	}

}
