<?php


class Example_Background_Processing {


	protected $process_single;

  
  protected $order;

	/**
	 * Example_Background_Processing constructor.
	 */
	public function __construct() {
		add_action( 'plugins_loaded', array( $this, 'init' ) );
		//add_action( 'admin_bar_menu', array( $this, 'admin_bar' ), 100 );
		add_action( 'init', array( $this, 'process_handler' ) );
    //$this->handle_single();
	}

	/**
	 * Init
	 */
	public function init() {

		require_once(get_template_directory() . '/theme-extension/woo-module/includes/class-example-request.php');	


		$this->process_single = new WP_Example_Request();	}


	public function process_handler() { 
		if ( ! isset( $_GET['process'] ) ) {
			return;
		}
    
    /*if ( ! isset( $_GET['process'] ) || ! isset( $_GET['_wpnonce'] ) ) {
			return;
		}

		if ( ! wp_verify_nonce( $_GET['_wpnonce'], 'process') ) {
			return;
		} */ 

		if ( 'single' === $_GET['process'] ) {    
      $this->order = $_GET['orderID'];
			$this->handle_single();
		}
  
	}

	/**
	 * Handle single
	 */
	protected function handle_single() {

    $order = $this->order; 


		//$this->process_single->data( array(  'order' => $order ) )->dispatch();

	}



}

new Example_Background_Processing();