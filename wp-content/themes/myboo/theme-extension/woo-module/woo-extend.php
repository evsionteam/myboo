<?php

// aggiunge il libro scelto al carrello quando viene fatto il submit del form di personalizzazione

function woo_style() {
	wp_enqueue_style( 'woo-style', get_template_directory_uri() . '/theme-extension/woo-module/css/woo-style.css',  array(), 1  );   
}
add_action('wp_enqueue_scripts', 'woo_style');


require_once( get_template_directory() . '/theme-extension/woo-module/includes/example-plugin.php' );
	new Example_Background_Processing();

function myboo_add_to_cart( $entry, $form ) {
	
	session_start();
	global $book_forms; 
	
	
	
		
	//error_log($product_id .' + ' .$gift_box_id .' + ' .$variation_id, 1, "lucia.arsena@gmail.com");
	
	if (in_array($form['id'], $book_forms)) {
		
		ob_start();
		$product_id = $entry["151"];
	$gift_box_id = $entry["150.1"];
	$variation_id = $entry["148"];
	if ($gift_box_id) {
				$_SESSION['gift_note'] = "Richiesta confezione regalo";} else {$_SESSION['gift_note'] = '';}
		$quantity          = 1;
		$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );
		$product_status    = get_post_status( $product_id ); 
		
		add_filter( 'wc_add_to_cart_message_html', '__return_null' );

		if ( $passed_validation && WC()->cart->add_to_cart( $product_id, $quantity,$variation_id, wc_get_product_variation_attributes( $variation_id) ) && 'publish' === $product_status ) {
			do_action( 'woocommerce_ajax_added_to_cart', $product_id );
			wc_add_to_cart_message( $product_id ); 
			if ($gift_box_id) {

			WC()->cart->add_to_cart( $gift_box_id, 1);
			do_action( 'woocommerce_ajax_added_to_cart', $gift_box_id );
			wc_add_to_cart_message( $gift_box_id);  
				}
		} 
		else {
			/*
			// If there was an error adding to the cart, redirect to the product page to show any errors
			$data = array(
			  'error'       => true,
			  'product_url' => apply_filters( 'woocommerce_cart_redirect_after_error', get_permalink( $product_id ), $product_id )
			);
			wp_send_json( $data );
			*/
		}
		
			 
			

	}
}
add_action( 'gform_after_submission', 'myboo_add_to_cart', 10, 2 );


function myboo_add_customization_data_to_cart_item( $cart_item_data, $product_id, $variation_id ) {
	
	$gift_box_id = 268;
	
	$book_link = $_SESSION['book_link'];
		$query = $_SESSION['query'];
		$customization = $_SESSION['customization'];
		$title = $_SESSION['title'];
	$gift_note = $_SESSION['gift_note'];
	
	if ( empty( $book_link ) && empty( $query ) && empty( $customization ) && empty($title) && empty($gift_note)) {
			return $cart_item_data;
		}
	
	if ($product_id != $gift_box_id) {
		
		$cart_item_data['book_link'] = $book_link;
		$cart_item_data['query'] = $query;
		$cart_item_data['customization'] = $customization;
		$cart_item_data['gift_note'] = $gift_note;

	}
	else {

		
		$cart_item_data['title'] = 'Confezione';
	} 
																								
    return $cart_item_data;
} 
add_filter( 'woocommerce_add_cart_item_data', 'myboo_add_customization_data_to_cart_item', 10, 3 );


function myboo_print_customization_data_cart( $product_name, $cart_item, $cart_item_key ) {


    if (  empty( $cart_item['customization'] )) {
        return $product_name;
    }
    return sprintf('<p><strong> %s </strong></p><p><strong>%s</strong>: %s</p><p><strong>%s</strong></p>', $product_name, 'Dati di personalizzazione', $cart_item['customization'] , $cart_item['gift_note']);
}
add_filter( 'woocommerce_cart_item_name', 'myboo_print_customization_data_cart', 10, 3 );

function  myboo_add_customization_data_to_order_items( $item, $cart_item_key, $values, $order ) {
    if (  empty( $values['customization']  )) {
        return;
    }
    $item->add_meta_data( __( 'Dati di personalizzazione', 'iconic' ), $values['customization'] );
	$item->add_meta_data( __( 'File', 'iconic' ), $values['book_link'] );
	$item->add_meta_data( __( 'Query', 'iconic' ), $values['query'] );
	$item->add_meta_data( __( 'Confezione regalo', 'iconic' ), $values['gift_note'] );
} 
add_action( 'woocommerce_checkout_create_order_line_item', 'myboo_add_customization_data_to_order_items', 10, 4 );


function myboo_woocommerce_payment_complete( $order_id ) { 
 
	function myboo_add_parameters( $order_id ){
		$order = wc_get_order( $order_id );
		$return_url = $order->get_checkout_order_received_url() .'&process=single&orderID='.$order_id;
		//error_log( $return_url, 1, "lucia.arsena@gmail.com" );
		wp_redirect($return_url);
	}
	add_action( 'woocommerce_thankyou', 'myboo_add_parameters');

}
add_action( 'woocommerce_payment_complete', 'myboo_woocommerce_payment_complete', 10, 3 );  
//add_action( 'woocommerce_add_to_cart', 'myboo_woocommerce_payment_complete', 10, 3 );  


function supplier_print_request_emails( $email_classes ) {
	require_once( get_template_directory() . '/theme-extension/woo-module/includes/class-wc-supplier-order-email.php' );
	$email_classes['Supplier_Email'] = new Supplier_Email(); // add to the list of email classes that WooCommerce loads
	return $email_classes;
}
add_filter( 'woocommerce_email_classes', 'supplier_print_request_emails' );

function wc_display_item_meta( $item, $args = array() ) {
	
		global $print_item_meta;
        $strings = array();
        $html    = '';
        $args    = wp_parse_args( $args, array(
            'before'    => '<ul class="wc-item-meta"><li>',
            'after'     => '</li></ul>',
            'separator' => '</li><li>',
            'echo'      => true,
            'autop'     => false,
        ) );

        foreach ( $item->get_formatted_meta_data() as $meta_id => $meta ) { 
			if ( ($meta->display_key == "Dati di personalizzazione" || $print_item_meta == true || $meta->display_key == "Confezione regalo") && $meta->display_key != "query" ) { 
            	$value = $args['autop'] ? wp_kses_post( wpautop( make_clickable( $meta->display_value ) ) ) : wp_kses_post( make_clickable( $meta->display_value ) ); 
            	$strings[] = '<strong class="wc-item-meta-label">' . wp_kses_post( $meta->display_key ) . ':</strong> ' . $value;
			}
        }

        if ( $strings ) {
            $html = $args['before'] . implode( $args['separator'], $strings ) . $args['after'];
        }

        $html = apply_filters( 'woocommerce_display_item_meta', $html, $item, $args );

        if ( $args['echo'] ) {
            echo $html;
        } else {
            return $html;
        }
    }



?>