<?php
/**
 * Supplier new order email
 *
 */

global $print_item_meta;
$print_item_meta = true;

 if ( ! defined( 'ABSPATH' ) ) {
 	exit;
 }

 /**
  * @hooked WC_Emails::email_header() Output the email header
  */
 do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

 <p><?php printf( __( "File di stampa all'indirizzo %s", 'woocommerce' ), $filename ); ?></p>

<p><?php printf( __( "Data e numero d'ordine %s %s", 'woocommerce' ), $date, $order ); ?></p>

 <p>Dettagli di spedizione</p>


 <?php

 do_action( 'woocommerce_email_customer_details', $order, $plain_text, $email );

 do_action( 'woocommerce_email_footer', $email );