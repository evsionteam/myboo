<?php
/*
Template Name: Create book
*/

get_header(); 



?>

<?php
  // query condition
  // occhi
  // capelli -> acconciatura + colore-capelli
  // pelle
  // occhiali
?>

<form type="post" action="" id="book-configuration">

<input type="hidden" name="titolo" value="test-book"/>

<input type="hidden" name="query-select-var" value="occhi,colore-capelli&acconciatura&lunghezza,pelle,occhiali"/>  
<input type="hidden" name="query-input-var" value="$$$nome$$$&$$$gioco$$$,$$$nome-mamma$$$"/> 
<input type="hidden" name="query-label" value="Nome,Gioco,Nome mamma,Occhi,Colore capelli,Acconciatura,Lunghezza,Pelle,Occhiali"/>
<input type="hidden" name="query-value" value="$$$nome$$$,$$$gioco$$$,$$$nome-mamma$$$,occhi,colore-capelli,acconciatura,lunghezza,pelle,occhiali"/>

<label for="$$$nome$$$">Nome:</label>

<input type="text" name="$$$nome$$$" />

<label for="$$$nome-mamma$$$">Nome mamma:</label>

<input type="text" name="$$$nome-mamma$$$" />

<label for="$$$gioco$$$">Gioco:</label>

<input type="text" name="$$$gioco$$$" />

<label for="occhi">Occhi:</label>

<select name="occhi">
  <option value="azzurri">azzurri</option>
  <option value="marroni">marroni</option>
  <option value="neri">neri</option>
  <option value="a mandorla">a mandorla</option>
</select>

<label for="colore-capelli">Colore capelli:</label>

<select name="colore-capelli">
  <option value="neri">neri</option>
  <option value="rossi">rossi</option>
  <option value="marroni">marroni</option>
  <option value="biondi">biondi</option>
</select>



<label for="acconciatura">Acconciatura:</label>
<select name="acconciatura">
  <option value="lisci">lisci</option>
  <option value="ricci">ricci</option>
</select>

<label for="lunghezza">Lunghezza:</label>
<select name="lunghezza">
  <option value="corti">corti</option>
  <option value="lunghi">lunghi</option>
</select>


<label for="pelle">Pelle:</label>
<select name="pelle">
  <option value="nera">nera</option>
  <option value="bianca">bianca</option>
  <option value="gialla">gialla</option>
</select>

<label for="occhiali">Occhiali:</label>
<select name="occhiali">
  <option value="si">si</option>
  <option value="no">no</option>
</select>

<input type="hidden" name="action" value="createBook"/>
<input type="submit">
</form>
<br/><br/>


<img src="/wp-content/uploads/img/default.gif" id="img" style="display:none"/ >
<div id="feedback"></div>
<div id="popupContainer"><div id="flipbookContainer"></div><button id="back">Torna indietro e modifica</button><button id="checkout"><?php echo do_shortcode('[add_to_cart id="13" sku=""]'); ?></button></div>
<br/><br/>

 
<script type="text/javascript"> 

var dFlipLocation = "http://myboo.nexnova.biz/wp-content/plugins/ebookgen/dflip/";

jQuery('#book-configuration').submit(ajaxSubmit);

function ajaxSubmit(){

  jQuery('#img').show();

  var newBook = jQuery(this).serialize();

  jQuery.ajax({
    type:"POST",
    url: "/wp-admin/admin-ajax.php",
    data: newBook,
    success:function(data){
      jQuery('#img').hide();
      jQuery("#feedback").html("<p>Ecco l'anteprima del tuo libro </p>" + data);
      var options = {
        height: 500, enableDownload: false,
        allControls: "altPrev,pageNumber,altNext,outline,thumbnail,zoomIn,zoomOut,fullScreen,share,more,download,pageMode,startPage,endPage,sound",
		    mainControls: "altPrev,pageNumber,altNext,outline,thumbnail,zoomIn,zoomOut,fullScreen,share,more",
		    hideControls: "outline, thumbnail, share, more",
        transparent: true,
       // isLightBox: true,
      };
      jQuery("#popupContainer").toggle();
      flipBook = jQuery("#flipbookContainer").flipBook(data, options);
      
      
    

           /*
						//cache the book element
						//var book = $(this);

						if (!window.dfLightBox) {
							window.dfLightBox = new DFLightBox(function () {

								if (window.location.hash.indexOf("#dflip-") == 0)
									window.location.hash = "#_";
								window.dfActiveLightBoxBook.dispose();
								window.dfActiveLightBoxBook = null;
							});
						}

						window.dfLightBox.duration = 500;

						if (window.dfActiveLightBoxBook && window.dfActiveLightBoxBook.dispose) {
							window.dfActiveLightBoxBook.dispose();

						} else {
							window.dfLightBox.show(
								function () {
//                            $("body").addClass("df-no-scroll");
								//	var options = DFLIP.getOptions(data);
								//	options.transparent = false;
								//	options.id = book.attr("id");
								//	var slug = book.attr("slug");
								//	if (slug !== void 0)
								//		options.slug = slug;
									//options.isLightBox = true;
									window.dfActiveLightBoxBook = jQuery(window.dfLightBox.container).flipBook(data, options);
								}
							);
						}

            */
      
      
    }
  });

  return false;
}
</script>


<?php

get_footer(); ?>