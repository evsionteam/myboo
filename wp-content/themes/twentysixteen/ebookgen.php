<?php
/**
 * Plugin Name: eBook generation
 * Plugin URI: 
 * Description: This plugin generates pdf ebook and preview it in flipbook mode.
 * Version: 1.0.0
 * Author: Lucia Arsena
 * Author URI: http://vivaria.it
 * License: GPL2
 */
 
 function createBook(){
  $time = time();
  $uploads = wp_upload_dir();
  
  if ( !defined('ABSPATH') )
	  define('ABSPATH', dirname(__FILE__) . '/');
  $abspath= ABSPATH; 
  
  $generated_book_dir = $uploads['basedir'] .'/myboo/generated-books';
  $generated_book_url = $uploads['baseurl'] .'/myboo/generated-books/' .date('Y', $time) .'/' .date('m', $time) .'/' .$_POST['titolo'];
  $generated_book_path = $generated_book_dir .'/' .date('Y', $time) .'/' .date('m', $time) .'/' .$_POST['titolo'];

  //$generated_book_path = $abspath;
  $template_book_dir = $uploads['basedir'] .'/myboo/template-books/' .$_POST['titolo'];
  $path = site_url();
  
  require_once('dbcontroller.php');
  require('fpdf/fpdf.php');
  
  $query_var = explode(",",($_POST['query-select-var']));
  $query_var_num = count($query_var);
  $query_cond = "(field = '' AND value = '')";
  $and = "";
  $start = 0;
  $query_val = "";

  
  
  for($i = 0; $i < $query_var_num; $i++):
    $actual_query_var = $query_var[$i];  
    $end = strpos($actual_query_var, '&');
    if ($end): // se � una condizione complessa costituita dalla combinazione di pi� campi
      while ($end):
        $actual_query_val_item = substr($actual_query_var, 0, $end);
        $query_val = $query_val .$and .$_POST[$actual_query_val_item];
        $start = $end+1;
        $actual_query_var = substr($actual_query_var, $start);    
        $end = strpos($actual_query_var, '&');
        $and = '&';
        //array_push($customization_data , $actual_query_val_item .' = ' .$_POST[$actual_query_val_item]);
      endwhile;
      $query_val = $query_val .$and .$_POST[$actual_query_var];
      //array_push($customization_data , $actual_query_var .' = ' .$_POST[$actual_query_var]);
      $query_cond = $query_cond ." OR " ."(field = '" .$query_var[$i] ."' AND " ."value = '" .$query_val ."')";
    else: // se � una condizione semplice costituita da un solo campo 
      //array_push($customization_data , $query_var[$i] .' = ' .$_POST[$query_var[$i]]);
      $query_cond = $query_cond ." OR " ."(field = '" .$query_var[$i] ."' AND " ."value = '" .$_POST[$query_var[$i]] ."')";
    endif;
  endfor;
  
  $query_var = explode(",",($_POST['query-input-var']));
  $query_var_num = count($query_var);
  //$query_cond = "(field = '' AND value = '')";
  $and = "";
  $start = 0;
  //$query_val = "";
  
  $text_var = array();
  $text_val = array();
  for($i = 0; $i < $query_var_num; $i++):
    $actual_query_var = $query_var[$i];  
    $end = strpos($actual_query_var, '&');
    if ($end): // se � una condizione complessa costituita dalla combinazione di pi� campi    
      while ($end):
        $actual_query_val_item = substr($actual_query_var, 0, $end);
        array_push($text_var , $actual_query_val_item);
        array_push($text_val , $_POST[$actual_query_val_item]);
        $start = $end+1;
        $actual_query_var = substr($actual_query_var, $start);    
        $end = strpos($actual_query_var, '&');
        //array_push($customization_data , $actual_query_val_item .' = ' .$_POST[$actual_query_val_item]);
        //$and = '&';    
      endwhile;    
      array_push($text_var , $actual_query_var);
      array_push($text_val , $_POST[$actual_query_var]);  
      //array_push($customization_data , $actual_query_var .' = ' .$_POST[$actual_query_var]);
      //$query_val = $query_val .$and .$_POST[substr($query_var[$i], $start + 1)];
      $query_cond = $query_cond ." OR " ."(field = '" .$query_var[$i] ."')";
    else: // se � una condizione semplice costituita da un solo campo  
      array_push($text_var, $query_var[$i]); 
      array_push($text_val, $_POST[$query_var[$i]]); 
      //array_push($customization_data , $query_var[$i] .' = ' .$_POST[$query_var[$i]]);
      $query_cond = $query_cond ." OR " ."(field = '" .$query_var[$i] ."')";
    endif;
  endfor;
  
  $customization_data = array();
  $query_label = explode(",",($_POST['query-label']));
  $query_label_val = explode(",",($_POST['query-value']));
  $query_var_num = count($query_label);  
  
  for($i = 0; $i < $query_var_num; $i++):
    array_push($customization_data , $query_label[$i] .' = ' .$_POST[$query_label_val[$i]]);
  endfor;
  
  

  
  
  $filename = $_POST['titolo'] .'-' .time();
  
  
  
  $db_handle = new DBController();
  $query = "SELECT type, page, layerorder, file FROM books WHERE " .$query_cond ." ORDER BY page, layerorder ASC"; 
  $result = $db_handle->runQuery($query);
  

  $pdf = new FPDF('P','cm',array(21,21));
  $pdf->SetMargins(3, 3);
  //$pdf->SetLineWidth(2);
  $page = 0;
  foreach($result as $row):
    $prev_page = $page;
    $page = $row["page"];
    if ( $page > $prev_page):
      $pdf->AddPage();
      $pdf->SetFont('Arial','B',16);
    endif;    
    if ($row["type"] === 'image'):
      $file = $template_book_dir .'/' .$row["file"];
      $pdf->Image($file,0,0,-300);
    elseif ($row["type"] === 'text'):
      //$text = $row["file"];
      $text = file_get_contents($template_book_dir .'/' .$row["file"]);
      $text_var_count = count($text_var);
      for($i = 0; $i < $text_var_count; $i++):
        $text = str_replace($text_var[$i], $text_val[$i], $text);
      endfor; 
      $pdf->MultiCell(0, 1, $text, 0, 'C');  
      //$pdf->Write(1, $text);
    endif;
  endforeach; 
  
  if (!file_exists($generated_book_path )) {
    wp_mkdir_p($generated_book_path);
  }
  
  $book_url =  $generated_book_url .'/' .$filename .'.pdf';
  $book_path =  $generated_book_path .'/' .$filename .'.pdf';  
  
  session_start();     
  $_SESSION['wdm_user_custom_data'] = $customization_data;
  $_SESSION['wdm_file'] = $book_url;

  $pdf->Output('F', $book_path);

    
  echo $book_url;
  die();
}
add_action('wp_ajax_createBook', 'createBook');
add_action('wp_ajax_nopriv_createBook', 'createBook');

function ebookgen_enqueue_script() {   
  wp_enqueue_script( 'dFlip-script', plugin_dir_url( __FILE__ ) .'dflip/js/dflip.min.js', array('jquery'), '1.0' );
  wp_enqueue_script( 'ebookgen-script', plugin_dir_url( __FILE__ ) .'js/ebookgen.js', array('jquery'), '1.0' );
}
add_action('wp_enqueue_scripts', 'ebookgen_enqueue_script');

function ebookgen_enqueue_style() { 
  wp_enqueue_style( 'dFlip-style', plugin_dir_url( __FILE__ ) .'dflip/css/dflip.css',  array(), 1  );    
  wp_enqueue_style( 'dFlip-icons', plugin_dir_url( __FILE__ ) .'dflip/css/themify-icons.css',  array(), 1  );
  wp_enqueue_style( 'ebookgen-style', plugin_dir_url( __FILE__ ) .'css/ebookgen.css',  array(), 1  );  
}
add_action('wp_enqueue_scripts', 'ebookgen_enqueue_style');

add_filter('woocommerce_add_cart_item_data','wdm_add_item_data',1,2);
 
if(!function_exists('wdm_add_item_data'))
{
    function wdm_add_item_data($cart_item_data,$product_id)
    {
        /*Here, We are adding item in WooCommerce session with, wdm_user_custom_data_value name*/
        global $woocommerce;
        session_start();    
        //if (isset($_SESSION['wdm_user_custom_data'])) {
         $option = array();
            $option = $_SESSION['wdm_user_custom_data'];       
            $new_value = array('wdm_user_custom_data_value' => $option);
            $file = array();
            $file = $_SESSION['wdm_file'];       
            $new_value1 = array('wdm_file_value' => $file);
            $cust = array_merge($new_value,$new_value1);
       // }
        if(empty($option))
            return $cart_item_data;
        else
        {    
            if(empty($cart_item_data))
                return $cust;
            else
                return array_merge($cart_item_data,$cust);
        }
        unset($_SESSION['wdm_user_custom_data']); 
        //Unset our custom session variable, as it is no longer needed.
    }
}
        
        
add_filter('woocommerce_get_cart_item_from_session', 'wdm_get_cart_items_from_session', 1, 3 );
if(!function_exists('wdm_get_cart_items_from_session'))
{
    function wdm_get_cart_items_from_session($item,$values,$key)
    {
        if (array_key_exists( 'wdm_user_custom_data_value', $values ) )
        {
        $item['wdm_user_custom_data_value'] = $values['wdm_user_custom_data_value'];
        }      
        if (array_key_exists( 'wdm_file_value', $values ) )
        {
        $item['wdm_file_value'] = $values['wdm_file_value'];
        }   
        return $item;
    }
}

add_filter('woocommerce_cart_item_name','wdm_add_user_custom_option_from_session_into_cart',1,3);
if(!function_exists('wdm_add_user_custom_option_from_session_into_cart'))
{
 function wdm_add_user_custom_option_from_session_into_cart($product_name, $values, $cart_item_key )
    {
        /*code to add custom data on Cart & checkout Page*/ 
        $count = count($values['wdm_user_custom_data_value']);   
        if($count > 0)
        {
            $return_string = $product_name . "</a><dl class='variation'>";
            $return_string .= "<table class='wdm_options_table' id='" . $values['product_id'] . "'>";
            for ($i = 0; $i < $count; $i++):
              $return_string .= "<tr><td>" . $values['wdm_user_custom_data_value'][$i] . "</td></tr>";
              
            endfor;
            $return_string .= "<tr><td>" . $values['wdm_file_value'] . "</td></tr>";
            $return_string .= "</table></dl>"; 
            return $return_string;
        }
        else
        {
            return $product_name;
        }
    }
}


add_action( 'woocommerce_new_order_item', 'my_line_item_metadata', 10, 3 );
function my_line_item_metadata( $item_id, $item, $order_id ) {
   // Here you have the item, his id, and the order's id
   // You can get the order, for example





      // Save here the metadata for the item id of the hooked line item
      wc_add_order_item_meta( $item_id, '_my_metadata', $item['wdm_file_value'] );
   
}




 /*   
add_action('woocommerce_new_order_item','wdm_add_values_to_order_item_meta',1,2);
if(!function_exists('wdm_add_values_to_order_item_meta'))
{
  function wdm_add_values_to_order_item_meta($item_id, $values)
  {
        global $woocommerce,$wpdb;
        $user_custom_values = array();
        $user_custom_values = $values['wdm_user_custom_data_value'];

        //if(!empty($user_custom_values))
       // {
       
       $count = count($user_custom_values);
      // for($i = 0; $i < $count; $i++):
       
            wc_add_order_item_meta($item_id,$user_custom_values , "aaaa", false);      wc_add_order_item_meta
            
  //    endfor;

       // }

        $user_custom_values1 = $values['wdm_file_value'];
       // if(!empty($user_custom_values))
       //{
            wc_add_order_item_meta($item_id,'wdm_file_value',"eeee", false);  

       // }
  }
}
*/    

add_action('woocommerce_before_cart_item_quantity_zero','wdm_remove_user_custom_data_options_from_cart',1,1);
if(!function_exists('wdm_remove_user_custom_data_options_from_cart'))
{
    function wdm_remove_user_custom_data_options_from_cart($cart_item_key)
    {
        global $woocommerce;
        // Get cart
        $cart = $woocommerce->cart->get_cart();
        // For each item in cart, if item is upsell of deleted product, delete it
        foreach( $cart as $key => $values)
        {
        if ( $values['wdm_user_custom_data_value'] == $cart_item_key )
            unset( $woocommerce->cart->cart_contents[ $key ] );
        }
    }
}

?>